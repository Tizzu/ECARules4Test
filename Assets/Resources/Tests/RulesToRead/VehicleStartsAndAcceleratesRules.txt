﻿define land vehicle LandVehicle1;

when the land vehicle LandVehicle1 accelerates by 2.0
if   the land vehicle LandVehicle1 status is off
then the land vehicle LandVehicle1 starts;

when the land vehicle LandVehicle1 accelerates by 2.0
if   the land vehicle LandVehicle1 status is on
then the land vehicle LandVehicle1 changes speed to 2.0 Km/h;