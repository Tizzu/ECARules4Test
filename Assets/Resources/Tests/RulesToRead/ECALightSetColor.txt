﻿define human chParser;
define light Light1;

define color red = #FF0000;

when the human chParser interacts with the light Light1
if   the light Light1 status is on 
then the light Light1 changes color to the color red;

when the human chParser interacts with the light Light1
if   the light Light1 status is on 
then the light Light1 changes color to #FF0000;

when the human chParser interacts with the light Light1
if   the light Light1 color is the color red
then the light Light1 turns off;