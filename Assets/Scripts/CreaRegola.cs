using ECARules4All.RuleEngine;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreaRegola : MonoBehaviour
{
    public InputField soggettoWhen;
    public InputField verboWhen;
    public InputField oggettoWhen;
    public InputField valoreWhen;
    public InputField soggettoThen;
    public InputField verboThen;
    public InputField oggettoThen;
    public InputField valoreThen;

    private bool oggettoW, valoreW, oggettoT, valoreT;
    // Start is called before the first frame update
    public void onCrea()
    {
        Action whenState = new Action(), thenState = new Action();
        oggettoW = oggettoWhen.text != "";
        valoreW = valoreWhen.text != "";
        oggettoT = oggettoThen.text != "";
        valoreT = valoreThen.text != "";

        whenState.SetSubject(GameObject.Find(soggettoWhen.text));
        thenState.SetSubject(GameObject.Find(soggettoThen.text));
        whenState.SetActionMethod(verboWhen.text);
        thenState.SetActionMethod(verboThen.text);

        if (oggettoW)
        {
            string[] coseOW = oggettoWhen.text.Split(' ');
            switch (coseOW[0])
            {
                case "int": whenState.SetObject(System.Int32.Parse(coseOW[1])); break;
                case "float": whenState.SetObject(System.Double.Parse(coseOW[1])); break;
                case "GameObject": whenState.SetObject(GameObject.Find(coseOW[1])); break;
                case "string": whenState.SetObject(coseOW[1]); break;
            }
        }
        if (valoreW)
        {
            string[] coseVW = valoreWhen.text.Split(' ');
            whenState.SetModifier(coseVW[0]); 
            switch (coseVW[1])
            {
                case "int": whenState.SetModifierValue(System.Int32.Parse(coseVW[2])); break;
                case "float": whenState.SetModifierValue(System.Double.Parse(coseVW[2])); break;
                case "bool": whenState.SetModifierValue(System.Boolean.Parse(coseVW[2])); break;
                case "string": whenState.SetModifierValue(coseVW[2]); break;
            }
        }
        if (oggettoT)
        {
            string[] coseOT = oggettoThen.text.Split(' ');
            switch (coseOT[0])
            {
                case "int": thenState.SetObject(System.Int32.Parse(coseOT[1])); break;
                case "float": thenState.SetObject(System.Double.Parse(coseOT[1])); break;
                case "GameObject": thenState.SetObject(GameObject.Find(coseOT[1])); break;
                case "string": thenState.SetObject(coseOT[1]); break;
            }
        }
        if (valoreT)
        {
            string[] coseVT = valoreThen.text.Split(' ');
            thenState.SetModifier(coseVT[0]);
            switch (coseVT[1])
            {
                case "int": thenState.SetModifierValue(System.Int32.Parse(coseVT[2])); break;
                case "float": thenState.SetModifierValue(System.Double.Parse(coseVT[2])); break;
                case "bool": thenState.SetModifierValue(System.Boolean.Parse(coseVT[2])); break;
                case "string": thenState.SetModifierValue(coseVT[2]); break;
            }
        }

        RuleEngine.GetInstance().Add(new Rule(
            whenState,
            new List<Action>
            {
                thenState
            }
            ));
        soggettoWhen.text = "";
        verboWhen.text = "";
        oggettoWhen.text = "";
        valoreWhen.text = "";

        soggettoThen.text = "";
        verboThen.text = "";
        oggettoThen.text = "";
        valoreThen.text = "";

    }
}