using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ECARules4All.RuleEngine;
using UnityEngine;
using UnityEngine.UI;
using Action = ECARules4All.RuleEngine.Action;

public class VerbComposition
{
    private string verb;
    private ActionAttribute _actionAttribute;

    public VerbComposition(string verb, ActionAttribute actionAttribute)
    {
        Verb = verb;
        ActionAttribute = actionAttribute;
    }

    public string Verb
    {
        get => verb;
        set => verb = value;
    }

    public ActionAttribute ActionAttribute
    {
        get => _actionAttribute;
        set => _actionAttribute = value;
    }
}
public class DropdownHandler : MonoBehaviour
{
    //dropdown references
    private Dropdown subj, verb, objDrop, value;
    public Dropdown Subj => subj;
    public Dropdown Verb => verb;
    public Dropdown ObjDrop => objDrop;
    public Dropdown Value => value;
    
    //Inputfield references
    private InputField objField;
    public InputField ObjField => objField;
    /*private InputField xPos, yPos, zPos;
    public InputField XPos => xPos;
    public InputField YPos => yPos;
    public InputField ZPos => zPos;
    
    //GameObject references
    private GameObject positionField;
    public GameObject PositionField => positionField;*/

    //Dictionary foreach subject with the reference of the gameobject
    private Dictionary<GameObject, string> subjects = new Dictionary<GameObject, string>();
    public Dictionary<GameObject, string> Subjects => subjects;
    
    //Dictionary foreach verb with the index and the verb and the type of object (Position, Rotation, ...)
    private Dictionary<int, VerbComposition> verbsItem = new Dictionary<int, VerbComposition>();
    public Dictionary<int, VerbComposition> VerbsItem => verbsItem;
    
    private Action action;
    private GameObject subjectSelected; //gameobject with the subject
    public GameObject SubjectSelected => subjectSelected;

    private string verbSelectedString; //string with the verb
    public string VerbSelectedString
    {
        get => verbSelectedString;
        set => verbSelectedString = value;
    }

    

    // Start is called before the first frame update
    void Start()
    {
        /*first find gameobject references
        positionField = gameObject.transform.Find("Positions").gameObject;
        xPos = positionField.transform.Find("Xpos").GetComponent<InputField>();
        yPos = positionField.transform.Find("Ypos").GetComponent<InputField>();
        zPos = positionField.transform.Find("Zpos").GetComponent<InputField>();*/

        subj = gameObject.transform.Find("SubjectDrop").gameObject.GetComponent<Dropdown>();
        verb = gameObject.transform.Find("VerbDrop").gameObject.GetComponent<Dropdown>();
        objDrop = gameObject.transform.Find("ObjectDrop").gameObject.GetComponent<Dropdown>();
        value = gameObject.transform.Find("ValueDrop").gameObject.GetComponent<Dropdown>();

        objField = gameObject.transform.Find("InputField").gameObject.GetComponent<InputField>();
        
        verb.onValueChanged.AddListener(delegate { DropdownValueChangedVerb(verb); });
        subj.onValueChanged.AddListener(delegate { DropdownValueChangedSubject(subj); }); 


        populateSubjDropdown();
        
        //initially verb, obj and value are disabled
        verb.gameObject.SetActive(false); 
        objDrop.gameObject.SetActive(false); 
        value.gameObject.SetActive(false); 
        objField.gameObject.SetActive(false);
        //positionField.gameObject.SetActive(false);
    }

    
    /**
     * Listener for verb dropdown, when called we need to provide the correct set of objects
     */
    void DropdownValueChangedVerb(Dropdown change)
    {
        //retrieve selected string and gameobject
        verbSelectedString = change.options[change.value].text;
        int verbSelectedIndex = change.value;

        disableNextComponent(2);
        
        //now, I need to know if the object would be a GameObject or a value
        ActionAttribute ac = verbsItem[change.value].ActionAttribute;
        Debug.Log(ac.ObjectType.Name);

        switch (ac.ObjectType.Name)
        {
            case "Object":
            case "ECAObject":
                objDrop.gameObject.SetActive(true);
                objDrop.ClearOptions();
                foreach(KeyValuePair<GameObject,string> entry in subjects)
                {
                    //TODO gestione alias
                    objDrop.options.Add(new Dropdown.OptionData(entry.Value + " " + entry.Key.name));
                }
                break;
            case "Position" :
            case "Path":
                objDrop.gameObject.SetActive(true);
                objDrop.ClearOptions();
                List<string> positions = RuleUtils.getPositions();
                foreach(string entry in positions)
                {
                    objDrop.options.Add(new Dropdown.OptionData(entry));
                }
                break;
            case "Rotation"://TODO
                break;
            default:
                objField.gameObject.SetActive(true);
                break;
        }
        
        
    }
    
    /**
     * Listener for subject dropdown, when called we need to provide the correct set of verbs
     */
    void DropdownValueChangedSubject(Dropdown change)
    {
       
        verb.gameObject.SetActive(true);
        verb.ClearOptions();
        
        //retrieve selected string and gameobject
        subjectSelected = subjects.Keys.ElementAt(change.value);

        verbsItem = RuleUtils.FindVerbs(subjectSelected); 
        
        //TODO gestire lista vuota
        foreach(KeyValuePair<int,VerbComposition> entry in verbsItem)
        {
            //TODO gestione alias
            verb.options.Add(new Dropdown.OptionData(entry.Value.ActionAttribute.Verb + " " + 
                                                     entry.Value.ActionAttribute.ObjectType.Name));
        }

        
    }

    void populateSubjDropdown()
    {
        //TODO gestire primo elemento selezionato
        subj.ClearOptions();
        subjects = RuleUtils.FindSubjects();
        //TODO gestire lista vuota
        foreach(KeyValuePair<GameObject,string> entry in subjects)
        {
            //TODO gestione alias
            subj.options.Add(new Dropdown.OptionData(entry.Value + " " + entry.Key.name));
        }
    }

    void disableNextComponent(int componentIndex)
    {
        switch (componentIndex)
        {
            case 1:
                verb.gameObject.SetActive(false); 
                objDrop.gameObject.SetActive(false); 
                value.gameObject.SetActive(false); 
                objField.gameObject.SetActive(false);
                
                break;
            case 2: 
                objDrop.gameObject.SetActive(false); 
                value.gameObject.SetActive(false); 
                objField.gameObject.SetActive(false);
                
                break;
        }
    }

    
    
}
