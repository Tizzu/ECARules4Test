﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using ECARules4All.RuleEngine;
using UnityEngine;
using UnityEngine.UI;
using Action = System.Action;

public class RuleUtils : MonoBehaviour
{
    
    public static Dictionary<GameObject,string> FindSubjects()
    {
        //ref to gameObject and inner type of ecaComponent
        var result = new Dictionary<GameObject, string>(); 
        //the subjects are eca components inside the scene
        var foundSubjects = FindObjectsOfType<ECAObject>();
        
        //TODO gestire foundSubjects vuota

        //foreach gameobject found with the ecaobject script
        foreach (var ecaObject in foundSubjects)
        {
            var listOfComponents = ecaObject.GetComponents<Component>();
            var listOfEcaAttributes = new List<Type>();
            foreach (Component c in listOfComponents)
            {
                Type cType = c.GetType();
            
                //searching for the components of type ecarules
                if (Attribute.IsDefined(cType, typeof(ECARules4AllAttribute)))
                {
                    //take all the feasible components
                    listOfEcaAttributes.Add(cType);

                }
            }
            //search among the feasible components the inner one
            var innerName = FindTheInnerOne(listOfEcaAttributes);
            result.Add(ecaObject.gameObject, innerName);
        }
        return result;

    }

    public static Dictionary<int, VerbComposition> FindVerbs(GameObject subjSelected)
    {
        Dictionary<int, VerbComposition> result = new Dictionary<int, VerbComposition>();
        int i = 0;
        foreach (Component c in subjSelected.GetComponents<Component>())
        {
            Type cType = c.GetType();
            
            //searching for the components of type ecarules
            if (Attribute.IsDefined(cType, typeof(ECARules4AllAttribute)))
            {
                //foreach component we find the verbs
                var componentVerbs = ListActionsItem(cType);
                foreach (var el in componentVerbs)
                {
                    result.Add(i, el);
                    i++;
                }
                
                //result = result.Concat(componentVerbs).ToDictionary(s => s.Key, s => s.Value);
               
            }

            
        }

        Debug.Log("Verbs: ");
        foreach (KeyValuePair<int, VerbComposition> kvp in result)
        {
            Debug.Log( string.Format("Key = {0}, Value = {1}", kvp.Key, kvp.Value.Verb + kvp.Value.ActionAttribute) );
        }
        
        return result;
    }
    
    //TODO e se un oggetto ha più ecacomponents?
    public static string FindTheInnerOne(List<Type> listEcaComponents)
    {
        Dictionary<int, string> depts = new Dictionary<int, string>();
        foreach (var comp in listEcaComponents)
        {
            int dept = GetDepth(comp, 0);
            depts.Add(dept, comp.Name);
        }
        var maxKey = depts.Keys.Max();
        return depts[maxKey];
    }
    
    /// <summary>
    /// Returns the dept of a ecarules component
    /// </summary>
    /// <param name="c"></param> type of a component
    /// <param name="depth"></param> the starting depth (0 if we want to search from ecaobject)
    /// <returns>the dept of a ecacomponent</returns>
    private static int GetDepth(MemberInfo c, int depth=0)
    {
        MemberInfo info = c;
        object[] attributes = info.GetCustomAttributes(true);
        for (int i = 0; i < attributes.Length; i++)
        {
            if (attributes[i] is RequireComponent)
            {
                MemberInfo new_info = ((RequireComponent)attributes[i]).m_Type0;
                return GetDepth(new_info, depth+1);
            }
        }
        return depth;
    }

    ///<summary>
    ///<c>ListActions</c> returns all the ActionAttribute tagged variables. 
    ///<para/>
    ///<strong>Parameters:</strong> 
    ///<list type="bullet">
    ///<item><description><paramref name="c"/>: The Type to check</description></item>
    ///</list>
    ///<para/>
    ///<strong>Returns:</strong> A dictionary of the string of the action and the object type (Position, Rotation...)
    ///</summary>
    public static List<VerbComposition> ListActionsItem(Type c)
    {
        List<VerbComposition> actions = new List<VerbComposition>();
        foreach (MethodInfo m in c.GetMethods())
        {
            
            var a = m.GetCustomAttributes(typeof(ActionAttribute), true);
            if (a.Length > 0)
            {
                foreach (var item in a)
                {
                    ActionAttribute ac = (ActionAttribute) item;
                    VerbComposition verbComposition = new VerbComposition(ac.ObjectType.ToString(), ac);
                    actions.Add(verbComposition);
                }
            }
        }
        return actions;
    }
    

    public static void printList(List<string> list)
    {
        foreach (var e in list)
        {
            Debug.Log(e);
        }
    }
    public static void printList( ECAObject [] list)
    {
        foreach (var e in list)
        {
            Debug.Log(e.ToString());
        }
    }
    
    public static void printList( Component [] list)
    {
        foreach (var e in list)
        {
            Debug.Log(e.ToString());
        }
    }
    
    public static void clearInputField(InputField inputfield)
    {
        inputfield.Select();
        inputfield.text = "";
    }

    //TODO returns positions and path
    public static List<string> getPositions()
    {
        return new List<string>();
    }

    
}

