﻿using System;
using UnityEngine;

namespace ECARules4All.RuleEngine
{
    [ECARules4All("behaviour")]
    [RequireComponent(typeof(ECAObject))]
    public class Behaviour : MonoBehaviour
    {
        
    }
}