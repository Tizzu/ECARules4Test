﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using ECAScripts;
using UnityEngine.UI;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("highlight")]
[RequireComponent(typeof(Behaviour))]
public class Highlight : MonoBehaviour
{
    [StateVariable("color", ECARules4AllType.Color)]
    public Color color;
    [StateVariable("on", ECARules4AllType.Boolean)]
    public bool on;

    private ECAOutline outline;

    private void Start()
    {
        outline = gameObject.AddComponent<ECAOutline>();
        outline.OutlineColor = color;
        if (on)
        {
            outline.OutlineWidth = 5f;
        }
        else
        {
            outline.OutlineWidth = 0f;
        }    
    }
    
    [Action(typeof(Highlight), "changes color to", typeof(Color))]
    public void ChangesColor(Color c)
    {
        color = c;
        outline.OutlineColor = color;
    }

    [Action(typeof(Highlight), "turns", typeof(bool))]
    public void Turns(bool on)
    {
        this.on = on;
        if (on)
        {
            outline.OutlineWidth = 5f;
        }
        else
        {
            outline.OutlineWidth = 0f;
        }
    }
}