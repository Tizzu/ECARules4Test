﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("sound")]
[RequireComponent(typeof(Behaviour))]
public class Sound : MonoBehaviour
{
    [StateVariable("source", ECARules4AllType.Text)] public string source;
    [StateVariable("volume", ECARules4AllType.Float)] public float volume;
    [StateVariable("maxVolume", ECARules4AllType.Float)] public float maxVolume;
    [StateVariable("duration", ECARules4AllType.Time)] public Time time;
    [StateVariable("currentTime", ECARules4AllType.Time)] public Time currentTime;
    [StateVariable("playing", ECARules4AllType.Boolean)] public bool playing;
    [StateVariable("paused", ECARules4AllType.Boolean)] public bool paused;
    [StateVariable("stopped", ECARules4AllType.Boolean)] public bool stopped;
    
    [Action(typeof(Sound), "plays")]
    public void Plays()
    {
        this.playing = true;
        this.stopped = false;
        this.paused = false;
        GetComponent<AudioSource>().Play();
    }
    [Action(typeof(Sound), "pauses")]
    public void Pauses()
    {
        this.playing = false;
        this.stopped = false;
        this.paused = true;
        GetComponent<AudioSource>().Pause();
    }
    [Action(typeof(Sound), "stops")]
    public void Stops()
    {
        this.playing = false;
        this.stopped = true;
        this.paused = false;
        GetComponent<AudioSource>().Stop();
    }
    
    //TODO: copy same function declaration from ECAVIDEO once it's reviewed
}