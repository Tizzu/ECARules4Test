﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("collectable")]
[RequireComponent(typeof(Behaviour))]
public class Collectable : MonoBehaviour
{
   
}