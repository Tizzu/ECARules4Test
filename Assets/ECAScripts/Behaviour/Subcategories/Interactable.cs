﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Action = ECARules4All.RuleEngine.Action;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("interactable")]
[RequireComponent(typeof(Behaviour))]
[RequireComponent(typeof(Collider))]
public class Interactable : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        EventBus.GetInstance().Publish(new Action(other.gameObject, "interacts with", this.gameObject));
    }
}