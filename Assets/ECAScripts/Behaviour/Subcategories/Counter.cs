﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("counter")]
[RequireComponent(typeof(Behaviour))]
public class Counter : MonoBehaviour
{
    [StateVariable("count", ECARules4AllType.Float)]
    public float count;

    [Action(typeof(Counter), "changes count to", typeof(float))]
    public void ChangesCount(float amount)
    {
        count = amount;
    }
}