﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Action = ECARules4All.RuleEngine.Action;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("trigger")]
[RequireComponent(typeof(Behaviour))]
public class Trigger : MonoBehaviour
{
    [Action(typeof(Trigger), "triggers", typeof(Action))]
    public void Triggers(Action action)
    {
        EventBus.GetInstance().Publish(action);
        //DOUBT: l'evento lo deve descrivere a mano l'End user Developer?
    }
}