﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("particle")]
[RequireComponent(typeof(Behaviour))]
public class Particle : MonoBehaviour
{
    [StateVariable("on", ECARules4AllType.Boolean)]
    public bool on;

    public ParticleSystem ps;

    //TODO: Questa azione VA in conflitto con azioni all'interno di ECAObject (ad es. Turns di Electronic)
    [Action(typeof(Particle), "turns", typeof(bool))]
    public void Turns(bool on)
    {
        this.@on = on;

        if (on && ps != null)
        {
            ps.Stop();
            ps.Play();
        }
        else if (!on && ps != null)
        {
            ps.Stop();
        }
    }

    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
        if (ps != null)
        {
            if (on)
                ps.Play();
            else
                ps.Stop();
        }
    }
}