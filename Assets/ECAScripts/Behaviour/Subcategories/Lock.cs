﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("lock")]
[RequireComponent(typeof(Behaviour))]
public class Lock : MonoBehaviour
{
    [StateVariable("locked", ECARules4AllType.Boolean)]
    public bool locked;

    [Action(typeof(Lock), "opens")]
    public void Opens()
    {
        locked = false;
    }
    [Action(typeof(Lock), "closes")]
    public void Closes()
    {
        locked = true;
    }
}