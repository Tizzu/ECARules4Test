﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("keypad")]
[RequireComponent(typeof(Behaviour))]
public class Keypad : MonoBehaviour
{
    [StateVariable("keycode", ECARules4AllType.Text)]
    public string keycode;
    [StateVariable("input", ECARules4AllType.Text)]
    public string input;

    //Inserts the whole input into the variable
    [Action(typeof(Keypad), "inserts", typeof(string))]
    public void Inserts(string input)
    {
        this.input = input;
    }
    
    //TODO: verb "sets/changes keycode" present in grammar but no function here
    //Inserts a single character into the variable
    //TODO: verb not present in grammar
    [Action(typeof(Keypad), "adds", typeof(string))]
    public void Add(string input)
    {
        this.input += input;
    }
    
    [Action(typeof(Keypad), "resets")]
    public void Resets()
    {
        input = "";
    }
}