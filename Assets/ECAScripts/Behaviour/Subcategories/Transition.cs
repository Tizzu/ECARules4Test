﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using UnityEngine.SceneManagement;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("transition")]
[RequireComponent(typeof(Behaviour))]
public class Transition : MonoBehaviour
{
    [StateVariable("reference", ECARules4AllType.Identifier)]
    public Scene reference;

    [Action(typeof(Transition), "teleports to", typeof(Scene))]
    public void Teleports(Scene reference)
    {
        if (reference.name != SceneManager.GetActiveScene().name)
        {
            SceneManager.LoadScene(reference.name);
        }

        //DOUBT: come identificare il giocatore nella scena? è giusto che sia Transition e non ECAobject?
        //Giocatore.posizione = reference.position.GetPosition();
        //TODO: test per controllare che la scena esista
    }
}