﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;
using Object = UnityEngine.Object;

[ECARules4All("container")]
[RequireComponent(typeof(Behaviour))]
public class Container : MonoBehaviour
{
    [StateVariable("capacity", ECARules4AllType.Integer)] public int capacity;
    [StateVariable("objectsCount", ECARules4AllType.Integer)] public int objectsCount;
    private List<GameObject> objectsList;

    private void Start()
    {
        objectsList = new List<GameObject>();
    }

    [Action(typeof(Container), "inserts", typeof(GameObject))]
    public void Inserts(GameObject o)
    {
        if (!objectsList.Contains(o))
        {
            if (objectsCount < capacity)
            {
                objectsList.Add(o);
                objectsCount++;
            }

            o.GetComponent<ECAObject>().isActive = false;
        }
    }

    [Action(typeof(Container), "removes", typeof(GameObject))]
    public void Removes(GameObject o)
    {
        objectsList.Remove(o);
        if (objectsList.Count < objectsCount)
        {
            objectsCount--;
        }

        Vector3 fwd = gameObject.transform.forward;
        o.transform.position = gameObject.transform.position + (fwd * 3);
        o.GetComponent<ECAObject>().isActive = true;
    }

    [Action(typeof(Container), "empties")]
    public void Empties()
    {

        foreach (GameObject o in objectsList.ToList())
        {
            Removes(o);
        }

        objectsCount = 0;
    }
}