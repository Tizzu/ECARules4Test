﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Action = ECARules4All.RuleEngine.Action;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("timer")]
[RequireComponent(typeof(Behaviour))]
public class Timer : MonoBehaviour
{
    [StateVariable("duration", ECARules4AllType.Float)] public float duration;
    [StateVariable("current", ECARules4AllType.Float)] public float current;
    private bool active;
    private int elapsed;

    //TODO: possibile conflitto tra grammatica e chiamata di funzione
    [Action(typeof(Timer), "changes duration to", typeof(float))]
    public void ChangeDuration(float amount)
    {
        duration = amount;
    }
    
    //TODO: possibile conflitto tra grammatica e chiamata di funzione
    [Action(typeof(Timer), "changes current time to", typeof(float))]
    public void ChangeCurrentTime(float amount)
    {
        current = amount;
    }

    [Action(typeof(Timer), "starts")]
    public void Starts()
    {
        active = true;
    }

    [Action(typeof(Timer), "stops")]
    public void Stops()
    {
        active = false;
        //TODO: deve azzerare anche il tempo?
    }
    
    
    //TODO: verb "pauses" present in grammar but no function in documentation

    [Action(typeof(Timer), "elapses timer", typeof(int))]
    public void Elapses(int seconds)
    {
        EventBus.GetInstance().Publish(new Action(this.gameObject, "elapses timer", seconds));
    }
    
    [Action(typeof(Timer), "reaches", typeof(int))]
    public void Reaches(int seconds)
    {
        EventBus.GetInstance().Publish(new Action(this.gameObject, "reaches", seconds));
    }

    [Action(typeof(Timer), "resets")]
    public void Resets()
    {
        current = duration;
    }

    private void Update()
    {
        if (active)
        {
            current -= Time.deltaTime;
            if (current < 0)
            {
                active = false;
                current = 0;
            }

            elapsed = (int)(duration - current);
            Elapses(elapsed);
            Reaches((int)current);
        }
    }
}