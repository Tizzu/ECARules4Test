﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("switch")]
[RequireComponent(typeof(Behaviour))]
public class Switch : MonoBehaviour
{
    [StateVariable("on", ECARules4AllType.Boolean)]
    public bool on;

    [Action(typeof(Switch), "turns", typeof(bool))]
    public void Turns(bool on)
    {
        this.on = on;
    }
}