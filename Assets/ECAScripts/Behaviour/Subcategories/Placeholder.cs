﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using Behaviour = ECARules4All.RuleEngine.Behaviour;

[ECARules4All("placeholder")]
[RequireComponent(typeof(Behaviour), typeof(MeshFilter), typeof(MeshCollider))]
public class Placeholder : MonoBehaviour
{
    [StateVariable("mesh", ECARules4AllType.Identifier)]
    public Mesh placeholderMesh;

    void Start()
    {
        gameObject.GetComponent<MeshFilter>().mesh = placeholderMesh;
        gameObject.GetComponent<MeshCollider>().sharedMesh = placeholderMesh;
    }
    [Action(typeof(Placeholder), "changes", "mesh", typeof(Mesh))]
    public void Changes(Mesh mesh)
    {
        placeholderMesh = mesh;
    }
}