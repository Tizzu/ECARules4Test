﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ECARules4All.RuleEngine
{
    [ECARules4All("artwork")]
    [RequireComponent(typeof(Environment))]
    public class Artwork : MonoBehaviour
    {
        [StateVariable("author", ECARules4AllType.Text)]
        public string author;
        [StateVariable("price", ECARules4AllType.Float)]
        public float price;
        [StateVariable("year", ECARules4AllType.Integer)]
        public int year;
    }
}
