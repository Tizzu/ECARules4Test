﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ECARules4All.RuleEngine
{
    [ECARules4All("forniture")]
    [RequireComponent(typeof(Environment))]
    public class Furniture : MonoBehaviour
    {
        [StateVariable("price", ECARules4AllType.Float)]
        public float price;
        [StateVariable("color", ECARules4AllType.Color)]
        public Color color;
        [StateVariable("dimension", ECARules4AllType.Float)]
        public float dimension;

    }
}
