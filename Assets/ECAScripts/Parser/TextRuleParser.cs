using System.Collections.Generic;
using System.IO;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using System;
using UnityEngine;

namespace ECARules4All.RuleEngine
{
    public class TextRuleParser : IECARulesParserListener, IAntlrErrorListener<IToken>
    {
        private string _objectName;
        private Type _objectType;
        private string _objectTypeName;
        private Position _position;
        private Path _path;
        private UnityEngine.Color _color;
        private float _floatLiteral;
        private bool _boolLiteral;
        private int? _intLiteral;
        private string _stringLiteral;
        private string _timeLiteral;
        private GameObject _reference;
        private Action _event, _tmp;
        private CompositeCondition _c;
        private SimpleCondition _simple;
        private List<Action> _actions;
        private string _op;
        private object _value;


        public List<Pair<Type, GameObject>> ObjectDeclarations { get; internal set; }
        public Dictionary<string, Position> PositionDeclarations { get; internal set; }
        public Dictionary<string, UnityEngine.Color> ColorDeclarations { get; internal set; }
        public Dictionary<string, Path> PathDeclarations { get; internal set; }

        public TextRuleParser()
        {
            ObjectDeclarations = new ArrayList<Pair<Type, GameObject>>();
            PositionDeclarations = new Dictionary<string, Position>();
            PathDeclarations = new Dictionary<string, Path>();
            ColorDeclarations = new Dictionary<string, UnityEngine.Color>();
            _actions = new ArrayList<Action>();
        }

        public void ReadRuleFile(string path)
        {
            using (StreamReader fileStream = new StreamReader(path))
            {
                AntlrInputStream inputStream = new AntlrInputStream(fileStream);

                ECARulesLexer lexer = new ECARulesLexer(inputStream);
                CommonTokenStream commonTokenStream = new CommonTokenStream(lexer);
                ECARulesParser parser = new ECARulesParser(commonTokenStream);

                parser.AddParseListener(this);
                parser.AddErrorListener(this);

                parser.BuildParseTree = true;
                IParseTree parseTree = parser.program();

            }
        }

        private void ReadPosition(ECARulesParser.PositionLiteralContext c)
        {
            _position = new Position();
            _position.x = float.Parse(c.floatLiteral()[0].GetText());
            _position.y = float.Parse(c.floatLiteral()[1].GetText());
            _position.z = float.Parse(c.floatLiteral()[2].GetText());
        }

        private GameObject GetReference()
        {
            GameObject obj = GameObject.Find(_objectName);
            if (obj != null)
            {
                _objectType = null;
                foreach (Component c in obj.GetComponents(typeof(Component)))
                {
                    var attrs = c.GetType().GetCustomAttributes(typeof(ECARules4AllAttribute), false);
                    if (attrs != null && attrs.Length > 0)
                    {
                        ECARules4AllAttribute attr = (ECARules4AllAttribute)attrs[0];
                        if (attr.Name.Equals(_objectTypeName))
                        {
                            _objectType = c.GetType();
                        }
                    }


                }

                if (_objectType == null)
                {
                    throw new ArgumentException(
                    String.Format("The object {0} does not contain a {1} component",
                    _objectName,
                    _objectTypeName));
                }
            }
            else
            {
                throw new ArgumentException(
                    String.Format("Cannot find object with name {0}",
                    _objectName));
            }

            return obj;
        }

        private SimpleCondition ReadBaseCondition(ECARulesParser.BaseConditionContext context)
        {
            SimpleCondition condition = null; 
            _objectName = context.IDENTIFIER().GetText();
            _objectTypeName = context.type().IDENTIFIER().GetText();
            _reference = GetReference();
            var property = context.property().IDENTIFIER().GetText();
            if (context.@object() != null) condition = new SimpleCondition(_reference, property, _op, _reference);
            if (context.value() != null) condition = new SimpleCondition(_reference, property, _op, _value);
            return condition;
        }

        public void Reset()
        {
            _c = null;
            ObjectDeclarations.Clear();
            PositionDeclarations.Clear();
        }


        public void VisitTerminal(ITerminalNode node)
        {
            switch (node.Symbol.Type)
            {
                case ECARulesLexer.TIME_LITERAL:
                    _timeLiteral = node.GetText();
                    break;

                case ECARulesLexer.DECIMAL_LITERAL:
                    _intLiteral = int.Parse(node.GetText());
                    break;

                case ECARulesLexer.FLOAT_LITERAL:
                    _floatLiteral = float.Parse(node.GetText());
                    break;

                case ECARulesLexer.STRING_LITERAL:
                    _stringLiteral = node.GetText();
                    break;

                case ECARulesLexer.BOOL_LITERAL:
                    _boolLiteral = node.GetText().Equals("true");
                    break;

                case ECARulesLexer.BOOL_YES_NO:
                    _boolLiteral = node.GetText().Equals("yes");
                    break;

                case ECARulesLexer.COLOR_LITERAL:
                    UnityEngine.ColorUtility.TryParseHtmlString(node.GetText(), out _color);
                    break;

                case ECARulesLexer.ON:
                    _boolLiteral = true;
                    break;

                case ECARulesLexer.OFF:
                    _boolLiteral = false;
                    break;

            }
        }

        public void VisitErrorNode(IErrorNode node)
        {
            throw new ArgumentException("Error in parsing rules");
        }

        public void EnterAction([NotNull] ECARulesParser.ActionContext context)
        {
            _tmp = new Action();
        }

        public void EnterAlias([NotNull] ECARulesParser.AliasContext context)
        {
            
        }

        public void EnterAngle([NotNull] ECARulesParser.AngleContext context)
        {
           
        }

        public void EnterBaseCondition([NotNull] ECARulesParser.BaseConditionContext context)
        {
            
        }

        public void EnterBehaviourDeclaration([NotNull] ECARulesParser.BehaviourDeclarationContext context)
        {
            
        }

        public void EnterColor([NotNull] ECARulesParser.ColorContext context)
        {
            
        }

        public void EnterColorDeclaration([NotNull] ECARulesParser.ColorDeclarationContext context)
        {
            
        }

        public void EnterCondition([NotNull] ECARulesParser.ConditionContext context)
        {
            CompositeCondition composite = new CompositeCondition();
            if(_c != null)
            {
                _c.AddChild(composite);
            }
            _c = composite;
        }

        public void EnterDeclaration([NotNull] ECARulesParser.DeclarationContext context)
        {
            
        }

        public void EnterEcarule([NotNull] ECARulesParser.EcaruleContext context)
        {
            _tmp = null;
            _c = null;
            //_conditionTree = new ConditionTree(;
            _actions.Clear();
        }

        public void EnterEveryRule(ParserRuleContext ctx)
        {
            
        }

        public void EnterFloatLiteral([NotNull] ECARulesParser.FloatLiteralContext context)
        {
            
        }

        public void EnterObject([NotNull] ECARulesParser.ObjectContext context)
        {
            
        }

        public void EnterObjectDeclaration([NotNull] ECARulesParser.ObjectDeclarationContext context)
        {
            
        }

        public void EnterOperator([NotNull] ECARulesParser.OperatorContext context)
        {
            
        }

        public void EnterPath([NotNull] ECARulesParser.PathContext context)
        {
            
        }

        public void EnterPathDeclaration([NotNull] ECARulesParser.PathDeclarationContext context)
        {
            _path = null;
        }

        public void EnterPosition([NotNull] ECARulesParser.PositionContext context)
        {
            
        }

        public void EnterPositionDeclaration([NotNull] ECARulesParser.PositionDeclarationContext context)
        {
            _position = null;
        }

        public void EnterPositionLiteral([NotNull] ECARulesParser.PositionLiteralContext context)
        {
            
        }

        public void EnterPreposition([NotNull] ECARulesParser.PrepositionContext context)
        {
            
        }

        public void EnterProgram([NotNull] ECARulesParser.ProgramContext context)
        {
            this.Reset();
        }

        public void EnterProperty([NotNull] ECARulesParser.PropertyContext context)
        {
            
        }

        public void EnterSubject([NotNull] ECARulesParser.SubjectContext context)
        {
            
        }

        public void EnterType([NotNull] ECARulesParser.TypeContext context)
        {
            

        }

        public void EnterValue([NotNull] ECARulesParser.ValueContext context)
        {
            
        }

        public void EnterVerb([NotNull] ECARulesParser.VerbContext context)
        {
            
        }

        public void ExitAction([NotNull] ECARulesParser.ActionContext context)
        {
            if(context.property() != null)
            {
                // switch the object value with the property
                _tmp.SetModifierValue(_value);
                _tmp.SetObject(context.property().GetText());
            }
            else
            {
                if (context.@object() != null) _tmp.SetObject(_reference);
                if (context.value() != null) _tmp.SetObject(_value);
            }

            if(context.preposition() != null)
            {
                _tmp.SetPreposition(context.preposition().GetText());
            }


            if(context.modifier() != null)
            {
                _tmp.SetModifier(context.modifier().preposition().GetText());
            }

            // distringuishes between events and actions
            ECARulesParser.EcaruleContext parent = (ECARulesParser.EcaruleContext)context.Parent;
            if (parent.THEN() == null)
            {
                // event
                _event = _tmp;
            }
            else
            {
                _actions.Add(_tmp);
            }
        }

        public void ExitAlias([NotNull] ECARulesParser.AliasContext context)
        {
            // TODO is there any alias implementation?
            throw new System.NotImplementedException();
        }

        public void ExitAngle([NotNull] ECARulesParser.AngleContext context)
        {
            // TODO how to manage the axes?
            _floatLiteral = float.Parse(context.floatLiteral().GetText());
        }

        public void ExitBaseCondition([NotNull] ECARulesParser.BaseConditionContext context)
        {
            _simple = ReadBaseCondition(context);
            _c.AddChild(_simple);
        }

        public void ExitBehaviourDeclaration([NotNull] ECARulesParser.BehaviourDeclarationContext context)
        {
            _objectName = context.IDENTIFIER().GetText();
            _objectTypeName = context.type(0).IDENTIFIER().GetText();
            _reference = GetReference();
            string behaviour = context.type(1).IDENTIFIER().GetText();

            bool found = false;
            foreach(Component c in _reference.GetComponents(typeof(Behaviour)))
            {
                if(Attribute.IsDefined(c.GetType(), typeof(ECARules4AllAttribute)))
                {
                    var attrs = c.GetType().GetCustomAttributes(typeof(ECARules4AllAttribute), false);
                    if (attrs != null && attrs.Length > 0)
                    {
                        ECARules4AllAttribute attr = (ECARules4AllAttribute)attrs[0];
                        if (attr.Name.Equals(behaviour)) 
                        {
                            found = true;
                            break;
                        }
                    }
                }
            }

            if (!found)
            {
                throw new ArgumentException(
                    String.Format("The object {0} does not contain a {1} component",
                    _objectName,
                     behaviour));
            }
        }

        public void ExitColor([NotNull] ECARulesParser.ColorContext context)
        {
            _objectType = typeof(UnityEngine.Color);
        }

        public void ExitColorDeclaration([NotNull] ECARulesParser.ColorDeclarationContext context)
        {
            ColorDeclarations.Add(context.IDENTIFIER().GetText(), _color);
        }

        public void ExitCondition([NotNull] ECARulesParser.ConditionContext context)
        {
            if (context.NOT() != null)
            {
                _c.Op = CompositeCondition.ConditionType.NOT;
            }

            if(context.AND().Length > 0)
            {
                _c.Op = CompositeCondition.ConditionType.AND;
            }

            if(context.OR().Length > 0)
            {
                _c.Op = CompositeCondition.ConditionType.OR;
            }

            

            // prune wrapping composite conditions
            if (_c.ChildrenCount() == 1  && _c.parent != null)
            {
                Condition simple = _c.GetChild(0);
                _c.RemoveChild(simple);
                CompositeCondition parent = (CompositeCondition)_c.GetParent();
                parent.RemoveChild(_c);
                if (parent.Op == CompositeCondition.ConditionType.NONE) parent.Op = _c.Op;
                ((CompositeCondition)_c.parent).AddChild(simple);
            }


            if(_c.parent != null) _c = (CompositeCondition)_c.parent;



        }

        public void ExitDeclaration([NotNull] ECARulesParser.DeclarationContext context)
        {
            
        }

        public void ExitEcarule([NotNull] ECARulesParser.EcaruleContext context)
        {
            // remove the composite condition if not needed
            Condition c = _c;
            if(_c != null && _c.ChildrenCount() == 1 && _c.Op != CompositeCondition.ConditionType.NOT)
            {
                c = _c.GetChild(0);
            }
            Rule rule = new Rule(_event, c, _actions);
            RuleEngine.GetInstance().Add(rule);
        }

        public void ExitEveryRule(ParserRuleContext ctx)
        {
            
        }

        public void ExitFloatLiteral([NotNull] ECARulesParser.FloatLiteralContext context)
        {
            _floatLiteral = float.Parse(context.GetText());
        }

        public void ExitObject([NotNull] ECARulesParser.ObjectContext context)
        {
            _objectName = context.IDENTIFIER().GetText();
            _reference = GetReference();
        }

        public void ExitObjectDeclaration([NotNull] ECARulesParser.ObjectDeclarationContext context)
        {
            _objectName = context.IDENTIFIER().GetText();
            _reference = GetReference();
            this.ObjectDeclarations.Add(new Pair<Type, GameObject>(_objectType, _reference));
        }

        public void ExitOperator([NotNull] ECARulesParser.OperatorContext context)
        {
            if(context.GT() != null) _op = ">";
            if (context.LT() != null) _op = "<";
            if (context.EQUAL() != null) _op = "=";
            if (context.LE() != null) _op = "<=";
            if (context.GE() != null) _op = ">=";
            if (context.NOTEQUAL() != null) _op = "!=";
            if (context.IS() != null) _op = "is";
        }

        public void ExitPath([NotNull] ECARulesParser.PathContext context)
        {
            _objectType = typeof(Path);
        }

        public void ExitPathDeclaration([NotNull] ECARulesParser.PathDeclarationContext context)
        {
            _path = new Path(new List<Position>());
            foreach (ECARulesParser.PositionLiteralContext c in context.positionLiteral())
            {
                ReadPosition(c);
                _path.Points.Add(_position);

            }

            PathDeclarations.Add(context.IDENTIFIER().GetText(), _path);
        }

        public void ExitPosition([NotNull] ECARulesParser.PositionContext context)
        {
            _objectType = typeof(Position);
        }

        public void ExitPositionDeclaration([NotNull] ECARulesParser.PositionDeclarationContext context)
        {
            this.PositionDeclarations.Add(context.IDENTIFIER().GetText(), _position);
        }

        public void ExitPositionLiteral([NotNull] ECARulesParser.PositionLiteralContext context)
        {
            if (context.floatLiteral().Length == 3)
            {
                ReadPosition(context);
            }
        }

        public void ExitPreposition([NotNull] ECARulesParser.PrepositionContext context)
        {
            
        }

        public void ExitProgram([NotNull] ECARulesParser.ProgramContext context)
        {
            
        }

        public void ExitProperty([NotNull] ECARulesParser.PropertyContext context)
        {
            _tmp.SetModifier(context.IDENTIFIER().GetText());
        }

        public void ExitSubject([NotNull] ECARulesParser.SubjectContext context)
        {
            _objectName = context.IDENTIFIER().GetText();
            _reference = GetReference();
            _tmp.SetSubject(_reference);
        }

        public void ExitType([NotNull] ECARulesParser.TypeContext context)
        {
            _objectTypeName = context.IDENTIFIER().GetText();
        }

        public void ExitValue([NotNull] ECARulesParser.ValueContext context)
        {
            if (context.floatLiteral() != null) _value = _floatLiteral;
            if (context.position() != null) _value = _position;
            if (context.path() != null)  _value =  _path;
            if (context.POV_LITERAL() != null) _value = context.POV_LITERAL().GetText();
            if (context.BOOL_LITERAL() != null) _value = _boolLiteral;
            if (context.BOOL_YES_NO() != null) _value = _boolLiteral;
            if (context.ON() != null) _value = true;
            if (context.OFF() != null) _value = false;
            if (context.COLOR_LITERAL() != null) _value = _color;
            if (context.TIME_LITERAL() != null) _value = _timeLiteral;
            // TODO add angle management 
            if (context.angle() != null) _value = null;
        }

        public void ExitVerb([NotNull] ECARulesParser.VerbContext context)
        {
            _tmp.SetActionMethod(context.GetText());
        }

        public void SyntaxError(TextWriter output, IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e)
        {
            throw new ArgumentException(
                String.Format("The input file contains a syntax error. Please read the following message\r\n {0}",
                    e.Message));
        }

        public void EnterModifier([NotNull] ECARulesParser.ModifierContext context)
        {
            
        }

        public void ExitModifier([NotNull] ECARulesParser.ModifierContext context)
        {
            
        }
    }


    
}