﻿using System;
using System.IO;
using UnityEngine;

namespace ECARules4All.RuleEngine
{
    public class TextRuleSerializer
    {
        public TextRuleSerializer()
        {

        }

        public void SaveRules(string path)
        {
            using(StreamWriter writer = File.CreateText(path))
            {
                PrintDeclarations(writer);
                writer.WriteLine();
                PrintBehaviourDeclarations(writer);
                writer.WriteLine();

                //TODO add alias

                foreach (Rule r in RuleEngine.GetInstance().Rules())
                {
                    PrintRule(r, writer);
                }

                writer.Flush();
            }
        }

        public Type GetECAObjectType(GameObject gameObject)
        {
            bool isECAObject = false;
            bool changed;
            Type maxType = typeof(ECAObject);

            do
            {
                changed = false;
                foreach (Component c in gameObject.GetComponents(typeof(Component)))
                {
                    if (c.GetType() == typeof(ECAObject))
                    {
                        isECAObject = true;
                    }
                    else
                    {
                        foreach (RequireComponent require in c.GetType().GetCustomAttributes(
                            typeof(RequireComponent), true))
                        {
                            if (require.m_Type0 == maxType)
                            {
                                maxType = c.GetType();
                                changed = true;
                            }
                        }
                    }

                }
            } while (changed);
            

            if (isECAObject) return maxType;
            else return null;
        }

        private string GetTypeName(Type t)
        {
            var attrs = t.GetCustomAttributes(typeof(ECARules4AllAttribute), false);
            ECARules4AllAttribute attr = (ECARules4AllAttribute)attrs[0];
            return attr.Name;
        }

        private void PrintDeclarations(TextWriter writer)
        {
            var objects = GameObject.FindObjectsOfType<ECAObject>();
            foreach(ECAObject obj in objects)
            {
                GameObject gameObj = obj.gameObject;
                writer.WriteLine(String.Format("{0} {1} {2};",
                    "define",
                    GetTypeName(GetECAObjectType(gameObj)),
                    gameObj.name
                    )); ;
            }
        }

        private void PrintBehaviourDeclarations(TextWriter writer)
        {
            var objects = GameObject.FindObjectsOfType<ECAObject>();
            foreach (ECAObject obj in objects)
            {
                GameObject gameObj = obj.gameObject;
                string objType = GetTypeName(GetECAObjectType(gameObj));

                foreach (Component c in gameObj.GetComponents(typeof(Component)))
                {
                    foreach (RequireComponent require in c.GetType().GetCustomAttributes(
                            typeof(RequireComponent), true))
                    {
                        if(require.m_Type0 == typeof(Behaviour))
                        {
                            var attrs = c.GetType().GetCustomAttributes(typeof(ECARules4AllAttribute), false);
                            if (attrs != null && attrs.Length > 0)
                            {
                                ECARules4AllAttribute attr = (ECARules4AllAttribute)attrs[0];

                                writer.Write("the ");
                                writer.Write(objType);
                                writer.Write(" ");
                                writer.Write(gameObj.name);
                                writer.Write(" has a ");
                                writer.Write(attr.Name);
                                writer.WriteLine(";");
                                writer.Flush();

                            }
                        }
                   
                        
                    }
                }
                    
            }
        }

        private void PrintRule(Rule r, TextWriter writer)
        {
            // write event
            writer.Write("when ");
            PrintAction(r.GetEvent(), writer);
            writer.WriteLine();

            if(r.GetCondition() != null)
            {
                writer.Write("if   ");
                PrintCondition(r.GetCondition(), writer);
                writer.WriteLine();
            }
            

            // write actions
            writer.Write("then ");

            foreach(Action a in r.GetActions())
            {
                PrintAction(a, writer);
                writer.WriteLine(";");
                writer.Write("     ");
                writer.Flush();
            }
            writer.WriteLine();
            writer.WriteLine();
        }

        private void PrintAction(Action a, TextWriter writer)
        {
            writer.Write("the ");
            writer.Write(GetTypeName(GetECAObjectType(a.GetSubject())));
            writer.Write(" ");
            writer.Write(a.GetSubject().name);
            writer.Write(" ");
            writer.Write(a.GetActionMethod());
            writer.Write(" ");
            if(a.GetPreposition() != null)
            {
                writer.Write(a.GetPreposition());
                writer.Write(" ");
            }

            if (a.GetObject() is GameObject)
            {
                GameObject gameObject = (GameObject)a.GetObject();
                writer.Write("the ");
                writer.Write(GetTypeName(GetECAObjectType(gameObject)));
                writer.Write(" ");
                writer.Write(gameObject.name);
                writer.Write(" ");
            }
            if (a.GetModifier() != null)
            {
                writer.Write(a.GetModifier());
                writer.Write(" ");
            }

            if (a.GetModifierValue() != null)
            {
                writer.Write(a.GetModifierValue());
                writer.Write(" ");
            }
        }

        private void PrintCondition(Condition c, TextWriter writer)
        {
            if(c is CompositeCondition){
                PrintCompositeCondition(c as CompositeCondition, writer);
                
            }

            if(c is SimpleCondition)
            {
                PrintSimpleCondition(c as SimpleCondition, writer);
                
            }
        }

        private void PrintCompositeCondition(CompositeCondition c, TextWriter writer)
        {
            if (c.Op == CompositeCondition.ConditionType.NOT)
            {
                writer.Write(" not ");
            }

            if (c.GetParent() != null)
            {
                writer.Write("(");
            }



            int count = c.ChildrenCount() - 1;
            foreach(Condition child in c.Children())
            {
                PrintCondition(child, writer);
                if(count > 0)
                {
                   PrintOp(c.Op, writer);
                }
                
                count--;
            }

            if (c.GetParent() != null)
            {
                writer.Write(")");
            }
            
        }

        private void PrintOp(CompositeCondition.ConditionType op, TextWriter writer)
        {
            switch (op)
            {
               
                case CompositeCondition.ConditionType.AND:
                    writer.Write(" and ");
                    break;

                case CompositeCondition.ConditionType.OR:
                    writer.Write(" or ");
                    break;

                default:
                    break;

            }
        }

        private void PrintSimpleCondition(SimpleCondition c, TextWriter writer)
        {
            writer.Write("the ");
            writer.Write(GetTypeName(GetECAObjectType(c.GetSubject())));
            writer.Write(" ");
            writer.Write(c.GetSubject().name);
            writer.Write(" ");
            if(c.GetProperty() != null)
            {
                writer.Write(c.GetProperty());
                writer.Write(" ");
            }
            writer.Write(c.GetSymbol());
            writer.Write(" ");
            if(c.GetValueToCompare() is GameObject)
            {
                GameObject gameObject = (GameObject)c.GetValueToCompare();
                writer.Write("the ");
                writer.Write(GetTypeName(GetECAObjectType(gameObject)));
                writer.Write(" ");
                writer.Write(gameObject.name);
                writer.Write(" ");
            }
            else
            {
                writer.Write(c.GetValueToCompare());
            }
        }
    }
}
