﻿using System.Collections;
using UnityEngine;
using ECARules4All.RuleEngine;
using ECARules4All;
using UnityEditor;

[ECARules4All("character")]
[RequireComponent(typeof(ECAObject))]
public class Character : MonoBehaviour
{
    [StateVariable("life", ECARules4AllType.Float)] public float life;
    [StateVariable("playing", ECARules4AllType.Boolean)] public bool playing;
    private bool isBusyMoving = false;

    [Action(typeof(Character), "interacts with", typeof(ECAObject))]
    public void Interacts(ECAObject o)
    {
        //no need for implementation, see Interactable class (behaviour)
    }

    [Action(typeof(Character), "jumps to", typeof(Position))]
    public void Jumps(Position p)
    {
        float speed = 5.0F;
        Vector3 endMarker = new Vector3(p.x, p.y, p.z);
        StartCoroutine(MoveObject(speed, endMarker));
    }

    private IEnumerator MoveObject( float speed, Vector3 endMarker)
    {
        isBusyMoving = true;
        Vector3 startMarker = gameObject.transform.position;
        float startTime = Time.time;
        float journeyLength = Vector3.Distance(startMarker, endMarker);
        while (gameObject.transform.position != endMarker)
        {
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.

            gameObject.transform.position = Vector3.Lerp(startMarker, endMarker, fractionOfJourney);
            GetComponent<ECAObject>().p.Assign(gameObject.transform.position);

            yield return null;
        }
        GetComponent<ECAObject>().p.Assign(gameObject.transform.position);
        isBusyMoving = false;
    }

    [Action(typeof(Character), "jumps on", typeof(Path))]
    public void Jumps(Path p)
    {
        StartCoroutine(WaitForOrderedMovement(p));
    }

    private IEnumerator WaitForOrderedMovement(Path p)
    {
        foreach (Position pos in p.Points)
        {
            while (isBusyMoving)
            {
                yield return null;
            }

            Jumps(pos);
        }
    }


    //In order to make it works the template builder must specify a trigger that is directly connected to
    //the idle state and brings to the correct animation
    //TODO verb on parser is "starts-animation" but it should be "starts animation"
    [Action(typeof(Character), "starts animation", typeof(string))]
    public void StartsAnimation(string s)
    {
        Animator anim = this.gameObject.GetComponent<Animator>();
        anim.SetTrigger(s);
        anim.ResetTrigger(s);
    }
}