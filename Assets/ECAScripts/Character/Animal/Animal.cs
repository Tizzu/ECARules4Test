﻿using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("animal")]
[RequireComponent(typeof(Character))] //gerarchia 
public class Animal : MonoBehaviour
{
    [Action(typeof(Animal), "speaks", typeof(string))]
    public void Speaks(string s)
    {
        AudioSource audio = this.gameObject.GetComponent<AudioSource>();
        audio.clip = (AudioClip)Resources.Load(s);
        audio.Play();
    }

}
