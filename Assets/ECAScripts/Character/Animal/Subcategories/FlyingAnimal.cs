﻿using System.Collections;
using ECARules4All;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("flying-animal")]
[RequireComponent(typeof(Animal))]
public class FlyingAnimal : MonoBehaviour
{
 private bool isBusyMoving = false;

    [Action(typeof(FlyingAnimal), "flies to", typeof(Position))]
    public void Flies(Position p)
    {
        float speed = 5.0F;
        Vector3 endMarker = new Vector3(p.x, p.y, p.z);
        StartCoroutine(MoveObject(speed, endMarker));
    }

    [Action(typeof(FlyingAnimal), "flies on", typeof(Path))]
    public void Flies(Path p)
    {
        StartCoroutine(WaitForOrderedMovement(p, "flies"));
    }
    [Action(typeof(FlyingAnimal), "walks to", typeof(Position))]
    public void Walks(Position p)
    {
        float speed = 1.0F;
        Vector3 endMarker = new Vector3(p.x, p.y, p.z);
        StartCoroutine(MoveObject(speed, endMarker));

    }

    [Action(typeof(FlyingAnimal), "walks on", typeof(Path))]
    public void Walks(Path p)
    {
        StartCoroutine(WaitForOrderedMovement(p, "walks"));
    }
    
    private IEnumerator MoveObject( float speed, Vector3 endMarker)
    {
        isBusyMoving = true;
        Vector3 startMarker = gameObject.transform.position;
        float startTime = Time.time;
        float journeyLength = Vector3.Distance(startMarker, endMarker);
        while (gameObject.transform.position != endMarker)
        {
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.

            gameObject.transform.position = Vector3.Lerp(startMarker, endMarker, fractionOfJourney);
            GetComponent<ECAObject>().p.Assign(gameObject.transform.position);
            yield return null;
        }
        GetComponent<ECAObject>().p.Assign(gameObject.transform.position);
        isBusyMoving = false;
    }
    
    private IEnumerator WaitForOrderedMovement(Path p, string method)
    {
        foreach (Position pos in p.Points)
        {
            while (isBusyMoving)
            {
                yield return null;
            }

            switch (method)
            {
                case "flies": Flies(pos);
                    break;
                case "walks": Walks(pos);
                    break;
            }

        }
    }
    
}
