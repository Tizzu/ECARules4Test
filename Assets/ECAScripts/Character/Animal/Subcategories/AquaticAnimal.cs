﻿using System.Collections;
using ECARules4All;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("aquatic-animal")]
[RequireComponent(typeof(Animal))]
public class AquaticAnimal : MonoBehaviour
{
 private bool isBusyMoving = false;

    [Action(typeof(AquaticAnimal), "swims to", typeof(Position))]
    public void Swims(Position p)
    {
        float speed = 0.5F;
        Vector3 endMarker = new Vector3(p.x, p.y, p.z);
        StartCoroutine(MoveObject(speed, endMarker));
    }

    [Action(typeof(AquaticAnimal), "swims on", typeof(Path))]
    public void Swims(Path p)
    {
        StartCoroutine(WaitForOrderedMovement(p, "swims"));
    }

    private IEnumerator MoveObject( float speed, Vector3 endMarker)
    {
        isBusyMoving = true;
        Vector3 startMarker = gameObject.transform.position;
        float startTime = Time.time;
        float journeyLength = Vector3.Distance(startMarker, endMarker);
        while (gameObject.transform.position != endMarker)
        {
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.

            gameObject.transform.position = Vector3.Lerp(startMarker, endMarker, fractionOfJourney);
            GetComponent<ECAObject>().p.Assign(gameObject.transform.position);
            yield return null;
        }
        GetComponent<ECAObject>().p.Assign(gameObject.transform.position);
        isBusyMoving = false;
    }
    
    private IEnumerator WaitForOrderedMovement(Path p, string method)
    {
        foreach (Position pos in p.Points)
        {
            while (isBusyMoving)
            {
                yield return null;
            }

            switch (method)
            {
                case "swims": Swims(pos);
                    break;
            }

        }
    }
}