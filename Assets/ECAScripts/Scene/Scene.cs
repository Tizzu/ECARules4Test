﻿using ECARules4All;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("scene")]
[RequireComponent(typeof(ECAObject))]
public class Scene : MonoBehaviour
{
    //DOUBT: Aggiunto in questo file nome e posizione della scena di arrivo, può avere senso?
    [StateVariable("name", ECARules4AllType.Text)] public string name;
    [StateVariable("position", ECARules4AllType.Position)] public Position position;
}