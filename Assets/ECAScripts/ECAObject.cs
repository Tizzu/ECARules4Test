﻿using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ECARules4All.RuleEngine
{
    [ECARules4All("object")]
    public class ECAObject : MonoBehaviour
    {
        private Collider[] gameCollider;
        private Renderer[] gameRenderer;
        private bool isBusyMoving = false;
        
        [StateVariable("position", ECARules4AllType.Position)]
        public Position p;
        [StateVariable("rotation", ECARules4AllType.Rotation)]
        public Rotation r;
        [StateVariable("visible", ECARules4AllType.Boolean)] 
        public bool isVisible = true;
        [StateVariable("active", ECARules4AllType.Boolean)]
        public bool isActive = true;

        [Action(typeof(ECAObject), "moves to", typeof(Position))]
        public void Moves(Position newPos)
        {
            float speed = 1.0F;
            Vector3 endMarker = new Vector3(newPos.x, newPos.y, newPos.z);
            StartCoroutine(MoveObject(speed, endMarker));
        }
        [Action(typeof(ECAObject), "rotates towards", typeof(Rotation))]
        public void Rotates(Rotation newRot) { }
        [Action(typeof(ECAObject), "looks at", typeof(Object))]
        public void Looks(Object o) { }
        
        //TODO: queste funzioni, potrebbero risolvere il problema dei check nell'update
        [Action(typeof(ECAObject), "changes", "visible", typeof(bool))]
        public void ShowsHides(bool yesNo)
        {
            isVisible = yesNo;
            UpdateVisibility();
        }

        [Action(typeof(ECAObject), "changes", "active", typeof(bool))]
        public void ActivatesDeactivates(bool yesNo)
        {
            isActive = yesNo;
            UpdateVisibility();
        }

        private void Awake()
        {
            gameCollider = this.gameObject.GetComponents<Collider>();
            gameRenderer = this.gameObject.GetComponents<Renderer>();

            if (gameRenderer.Length == 0)
                gameRenderer = this.gameObject.GetComponentsInChildren<Renderer>();
            
            if (gameCollider.Length == 0)
                gameCollider = this.gameObject.GetComponentsInChildren<Collider>();
            
            p = new Position();
            r = new Rotation();
        }

        private void UpdateVisibility()
        {
            foreach (Collider c in gameCollider)
            {
                c.enabled = isActive;
            }

            foreach (Renderer r in gameRenderer)
            {
                if (!isActive || !isVisible)
                {
                    r.enabled = false;
                }
                else r.enabled = true;
            }
        }
        
        private IEnumerator MoveObject(float speed, Vector3 endMarker)
        {
            isBusyMoving = true;
            Vector3 startMarker = gameObject.transform.position;
            float startTime = Time.time;
            float journeyLength = Vector3.Distance(startMarker, endMarker);
            while (gameObject.transform.position != endMarker)
            {
                float distCovered = (Time.time - startTime) * speed;

                // Fraction of journey completed equals current distance divided by total distance.
                float fractionOfJourney = distCovered / journeyLength;

                // Set our position as a fraction of the distance between the markers.

                gameObject.transform.position = Vector3.Lerp(startMarker, endMarker, fractionOfJourney);
                GetComponent<ECAObject>().p.Assign(gameObject.transform.position);
                yield return null;
            }
            GetComponent<ECAObject>().p.Assign(gameObject.transform.position);
            isBusyMoving = false;
        }

    }
}