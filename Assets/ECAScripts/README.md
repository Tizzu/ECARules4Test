This folder contains the core files for instantiating the Rule Engine inside any template, plus a few examples of Objects

* [Core Elements](#core-elements)
* [How to Install](#how-to-install)
* [How to send Events](#how-to-send-events)

# Core Elements
These files have to be imported all together to get the engine up and running.
* **RuleEngine**: This file contains all the code that implements the Rule Engine itself, the Event Bus and the *Rule*, *Action* and *Condition*. The file is also contains documentation comments, in order to suggest code completion where supported;
* **RuleEngineLoader**: This file instantiates at runtime the Rule Engine and the Event Bus, in order to be sure that they are properly running from the start;
* **\[ ECARules4All | Action | StateVariable \]Attribute**: These files are the custom attributes needed to decorate the Objects so the Rule Engine can address their variables and functions;
* **ECAObject**: The root Object from which all the other Objects can derive;
* **Objects**: Contains definitions for *Position, Rotation, Path, Time* and *Color* classes;
* **ECARules4AllType**: This file will contain extra informations intended for the Rule Editor UI, as of now it's here for predisposition.

# How to install
The installation of the Rule Engine is simple and it consists in two parts, Unity-side and Project-side

**Unity Side**
* Inside the Unity main window go to **Edit > Project Settings...**
* On the left side of the window press **Player**
* Scroll down the right side of the window until you arrive at the **Configuration** subsection of the **Other Settings** section
* Change **Api Compatibility Level** to **.NET 4.x**

**Project Side**
* Create a new Unity project (or use an existing one)
* Import the core files somewhere in the project assets directory
* Create an empty GameObject and add the **RuleEngineLoader** script to it


# How to send Events
Whenever you need to fire an event to the Rule Engine you can do it by using 
 `EventBus.GetInstance().Publish(new Action(/*Action Syntax here*/))`

> Be sure to include (or have included by Unity) the following line of code at the start of your script:
> 
> `using ECARules4All.RuleEngine;`

The **Action** declaration is structured like the following:

| Name | Mandatory? | Description |
|---|---|---|
 |**Subject**|Yes|Always a GameObject, it tells who made the action in the scene|
 |**Verb** |Yes| A string that describes the action made by the subject|
 |**Object**|No| Additional values (e.g.: a GameObject, a string, a primitive type, etc.) used as parameters inside a function, or to define a variable to modify (with the *Quantity* field)|
 |**Quantity**|No[^1]|This field defines the value used to alter (by assignment or decrement/increment) the value indicated in the *Object* field. |

# How to add Rules
If you need to add Rules the syntax for adding one is the following:

`RuleEngine.GetInstance().Add(new Rule(/*Rule Syntax Here*/))`

The Rule declaration is structured like the following:

| Name | Mandatory? | Type | Description |
|---|---|---|---|
|**When**|Yes|`Action`|An Action that defines what needs to happen for this Rule to trigger|
|**If**|No|`Condition`|A Condition to be satisfied in order to execute the events in the *Then* list|
|**Then**|Yes|`List<Action>`|The list of actions that this rule will trigger|

[^1]: It is if modifying a value
