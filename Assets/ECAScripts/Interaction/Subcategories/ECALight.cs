using System;
using System.Collections;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("light")]
[RequireComponent(typeof(Interaction), typeof(Light))] //gerarchia 
public class ECALight : MonoBehaviour
{
    [StateVariable("intensity", ECARules4AllType.Float)] public float intensity = 1;
    [StateVariable("maxIntensity", ECARules4AllType.Float)] public float maxIntensity = 10;
    [StateVariable("color", ECARules4AllType.Color)] public Color color = new Color(1, 0.95686271f, 0.8392157f, 1);
    [StateVariable("on", ECARules4AllType.Boolean)] public bool on;
    private Light componentLight;
    [Action(typeof(ECALight), "turns", typeof(bool))]
    public void Turns(bool on)
    {
        this.on = on;
        componentLight.color = color;
    }
    //TODO:possibile conflitto tra grammatica e chiamata del metodo
    [Action(typeof(ECALight), "increases intensity by", typeof(float))]
    public void IncreasesIntensity(float amount)
    {
        if (intensity + amount < maxIntensity)
        {
            intensity += amount;
            GetComponent<Light>().intensity = intensity;
        }
        else
        {
            intensity = maxIntensity;
            GetComponent<Light>().intensity = intensity;
        }
    }

    //TODO:possibile conflitto tra grammatica e chiamata del metodo
    [Action(typeof(ECALight), "decreases intensity by", typeof(float))]
    public void DecreasesIntensity(float amount)
    {
        if (intensity - amount > 0)
        {
            intensity -= amount;
            GetComponent<Light>().intensity = intensity;
        }
        else
        {
            intensity = 0;
            GetComponent<Light>().intensity = intensity;
        }
    }
    
    //TODO:possibile conflitto tra grammatica e chiamata del metodo
    [Action(typeof(ECALight), "sets intensity to", typeof(float))]
    public void SetsIntensity(float i)
    {
        if (i >= maxIntensity)
        {
            GetComponent<Light>().intensity = maxIntensity;
        }
        else
        {
            GetComponent<Light>().intensity = i;
        }
    }

    //TODO:possibile conflitto tra grammatica e chiamata del metodo
    [Action(typeof(ECALight), "changes color to", typeof(string))]
    public void ChangesColor(string hexa)
    {
        if (ColorUtility.TryParseHtmlString(hexa, out color))
        {
            componentLight.color = color;
        }
        
    }

    public void Start()
    {
        componentLight = GetComponent<Light>();
        componentLight.color = color;
        intensity = intensity > 10.0f ? 10.0f : intensity;
        componentLight.intensity = intensity;
    }
}
