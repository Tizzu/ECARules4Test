using System.Collections;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("bounds")]
[RequireComponent(typeof(Interaction))] //gerarchia 
public class Bounds : MonoBehaviour
{
    [StateVariable("scale", ECARules4AllType.Float)] public float scale;

    //TODO: possibile conflitto tra metodo implementato e meccanica di serializzazione dell'azione
    [Action(typeof(Bounds), "scales to", typeof(float))]
    public void Scales(float newScale)
    {
        scale = newScale;
        //TODO: should probably scale the transform
    }
}
