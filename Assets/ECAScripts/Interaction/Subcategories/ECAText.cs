using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("text")]
[RequireComponent(typeof(Interaction))] //gerarchia 
public class ECAText : MonoBehaviour
{
    [StateVariable("content", ECARules4AllType.Text)] public string content;
    
    //TODO: possibile conflitto tra grammatica e chiamata del metodo
    [Action(typeof(ECAText), "changes content to", typeof(string))]
    public void ChangesContent(string content)
    {
        this.content = content;
    }
    
    [Action(typeof(ECAText), "appends", typeof(string))]
    public void Appends(string t)
    {
        this.content += t;
    }
    
    [Action(typeof(ECAText), "deletes", typeof(string))]
    public void Deletes(string t)
    {
        content = content.Replace (t, ""); // to replace the specific text with blank
    }
}
