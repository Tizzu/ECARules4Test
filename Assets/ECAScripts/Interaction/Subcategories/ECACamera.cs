using System;
using System.Collections;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("camera")]
[RequireComponent(typeof(Interaction), typeof(Camera))] //gerarchia 
public class ECACamera : MonoBehaviour
{
    private Camera camera;
    //In vr la 3a persona è una telecamera dall'alto fissa
    //TODO: Nel parser non ci sono regole che hanno un parametro per modificare zoom in/out
    public enum POV{First, Third};
    [StateVariable("pov", ECARules4AllType.Identifier)] public POV pov;
    [StateVariable("zoomLevel", ECARules4AllType.Float)] public float zoomLevel = 60;
    [StateVariable("playing", ECARules4AllType.Boolean)] public bool playing;
    
    //TODO: conflict between grammar and documentation, float parameter only in documentation
    [Action(typeof(ECACamera), "zooms-in", typeof(float))]
    public void ZoomsIn(float amount)
    {
        if(zoomLevel - amount > 30)
        {
            zoomLevel -= amount;
        }
        else
        {
            zoomLevel = 30;
        }
        camera.fieldOfView = zoomLevel;
    }
    //TODO: conflict between grammar and documentation, float parameter only in documentation
    [Action(typeof(ECACamera), "zooms-out", typeof(float))]
    public void ZoomsOut(float amount)
    {
        if(zoomLevel + amount < 100)
        {
            zoomLevel += amount;
        }
        else
        {
            zoomLevel = 100;
        }
        camera.fieldOfView = zoomLevel;
    }
    
    //TODO: conflict between grammar and documentation, float parameter only in documentation
    [Action(typeof(ECACamera), "changes POV to", typeof(POV))]
    public void ChangesPOV(POV pov)
    {
        this.pov = pov;
    }
    
    private void Start()
    {
        camera = GetComponent<Camera>();
        if (zoomLevel > 100) zoomLevel = 100;
        else if (zoomLevel < 30) zoomLevel = 30;
        camera.fieldOfView = zoomLevel;
    }
}
