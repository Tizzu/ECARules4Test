using System.Collections;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("image")]
[RequireComponent(typeof(Interaction))] //gerarchia 

public class ECAImage : MonoBehaviour
{
    [StateVariable("source", ECARules4AllType.Float)] public float source;
    [StateVariable("width", ECARules4AllType.Float)] public float width;
    [StateVariable("height", ECARules4AllType.Float)] public float height;
}
