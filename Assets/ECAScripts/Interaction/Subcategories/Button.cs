using System.Collections;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("button")]
[RequireComponent(typeof(Interaction))] //gerarchia 
public class Button : MonoBehaviour
{
    [Action(typeof(Character), "pushes", typeof(Button))]
    public void _Presses(Character c)
    {
        //no need for implementation, see Interactable class (behaviour)
    }
}
