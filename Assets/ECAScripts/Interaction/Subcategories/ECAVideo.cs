using System;
using System.Collections;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using UnityEngine;
using UnityEngine.Video;

[ECARules4All("video")]
[RequireComponent(typeof(Interaction), typeof(VideoPlayer))] //gerarchia 

public class ECAVideo : MonoBehaviour
{
    [StateVariable("source", ECARules4AllType.Text)] public string source;
    [StateVariable("volume", ECARules4AllType.Float)] public float volume;
    [StateVariable("maxVolume", ECARules4AllType.Float)] public float maxVolume = 1;
    //TODO: duration e currentTime, abbiamo cambiato il tipo da time a double
    [StateVariable("duration", ECARules4AllType.Float)] public double duration;
    [StateVariable("currentTime", ECARules4AllType.Float)] public double currentTime;
    [StateVariable("playing", ECARules4AllType.Boolean)] public bool playing;
    [StateVariable("paused", ECARules4AllType.Boolean)] public bool paused;
    [StateVariable("stopped", ECARules4AllType.Boolean)] public bool stopped;
    private VideoPlayer player;
    
    [Action(typeof(ECAVideo), "plays")]
    public void Plays()
    {
        this.playing = true;
        this.stopped = false;
        this.paused = false;
        player.Play();
    }
    [Action(typeof(ECAVideo), "pauses")]
    public void Pauses()
    {
        this.playing = false;
        this.stopped = false;
        this.paused = true;
        player.Pause();
    }
    [Action(typeof(ECAVideo), "stops")]
    public void Stops()
    {
        this.playing = false;
        this.stopped = true;
        this.paused = false;
        currentTime = 0;
        player.Stop();
    }
    
    //TODO: possibile conflitto tra grammatica e chiamata di funzione
    [Action(typeof(ECAVideo), "changes volume to", typeof(float))]
    public void ChangesVolume(float v)
    {
        if (v > maxVolume)
        {
            v = maxVolume;
        }

        if (v < 0)
        {
            v = 0;
        }
        volume = v;
        player.SetDirectAudioVolume(0, volume);
        //trackindex is set to 0, but there may be more than 1 audio track
    }
    
    //TODO: possibile conflitto tra grammatica e chiamata di funzione
    [Action(typeof(ECAVideo), "changes current time to", typeof(double))]
    public void ChangesCurrentTime(double c)
    {
        if (c <= duration)
        {
            var frameRate = player.frameRate;
            var seek = (frameRate * c);
            player.frame = (long)(seek);
        }
    }

    private void Update()
    {
        if (playing)
        {
            currentTime = (float)player.time;
        }
    }

    private void Start()
    {
        player = GetComponent<VideoPlayer>();
        duration = player.length;
    }
}
