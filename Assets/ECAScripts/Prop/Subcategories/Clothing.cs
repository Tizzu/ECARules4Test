﻿using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("clothing")]
[RequireComponent(typeof(Prop))] //gerarchia 

public class Clothing : MonoBehaviour
{
    [StateVariable("brand", ECARules4AllType.Text)] public string brand;
    [StateVariable("color", ECARules4AllType.Color)] public Color color;
    [StateVariable("size", ECARules4AllType.Text)] public string size;
    [StateVariable("weared", ECARules4AllType.Boolean)] public bool weared;

    [Action(typeof(Character), "wears", typeof(Clothing))]
    //_ perchè passivo
    public void _Wears(Character c)
    {
        //TODO gestire lo stato e l'effettiva vestizione
        weared = true;
    }
    
    [Action(typeof(Character), "unwears", typeof(Clothing))]
    //_ perchè passivo
    public void _Unwears(Character c)
    {
        weared = false;
        //TODO gestire lo stato e l'effettiva vestizione
    }
}
