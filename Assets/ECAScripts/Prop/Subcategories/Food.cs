﻿using System;
using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("food")]
[RequireComponent(typeof(Prop))]
public class Food: MonoBehaviour
{
    [StateVariable("weight", ECARules4AllType.Float)] public float weight;
    [StateVariable("expiration", ECARules4AllType.Time)] public DateTime expiration;
    [StateVariable("description", ECARules4AllType.Text)] public string description;
    [StateVariable("eaten", ECARules4AllType.Boolean)] public bool eaten;

    //il character mangia il cibo
    [Action(typeof(Character), "eats", typeof(Food))]
    public void _Eats(Character c)
    {
        GetComponent<ECAObject>().isActive = false;
        eaten = true;
    }

}