﻿using System;
using ECARules4All.RuleEngine;
using UnityEngine; 


[ECARules4All("electronic")]
[RequireComponent(typeof(Prop))]
public class Electronic: MonoBehaviour
{
    [StateVariable("brand", ECARules4AllType.Text)] public string brand; 
    [StateVariable("model", ECARules4AllType.Text)] public string model;
    [StateVariable("on", ECARules4AllType.Boolean)] public bool on;
    public ParticleSystem turnParticle;
    private GameObject particlePrefab;

    //l'oggetto elettronico si accende/spegne
    [Action(typeof(Electronic), "turns", typeof(bool))]
    public void Turns(bool on)
    {
        this.@on = on;
        
        if (on && turnParticle)
        {
            turnParticle.Stop();
            turnParticle.Play();
        }
        else if(!on && turnParticle)
        {
            turnParticle.Stop();
        }
    }

    private void Start()
    {
        if (turnParticle == null)
        {
            particlePrefab = Instantiate(Resources.Load("Particles/Particle_Turns"), transform) as GameObject;
            turnParticle = particlePrefab.GetComponent<ParticleSystem>();
        }
        else
        {
            Instantiate(turnParticle, transform);
            turnParticle = transform.Find(turnParticle.name + "(Clone)").GetComponent<ParticleSystem>();
        }
        

        if (on)
        {
            turnParticle.Play();
        }
        else
        {
            turnParticle.Stop();
        }
    }
}