﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
[ECARules4All("firearm")]
[RequireComponent(typeof(Weapon))]

    public class Firearm : MonoBehaviour
    {
        public ParticleSystem particleFire;
        public ParticleSystem particleRecharge;
        private GameObject particleRechargePrefab;
        private GameObject particleFirePrefab;
        
        public GameObject bulletPrefab;
        
        [StateVariable("charge", ECARules4AllType.Integer)] public int charge;

        [Action(typeof(Firearm), "recharges", typeof(int))]
        public void Recharges(int charge)
        {
            this.charge = charge;
            
            if (particleRecharge == null)
            {
                particleRechargePrefab = Instantiate(Resources.Load("Particles/Particle_Recharge"), transform) as GameObject;
                particleRecharge = particleRechargePrefab.GetComponent<ParticleSystem>();
            }
            else
            {
                Instantiate(particleRecharge, transform);
                particleRecharge = transform.Find(particleRecharge.name + "(Clone)").GetComponent<ParticleSystem>();
            }
            particleRecharge.Stop();
            particleRecharge.Play();
        }
        
        [Action(typeof(Firearm), "fires", typeof(ECAObject))]
        public void Fires(ECAObject obj)
        {
            if (charge > 0)
            {
                charge -= 1;
                if (bulletPrefab != null)
                {
                    GameObject bulletInstance;
                    bulletInstance = Instantiate(bulletPrefab, transform.position, transform.rotation) as GameObject;
                    bulletInstance.GetComponent<Rigidbody>().AddForce(transform.forward * bulletPrefab.GetComponent<Bullet>().speed);
                }
                
                if (particleFire == null)
                {
                    particleFirePrefab = Instantiate(Resources.Load("Particles/Particle_Fire"), transform) as GameObject;
                    particleFire = particleFirePrefab.GetComponent<ParticleSystem>();
                }
                else
                {
                    Instantiate(particleFire, transform);
                    particleFire = transform.Find(particleFire.name + "(Clone)").GetComponent<ParticleSystem>();
                }
                particleFire.Stop();
                particleFire.Play();
                
                
                if (transform.Find(particleFire.name + "(Clone)"))
                {
                    transform.Find(particleFire.name + "(Clone)").GetComponent<ParticleSystem>().Stop();
                    transform.Find(particleFire.name + "(Clone)").GetComponent<ParticleSystem>().Play();
                }
                else
                {
                    Instantiate(particleFire, transform);
                    transform.Find(particleFire.name + "(Clone)").GetComponent<ParticleSystem>().Play();
                }
            }
        }
        
        [Action(typeof(Firearm), "aims", typeof(ECAObject))]
        public void Aims(ECAObject obj)
        {
            transform.LookAt(obj.transform);
            this.GetComponent<ECAObject>().p.Assign(transform.position);
            this.GetComponent<ECAObject>().r.Assign(transform.rotation);
        }
        
    }
