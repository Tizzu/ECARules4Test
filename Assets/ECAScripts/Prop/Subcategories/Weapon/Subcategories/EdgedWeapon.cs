﻿using UnityEngine;
using ECARules4All.RuleEngine;

[ECARules4All("edged-weapon")]
[RequireComponent(typeof(Weapon))]

public class EdgedWeapon:MonoBehaviour
{
    public ParticleSystem particleStab;
    public ParticleSystem particleSlice;
    private GameObject particleStabPrefab = null;
    private GameObject particleSlicePrefab = null;
    [Action(typeof(EdgedWeapon), "stabs", typeof(ECAObject))]
    public void Stabs(ECAObject obj)
    {
        Character c = obj.gameObject.GetComponent<Character>();
        Weapon w = this.gameObject.GetComponent<Weapon>();
        if (c != null && w != null)
        {
            c.life -= w.power;
            if (c.life < 0)
                c.life = 0;
        }

        if (particleStab == null)
        {
            particleStabPrefab = Instantiate(Resources.Load("Particles/Particle_Stab"), transform) as GameObject;
            particleStab = particleStabPrefab.GetComponent<ParticleSystem>();
        }
        else
        {
            Instantiate(particleStab, transform);
            particleStab = transform.Find(particleStab.name + "(Clone)").GetComponent<ParticleSystem>();
        }
        particleStab.Stop();
        particleStab.Play();
    }
    
    [Action(typeof(EdgedWeapon), "slices", typeof(ECAObject))]
    public void Slices(ECAObject obj)
    {
        Character c = obj.gameObject.GetComponent<Character>();
        Weapon w = this.gameObject.GetComponent<Weapon>();
        if (c != null && w != null)
        {
            c.life -= w.power;
            if (c.life < 0)
                c.life = 0;
        }

        if (particleSlice == null)
        {
            particleSlicePrefab = Instantiate(Resources.Load("Particles/Particle_Slice"), transform) as GameObject;
            particleSlice = particleSlicePrefab.GetComponent<ParticleSystem>();
        }
        else
        {
            Instantiate(particleSlice, transform);
            particleSlice = transform.Find(particleSlice.name + "(Clone)").GetComponent<ParticleSystem>();
        }
        particleSlice.Stop();
        particleSlice.Play();
    }
}