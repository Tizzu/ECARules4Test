﻿using System;
using UnityEngine;
using ECARules4All.RuleEngine;
using UnityEngine;
[ECARules4All("bullet")]
[RequireComponent(typeof(Weapon))]
public class Bullet : MonoBehaviour
{
    [StateVariable("speed", ECARules4AllType.Float)] public float speed;
    //FUTURE: eventualmente avere un'evento per quando colpisce qualcosa
    

    private void OnCollisionEnter(Collision other)
    {
        Destroy(gameObject);
    }
}