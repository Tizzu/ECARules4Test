﻿using UnityEngine;
using ECARules4All.RuleEngine;

[ECARules4All("shield")]
[RequireComponent(typeof(Weapon))]
public class Shield : MonoBehaviour
{
    [Action(typeof(Shield), "blocks", typeof(Weapon))]
    public void Blocks(Weapon weapon)
    {
        Character c = GetComponentInParent<Character>();
        if (c != null)
        {
            c.life -= weapon.power / 3;
        }
    }
}