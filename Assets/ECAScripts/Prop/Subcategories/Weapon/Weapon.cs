﻿using ECARules4All.RuleEngine;
using UnityEngine;

[ECARules4All("weapon")]
[RequireComponent(typeof(Prop))] //gerarchia 
public class Weapon:MonoBehaviour
{
    [StateVariable("power", ECARules4AllType.Float)] public float power;
}