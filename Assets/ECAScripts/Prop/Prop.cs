using System.Collections;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using UnityEngine;


[ECARules4All("prop")]
[RequireComponent(typeof(ECAObject))] //gerarchia 
public class Prop : MonoBehaviour
{
    [StateVariable("price", ECARules4AllType.Float)] public float price;
}
