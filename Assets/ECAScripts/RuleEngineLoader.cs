﻿using System;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using UnityEngine;
using Action = ECARules4All.RuleEngine.Action;

public class RuleEngineLoader : MonoBehaviour
{
    private RuleEngine ruleEngine;
    private EventBus eventBus;

    private void Start()
    {
        ruleEngine = RuleEngine.GetInstance();
        eventBus = EventBus.GetInstance();
    }
}
