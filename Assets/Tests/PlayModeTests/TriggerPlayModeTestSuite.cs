using System.Collections;
using System.Collections.Generic;
using ECARules4All;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class TriggerPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Trigger1";
    }

    private const string triggersVerb = "triggers";
    
    [UnityTest]
    public IEnumerator TriggersPassingSubjectVerbAction()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Triggers()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the container script
        var triggerScriptReference = myObject.GetComponent<Trigger>();

        // Creating the action/event
        var sub1 = GameObject.Find("/Container1");
        const string verb1 = "empties";

        var eventAction = new Action(sub1, verb1);

        // Getting the reference of the script
        var obj1 = GameObject.Find("/Switch1");
        var subScriptReference = obj1.GetComponent<Switch>();

        // Creating the actions to be performed after the event occurs 
        const string verb2 = "turns";
        var value1 = !subScriptReference.@on;
        var effect1 = new Action(obj1, verb2, value1);
        var actions = new List<Action> {effect1};

        // Creating the rule (event -> actions)
        var rule = new Rule(eventAction, actions);
        // Add = Publish
        RuleEngine.GetInstance().Add(rule);

        // Triggering the event
        // triggerScriptReference.Triggers(eventAction);
        Action action = new Action(myObject, triggersVerb, eventAction);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait
        yield return new WaitForSeconds(ShortTimeDelay);

        // Check if the actions has been performed correctly
        Assert.IsTrue(subScriptReference.@on == value1);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TriggersPassingSubjectVerbObjectAction()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Triggers()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the container script
        var triggerScriptReference = myObject.GetComponent<Trigger>();

        // Creating the action/event
        var sub1 = GameObject.Find("/Container1");
        const string verb1 = "interacts with";
        var obj1 = GameObject.Find("/Switch1");
        var eventAction = new Action(sub1, verb1, obj1);

        // Getting the reference of the script
        var subScriptReference = obj1.GetComponent<Switch>();

        // Creating the actions to be performed after the event occurs 
        const string verb2 = "turns";
        var value1 = !subScriptReference.@on;
        var effect1 = new Action(obj1, verb2, value1);
        var actions = new List<Action> {effect1};

        // Creating the rule (event -> actions)
        var rule = new Rule(eventAction, actions);
        // Add = Publish
        RuleEngine.GetInstance().Add(rule);

        // Triggering the event
        // triggerScriptReference.Triggers(eventAction);
        Action action = new Action(myObject, triggersVerb, eventAction);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait
        yield return new WaitForSeconds(ShortTimeDelay);
        // Check if the actions has been performed correctly
        Assert.IsTrue(subScriptReference.@on == value1);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TriggersPassingSubjectVerbValueAction()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Triggers()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the container script
        var triggerScriptReference = myObject.GetComponent<Trigger>();

        // Creating the action/event
        var sub1 = GameObject.Find("/Container1");
        const string verb1 = "moves to";
        var value = new Position(10, 10, 10);
        var eventAction = new Action(sub1, verb1, value);

        // Getting the reference of the script
        var obj1 = GameObject.Find("/Switch1");
        var subScriptReference = obj1.GetComponent<Switch>();

        // Creating the actions to be performed after the event occurs 
        const string verb2 = "turns";
        var value1 = !subScriptReference.@on;
        var effect1 = new Action(obj1, verb2, value1);
        var actions = new List<Action> {effect1};

        // Creating the rule (event -> actions)
        var rule = new Rule(eventAction, actions);
        // Add = Publish
        RuleEngine.GetInstance().Add(rule);

        // Triggering the event
        // triggerScriptReference.Triggers(eventAction);
        Action action = new Action(myObject, triggersVerb, eventAction);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait
        yield return new WaitForSeconds(ShortTimeDelay);

        // Check if the actions has been performed correctly
        Assert.IsTrue(subScriptReference.@on == value1);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TriggersPassingSubjectChangesPropertyToValueAction()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Triggers()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the container script
        var triggerScriptReference = myObject.GetComponent<Trigger>();

        // Creating the action/event
        var sub1 = GameObject.Find("/Counter1");
        const string verb1 = "changes count to";
        const int value = 10;
        var eventAction = new Action(sub1, verb1, value);

        // Getting the reference of the script
        var obj1 = GameObject.Find("/Switch1");
        var subScriptReference = obj1.GetComponent<Switch>();

        // Creating the actions to be performed after the event occurs 
        const string verb2 = "turns";
        var value1 = !subScriptReference.@on;
        var effect1 = new Action(obj1, verb2, value1);
        var actions = new List<Action> {effect1};

        // Creating the rule (event -> actions)
        var rule = new Rule(eventAction, actions);
        // Add = Publish
        RuleEngine.GetInstance().Add(rule);
        
        // Triggering the event
        // triggerScriptReference.Triggers(eventAction);
        Action action = new Action(myObject, triggersVerb, eventAction);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait
        yield return new WaitForSeconds(ShortTimeDelay);

        // Check if the actions has been performed correctly
        Assert.IsTrue(subScriptReference.@on == value1);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}