using System;
using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class KeypadPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Keypad1";
    }

    private const string addsVerb = "adds";
    
    [UnityTest]
    public IEnumerator InsertsPassingNullString()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Inserts()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();

        // Saving the imput before calling the method
        var inputBefore = keypadScriptReference.input;

        // Jumps() call
        Debug.Log($"Before calling {methodNameToTest})");
        try
        {
            // keypadScriptReference.Inserts( null);
            Action action = new Action(myObject, insertsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreEqual(inputBefore, inputAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator InsertsPassingNotNullString()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Inserts()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();

        // creating the input to pass
        const string inputToPass = "inputPassed";
        if (keypadScriptReference.input == inputToPass)
            keypadScriptReference.input += "1";
        
        // method call
        Debug.Log($"Before calling {methodNameToTest})");
        // keypadScriptReference.Inserts( inputToPass);
        Action action = new Action(myObject, insertsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreEqual(inputToPass, inputAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator InsertsPassingWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Inserts()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();
        myObject.GetComponent<ECAObject>().isActive = false;
        
        // creating the input to pass
        const string inputToPass = "inputPassed";
        if (keypadScriptReference.input == inputToPass)
            keypadScriptReference.input += "1";

        var inputBefore = keypadScriptReference.input;
        
        // method call
        Debug.Log($"Before calling {methodNameToTest})");
        // keypadScriptReference.Inserts( inputToPass);
        Action action = new Action(myObject, insertsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreNotEqual(inputToPass, inputAfter);
        Assert.AreEqual(inputBefore, inputAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator AddsPassingNullString()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Adds()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();

        // Saving the imput before calling the method
        var inputBefore = keypadScriptReference.input;

        // Jumps() call
        Debug.Log($"Before calling {methodNameToTest})");
        try
        {
            // keypadScriptReference.Add(null);
            Action action = new Action(myObject, addsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreEqual(inputBefore, inputAfter);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator AddsPassingNotNullString()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Adds()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();

        // saving the input before
        var inputBefore = keypadScriptReference.input;
        
        // creating the input to pass
        const string inputToPass = "inputPassed";

        // method call
        Debug.Log($"Before calling {methodNameToTest})");
        // keypadScriptReference.Add( inputToPass);
        Action action = new Action(myObject, addsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreEqual(inputBefore+inputToPass, inputAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator AddsPassingWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Adds()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();
        myObject.GetComponent<ECAObject>().isActive = false;
        
        // saving the input before
        var inputBefore = keypadScriptReference.input;
        
        // creating the input to pass
        const string inputToPass = "inputPassed";

        // method call
        Debug.Log($"Before calling {methodNameToTest})");
        // keypadScriptReference.Add(inputToPass);
        Action action = new Action(myObject, addsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreNotEqual(inputBefore+inputToPass, inputAfter);
        Assert.AreEqual(inputBefore, inputAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ResetsWhenInputIsEmptyString()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Resets()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();

        keypadScriptReference.input = "";
        
        var inputBefore = keypadScriptReference.input;
        
        // method call
        Debug.Log($"Before calling {methodNameToTest})");
        // keypadScriptReference.Resets();
        Action action = new Action(myObject, resetsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreEqual(inputBefore, inputAfter);
        Assert.AreEqual(inputAfter, "");

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);

    }

    [UnityTest]
    public IEnumerator ResetsWhenInputIsNotEmptyString()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Resets()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();

        // creating the input to pass
        if (String.IsNullOrEmpty(keypadScriptReference.input))
            keypadScriptReference.input = "1";
        
        var inputBefore = keypadScriptReference.input;
        
        // method call
        Debug.Log($"Before calling {methodNameToTest})");
        // keypadScriptReference.Resets();
        Action action = new Action(myObject, resetsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreNotEqual(inputBefore, inputAfter);
        Assert.AreEqual(inputAfter, "");

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);

    }
    
    [UnityTest]
    public IEnumerator ResetsWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Resets()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var keypadScriptReference = myObject.GetComponent<Keypad>();
        myObject.GetComponent<ECAObject>().isActive = false;
        
        // saving the input before the method call
        const string inputToPass = "1234534";
        var inputBefore = keypadScriptReference.input = inputToPass;
        
        // method call
        Debug.Log($"Before calling {methodNameToTest})");
        // keypadScriptReference.Resets();
        Action action = new Action(myObject, resetsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the input after the method call
        var inputAfter = keypadScriptReference.input;

        // Check if the input didn't change are the same
        Assert.AreEqual(inputBefore, inputAfter);
        Assert.AreNotEqual(inputAfter, "");

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);

    }
}