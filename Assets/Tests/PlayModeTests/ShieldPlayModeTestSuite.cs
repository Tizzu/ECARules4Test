using System;
using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class ShieldPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/HumanWithShield1";
    }

    [UnityTest]
    public IEnumerator LoadingBlocksRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"ShieldBlocksRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }

    private const string blocksVerb = "blocks";
    
    [UnityTest]
    public IEnumerator BlocksPassingNullWeapon()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Blocks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var shieldScriptReference = myObject.GetComponent<Shield>();
        var characterScriptReference = myObject.GetComponent<Character>();

        // Saving the life
        var lifeBefore = characterScriptReference.life;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");

        try
        {
            //shieldScriptReference.Blocks(null);
            Action action = new Action(myObject, blocksVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the life after the block
        var lifeAfter = characterScriptReference.life;

        // Check if the life is the same
        Assert.AreEqual(lifeBefore, lifeAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator BlocksPassingWeaponWithPositiveDamage()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Blocks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var shieldScriptReference = myObject.GetComponent<Shield>();
        var characterScriptReference = myObject.GetComponent<Character>();

        // getting attacker
        var attacker = GameObject.Find("EdgedWeapon");
        var attackerScriptReference = attacker.GetComponent<Weapon>();

        const float attackerPower = 5;
        const float hpBeforeAttack = 10;
        const float blockerPower = 1;
        
        // Setting the attacker power stat to be greater than zero 
        attackerScriptReference.power = attackerPower;
        // Setting the blocker power
        myObject.GetComponent<Weapon>().power = blockerPower;
        // Setting the character life
        characterScriptReference.life = hpBeforeAttack;
        
        // Saving the life
        var lifeBefore = characterScriptReference.life;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // shieldScriptReference.Blocks(attackerScriptReference);
        Action action = new Action(myObject, blocksVerb, attacker);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the life after the block
        var lifeAfter = characterScriptReference.life;

        // Check if the life is the same
        Assert.Less(lifeAfter, lifeBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator BlocksPassingWeaponWithNullDamage()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Blocks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var shieldScriptReference = myObject.GetComponent<Shield>();
        var characterScriptReference = myObject.GetComponent<Character>();

        // getting attacker
        var attacker = GameObject.Find("EdgedWeapon");
        var attackerScriptReference = attacker.GetComponent<Weapon>();
        
        const float attackerPower = 0;
        const float hpBeforeAttack = 10;
        const float blockerPower = 1;
        
        // Setting the attacker power stat to be equal to zero 
        attackerScriptReference.power = attackerPower;
        // Setting the blocker power
        myObject.GetComponent<Weapon>().power = blockerPower;
        // Setting the character life
        characterScriptReference.life = hpBeforeAttack;

        // Saving the life
        var lifeBefore = characterScriptReference.life;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // shieldScriptReference.Blocks(attackerScriptReference);
        Action action = new Action(myObject, blocksVerb, attacker);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the life after the block
        var lifeAfter = characterScriptReference.life;

        // Check if the life is the same
        Assert.AreEqual(lifeAfter, lifeBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator BlocksPassingWeaponWithNegativeDamage()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Blocks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var shieldScriptReference = myObject.GetComponent<Shield>();
        var characterScriptReference = myObject.GetComponent<Character>();

        // getting attacker
        var attacker = GameObject.Find("EdgedWeapon");
        var attackerScriptReference = attacker.GetComponent<Weapon>();
        
        const float attackerPower = -5;
        const float hpBeforeAttack = 10;
        const float blockerPower = 1;
        
        // Setting the attacker power stat to be smaller than zero 
        attackerScriptReference.power = attackerPower;
        // Setting the blocker power
        myObject.GetComponent<Weapon>().power = blockerPower;
        // Setting the character life
        characterScriptReference.life = hpBeforeAttack;

        // Saving the life
        var lifeBefore = characterScriptReference.life;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // shieldScriptReference.Blocks(attackerScriptReference);
        Action action = new Action(myObject, blocksVerb, attacker);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the life after the block
        var lifeAfter = characterScriptReference.life;

        // Check if the life is the same
        Assert.AreEqual(lifeBefore, lifeAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator BlocksTestingIfShieldPowerLowersDamageTaken()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Blocks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var shieldScriptReference = myObject.GetComponent<Shield>();
        var characterScriptReference = myObject.GetComponent<Character>();

        // getting attacker
        var attacker = GameObject.Find("EdgedWeapon");
        var attackerScriptReference = attacker.GetComponent<Weapon>();
        
        const float attackerPower = 5;
        const float hpBeforeAttack = 10;
        const float highBlockerPower = 5;
        const float lowBlockerPower = 1;
        
        //////////////// 1° attack: low defence ////////////////
        // Setting the attacker power stat
        attackerScriptReference.power = attackerPower;
        // Setting the blocker power for the first attack
        myObject.GetComponent<Weapon>().power = lowBlockerPower;
        // Setting the character life
        characterScriptReference.life = hpBeforeAttack;

        // Saving the life
        var lifeBefore = characterScriptReference.life;

        // Method call: FIRST ATTACK => LOW DEFENCE STAT
        Debug.Log($"Before calling {methodNameToTest}");
        // shieldScriptReference.Blocks(attackerScriptReference);
        Action action = new Action(myObject, blocksVerb, attacker);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the life after the block
        var lifeAfter = characterScriptReference.life;

        // Check if the life is the same
        Assert.Less(lifeAfter, lifeBefore);
        var damageTakenWithLowDefence = lifeBefore - lifeAfter;
        ////////////////////////////////////////////////
        
        
        //////////////// 2° attack: high defence ////////////////
        // Setting the attacker power stat
        attackerScriptReference.power = attackerPower;
        // Setting the blocker power for the first attack
        myObject.GetComponent<Weapon>().power = highBlockerPower;
        // Setting the character life
        characterScriptReference.life = hpBeforeAttack;

        // Saving the life
        lifeBefore = characterScriptReference.life;

        // Method call: FIRST ATTACK => LOW DEFENCE STAT
        Debug.Log($"Before calling {methodNameToTest}");
        // shieldScriptReference.Blocks(attackerScriptReference);
        action = new Action(myObject, blocksVerb, attacker);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the life after the block
        lifeAfter = characterScriptReference.life;

        // Check if the life is the same
        Assert.Less(lifeAfter, lifeBefore);
        var damageTakenWithHighDefence = lifeBefore - lifeAfter;
        ////////////////////////////////////////////////
        
        // Chck if 
        Assert.Greater(damageTakenWithLowDefence, damageTakenWithHighDefence);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator BlocksTestingWhenShieldPowerIsGreaterThanAttackerDamage()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Blocks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var shieldScriptReference = myObject.GetComponent<Shield>();
        var characterScriptReference = myObject.GetComponent<Character>();

        // getting attacker
        var attacker = GameObject.Find("EdgedWeapon");
        var attackerScriptReference = attacker.GetComponent<Weapon>();
        
        const float attackerPower = 5;
        const float hpBeforeAttack = 10;
        const float blockerPower = 10;
        
        // Setting the attacker power stat to be equal to zero 
        attackerScriptReference.power = attackerPower;
        // Setting the blocker power
        myObject.GetComponent<Weapon>().power = blockerPower;
        // Setting the character life
        characterScriptReference.life = hpBeforeAttack;

        // Saving the life
        var lifeBefore = characterScriptReference.life;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // shieldScriptReference.Blocks(attackerScriptReference);
        Action action = new Action(myObject, blocksVerb, attacker);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the life after the block
        var lifeAfter = characterScriptReference.life;

        // Check if the life is the same
        Assert.LessOrEqual(lifeAfter, lifeBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}