using System.Collections;
using System;
using System.Collections.Generic;
using ECARules4All;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class CharacterTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Character1";
    }

    private const string interactsVerb = "interacts with";
    private const string jumpPosVerb = "jumps to";
    private const string jumpPathVerb = "jumps on";
    private const string startsAnimationVerb = "starts animation";
    
    
    [UnityTest]
    public IEnumerator InteractsPassingNullObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Interacts()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var characterScriptReference = myObject.GetComponent<Character>();
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        var ecaObjectPositionBefore = new Position();
        ecaObjectPositionBefore.Assign(ecaObjectScriptReference.p);
        var positionBefore = myObject.transform.position;

        // Interacts() call
        Debug.Log($"Before calling {methodNameToTest})");
        try
        {
            // characterScriptReference.Interacts(null);
            Action action = new Action(myObject, interactsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaObjectPositionAfter = new Position();
        ecaObjectPositionAfter.Assign(ecaObjectScriptReference.p);
        var positionAfter = myObject.transform.position;

        // Check if the positions are the same
        PositionsAreEqual(ecaObjectPositionBefore, ecaObjectPositionAfter);
        Assert.AreEqual(positionBefore, positionAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
        yield return null;
    }

    [UnityTest]
    public IEnumerator JumpsPassingNullPosition()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Jumps(Position)";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var characterScriptReference = myObject.GetComponent<Character>();
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Saving the position before calling the method
        var ecaObjectPositionBefore = new Position();
        ecaObjectPositionBefore.Assign(ecaObjectScriptReference.p);
        var positionBefore = myObject.transform.position;

        // Jumps() call
        Debug.Log($"Before calling {methodNameToTest})");
        try
        {
            // characterScriptReference.Jumps((Position) null);
            Action action = new Action(myObject, jumpPosVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaObjectPositionAfter = new Position();
        ecaObjectPositionAfter.Assign(ecaObjectScriptReference.p);
        var positionAfter = myObject.transform.position;

        // Check if the positions are the same
        PositionsAreEqual(ecaObjectPositionBefore, ecaObjectPositionAfter);
        Assert.AreEqual(positionBefore, positionAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator JumpsPassingNullPath()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Jump(Path)";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var characterScriptReference = myObject.GetComponent<Character>();
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Saving the position before calling the method
        var ecaObjectPositionBefore = new Position();
        ecaObjectPositionBefore.Assign(ecaObjectScriptReference.p);
        var positionBefore = myObject.transform.position;

        // Jumps() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            Action action = new Action(myObject, jumpPathVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
            // characterScriptReference.Jumps((Path) null);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaObjectPositionAfter = new Position();
        ecaObjectPositionAfter.Assign(ecaObjectScriptReference.p);
        var positionAfter = myObject.transform.position;

        // Check if the positions are the same
        PositionsAreEqual(ecaObjectPositionBefore, ecaObjectPositionAfter);
        Assert.AreEqual(positionBefore, positionAfter);


        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator StartsAnimationPassingNullTriggerName()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "StartsAnimation()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var characterScriptReference = myObject.GetComponent<Character>();

        // Saving the status of the animator before the method call
        var animator = myObject.GetComponent<Animator>();
        var beforeStateHashList = new HashSet<int>();
        for (var i = 0; i < animator.layerCount; i++)
        {
            beforeStateHashList.Add(animator.GetCurrentAnimatorStateInfo(i).fullPathHash);
        }

        Debug.Log($"I saved the status of the animator before the method call");

        /*// Saving the status of the audio source before the method call
        var audioSource = myObject.GetComponent<AudioSource>();
        var beforeAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source before the method call");*/

        // Saving the status of the parameters list before the method call
        var beforeParametersDictionary = new Dictionary<int, int>();
        foreach (var param in animator.parameters)
        {
            var hashCodeVar = param.type switch
            {
                AnimatorControllerParameterType.Bool => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Trigger => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Float => animator.GetFloat(param.name).GetHashCode(),
                AnimatorControllerParameterType.Int => animator.GetInteger(param.name).GetHashCode(),
                _ => -1
            };
            beforeParametersDictionary.Add(param.nameHash, hashCodeVar);
        }

        Debug.Log($"I saved the status of the parameters list before the method call");

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // characterScriptReference.StartsAnimation(null);
            Action action = new Action(myObject, startsAnimationVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the status of the animator after the method call
        var afterStateHashList = new HashSet<int>();
        for (var i = 0; i < animator.layerCount; i++)
        {
            afterStateHashList.Add(animator.GetCurrentAnimatorStateInfo(i).fullPathHash);
        }

        Debug.Log($"I saved the status of the animator after the method call");

        /*// Saving the status of the audio source after the method call
        var afterAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source after the method call");*/

        // Saving the status of the parameters list before the method call
        var afterParametersDictionary = new Dictionary<int, int>();
        foreach (var param in animator.parameters)
        {
            var hashCodeVar = param.type switch
            {
                AnimatorControllerParameterType.Bool => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Trigger => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Float => animator.GetFloat(param.name).GetHashCode(),
                AnimatorControllerParameterType.Int => animator.GetInteger(param.name).GetHashCode(),
                _ => -1
            };
            afterParametersDictionary.Add(param.nameHash, hashCodeVar);
        }

        Debug.Log($"I saved the status of the parameters list after the method call");

        // Check if the animator status didn't change
        Assert.IsTrue(beforeStateHashList.SetEquals(afterStateHashList));
        // Check if the parameters status didn't change
        CollectionAssert.AreEquivalent(beforeParametersDictionary, afterParametersDictionary);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator StartsAnimationPassingNonExistingTriggerName()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "StartsAnimation()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var characterScriptReference = myObject.GetComponent<Character>();

        // getting the reference to the animator
        var animator = myObject.GetComponent<Animator>();

        // Setting the trigger to pass: it must not exist
        var triggerNameToPass = "triggerThatMustNotExist";
        bool triggerAlreadyExist;
        do
        {
            triggerAlreadyExist = false;
            foreach (var param in animator.parameters)
            {
                if (!param.name.Equals(triggerNameToPass)) continue;

                triggerAlreadyExist = true;
                triggerNameToPass += "a";
                break;
            }
        } while (triggerAlreadyExist);

        // Saving the status of the animator before the method call
        var beforeStateHashList = new HashSet<int>();
        for (var i = 0; i < animator.layerCount; i++)
        {
            beforeStateHashList.Add(animator.GetCurrentAnimatorStateInfo(i).fullPathHash);
        }

        Debug.Log($"I saved the status of the animator before the method call");

        // Saving the status of the parameters list before the method call
        var beforeParametersDictionary = new Dictionary<int, int>();
        foreach (var param in animator.parameters)
        {
            var hashCodeVar = param.type switch
            {
                AnimatorControllerParameterType.Bool => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Trigger => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Float => animator.GetFloat(param.name).GetHashCode(),
                AnimatorControllerParameterType.Int => animator.GetInteger(param.name).GetHashCode(),
                _ => -1
            };
            beforeParametersDictionary.Add(param.nameHash, hashCodeVar);
        }

        Debug.Log($"I saved the status of the parameters list before the method call");

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // characterScriptReference.StartsAnimation(triggerNameToPass);
            Action action = new Action(myObject, startsAnimationVerb, triggerNameToPass);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the status of the animator after the method call
        var afterStateHashList = new HashSet<int>();
        for (var i = 0; i < animator.layerCount; i++)
        {
            afterStateHashList.Add(animator.GetCurrentAnimatorStateInfo(i).fullPathHash);
        }

        Debug.Log($"I saved the status of the animator after the method call");

        // Saving the status of the parameters list before the method call
        var afterParametersDictionary = new Dictionary<int, int>();
        foreach (var param in animator.parameters)
        {
            var hashCodeVar = param.type switch
            {
                AnimatorControllerParameterType.Bool => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Trigger => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Float => animator.GetFloat(param.name).GetHashCode(),
                AnimatorControllerParameterType.Int => animator.GetInteger(param.name).GetHashCode(),
                _ => -1
            };
            afterParametersDictionary.Add(param.nameHash, hashCodeVar);
        }

        Debug.Log($"I saved the status of the parameters list after the method call");

        // Check if the animator status didn't change
        Assert.IsTrue(beforeStateHashList.SetEquals(afterStateHashList));
        // Check if the parameters status didn't change
        CollectionAssert.AreEquivalent(beforeParametersDictionary, afterParametersDictionary);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator StartsAnimationPassingExistingTriggerName()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "StartsAnimation()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var characterScriptReference = myObject.GetComponent<Character>();

        // getting the reference to the animator
        var animator = myObject.GetComponent<Animator>();

        // Setting the trigger to pass: it must not exist
        var triggerNameToPass = "";
        var hasFound = false;
        foreach (var param in animator.parameters)
        {
            if (param.type != AnimatorControllerParameterType.Trigger) continue;

            triggerNameToPass = param.name;
            hasFound = true;
            break;
        }
        
        const string baseTriggerNameIfNotFound = "TriggerTestabc";
        var animatorController = (AnimatorController) animator.runtimeAnimatorController;
        if (!hasFound)
        {
            triggerNameToPass = baseTriggerNameIfNotFound;
            animatorController.AddParameter(triggerNameToPass,
                AnimatorControllerParameterType.Trigger);
        }

        /* not needed
        // Saving the status of the animator before the method call
        var beforeStateHashList = new HashSet<int>();
        for (var i = 0; i < animator.layerCount; i++)
        {
            beforeStateHashList.Add(animator.GetCurrentAnimatorStateInfo(i).fullPathHash);
        }
        Debug.Log($"I saved the status of the animator before the method call");
        */
        yield return new WaitForSeconds(ShortTimeDelay);
        
        // Saving the status of the parameters list before the method call without the trigger to pass
        var beforeParametersDictionary = new Dictionary<int, int>();
        foreach (var param in animator.parameters)
        {
            if (param.name.Equals(triggerNameToPass)) continue;

            var hashCodeVar = param.type switch
            {
                AnimatorControllerParameterType.Bool => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Trigger => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Float => animator.GetFloat(param.name).GetHashCode(),
                AnimatorControllerParameterType.Int => animator.GetInteger(param.name).GetHashCode(),
                _ => -1
            };
            beforeParametersDictionary.Add(param.nameHash, hashCodeVar);
        }
        Debug.Log($"I saved the status of the parameters list before the method call");

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // characterScriptReference.StartsAnimation(triggerNameToPass);
        Action action = new Action(myObject, startsAnimationVerb, triggerNameToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        /* Not needed
        // Saving the status of the animator after the method call
        var afterStateHashList = new HashSet<int>();
        for (var i = 0; i < animator.layerCount; i++)
        {
            afterStateHashList.Add(animator.GetCurrentAnimatorStateInfo(i).fullPathHash);
        }
        Debug.Log($"I saved the status of the animator after the method call");
        */

        // Saving the status of the parameters list before the method call without the param passed
        var afterParametersDictionary = new Dictionary<int, int>();
        foreach (var param in animator.parameters)
        {
            if (param.name.Equals(triggerNameToPass)) continue;

            var hashCodeVar = param.type switch
            {
                AnimatorControllerParameterType.Bool => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Trigger => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Float => animator.GetFloat(param.name).GetHashCode(),
                AnimatorControllerParameterType.Int => animator.GetInteger(param.name).GetHashCode(),
                _ => -1
            };
            afterParametersDictionary.Add(param.nameHash, hashCodeVar);
        }
        Debug.Log($"I saved the status of the parameters list after the method call");

        // Check if the parameters status (excluded the trigger passed) didn't change
        CollectionAssert.AreEquivalent(beforeParametersDictionary, afterParametersDictionary);
        // Check if the trigger passed is triggered
        var isTriggered = animator.GetBool(triggerNameToPass);
        if (!hasFound)
        {
            var index = -1;
            for (var i = 0; i < animatorController.parameters.Length; i++)
            {
                var anim = animatorController.parameters[i];
                if (!anim.name.Equals(triggerNameToPass)) continue;
                index = i;
                break;
            }
            animatorController.RemoveParameter(index);
        }
        
        if(!isTriggered)
            Assert.Fail();
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
     [UnityTest]
    public IEnumerator StartsAnimationWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "StartsAnimation()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        myObject.GetComponent<ECAObject>().isActive = false;
        var characterScriptReference = myObject.GetComponent<Character>();

        // getting the reference to the animator
        var animator = myObject.GetComponent<Animator>();

        // Setting the trigger to pass: it must not exist
        var triggerNameToPass = "";
        var hasFound = false;
        foreach (var param in animator.parameters)
        {
            if (param.type != AnimatorControllerParameterType.Trigger) continue;

            triggerNameToPass = param.name;
            hasFound = true;
            break;
        }
        
        const string baseTriggerNameIfNotFound = "TriggerTestabc";
        var animatorController = (AnimatorController) animator.runtimeAnimatorController;
        if (!hasFound)
        {
            triggerNameToPass = baseTriggerNameIfNotFound;
            animatorController.AddParameter(triggerNameToPass,
                AnimatorControllerParameterType.Trigger);
        }

        // Saving the status of the animator before the method call
        var beforeStateHashList = new HashSet<int>();
        for (var i = 0; i < animator.layerCount; i++)
        {
            beforeStateHashList.Add(animator.GetCurrentAnimatorStateInfo(i).fullPathHash);
        }
        Debug.Log($"I saved the status of the animator before the method call");
        
        yield return new WaitForSeconds(ShortTimeDelay);
        
        // Saving the status of the parameters list before the method call without the trigger to pass
        var beforeParametersDictionary = new Dictionary<int, int>();
        foreach (var param in animator.parameters)
        {
            //if (param.name.Equals(triggerNameToPass)) continue;

            var hashCodeVar = param.type switch
            {
                AnimatorControllerParameterType.Bool => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Trigger => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Float => animator.GetFloat(param.name).GetHashCode(),
                AnimatorControllerParameterType.Int => animator.GetInteger(param.name).GetHashCode(),
                _ => -1
            };
            beforeParametersDictionary.Add(param.nameHash, hashCodeVar);
        }
        Debug.Log($"I saved the status of the parameters list before the method call");

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // characterScriptReference.StartsAnimation(triggerNameToPass);
        Action action = new Action(myObject, startsAnimationVerb, triggerNameToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        yield return new WaitForSeconds(ShortTimeDelay);
        
        // Saving the status of the animator after the method call
        var afterStateHashList = new HashSet<int>();
        for (var i = 0; i < animator.layerCount; i++)
        {
            afterStateHashList.Add(animator.GetCurrentAnimatorStateInfo(i).fullPathHash);
        }
        Debug.Log($"I saved the status of the animator after the method call");


        // Saving the status of the parameters list before the method call without the param passed
        var afterParametersDictionary = new Dictionary<int, int>();
        foreach (var param in animator.parameters)
        {
            var hashCodeVar = param.type switch
            {
                AnimatorControllerParameterType.Bool => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Trigger => animator.GetBool(param.name).GetHashCode(),
                AnimatorControllerParameterType.Float => animator.GetFloat(param.name).GetHashCode(),
                AnimatorControllerParameterType.Int => animator.GetInteger(param.name).GetHashCode(),
                _ => -1
            };
            afterParametersDictionary.Add(param.nameHash, hashCodeVar);
        }
        Debug.Log($"I saved the status of the parameters list after the method call");
        
        
        // Check if the trigger passed is triggered
        if (!hasFound)
        {
            var index = -1;
            for (var i = 0; i < animatorController.parameters.Length; i++)
            {
                var anim = animatorController.parameters[i];
                if (!anim.name.Equals(triggerNameToPass)) continue;
                index = i;
                break;
            }
            animatorController.RemoveParameter(index);
        }
        // Check if the parameters status (excluded the trigger passed) didn't change
        CollectionAssert.AreEquivalent(beforeParametersDictionary, afterParametersDictionary);

        // Check if the animator status didn't change
        Assert.IsTrue(beforeStateHashList.SetEquals(afterStateHashList));

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}