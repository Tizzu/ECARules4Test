using System;
using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class AnimalPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Animal1";
    }

    private const string speaksVerb = "speaks";
    [UnityTest]
    public IEnumerator SpeaksPassingNullAudioClipName()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Speaks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Animal script
        var animalScriptReference = myObject.GetComponent<Animal>();

        // Saving the status of the audio source before the method call
        var audioSource = myObject.GetComponent<AudioSource>();
        var beforeAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source before the method call");

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // animalScriptReference.Speaks(null);
            Action action = new Action(myObject, speaksVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);  
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the status of the audio source after the method call
        var afterAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source after the method call");

        // Check if the status are the same
        Assert.AreEqual(beforeAudioSourceClipHashCode, afterAudioSourceClipHashCode);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator SpeaksPassingExistingAudioClipName()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Speaks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Animal script
        var animalScriptReference = myObject.GetComponent<Animal>();
        var audioSource = myObject.GetComponent<AudioSource>();

        // Saving the status of the audio source before the method call
        var beforeAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        var beforeAudioSourceClipName = audioSource.clip.name;
        Debug.Log($"I saved the status of the audio source before the method call");
        
        const string clipNameToPass =  "clipAudioForTest";

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        //animalScriptReference.Speaks(clipNameToPass);
        Action action = new Action(myObject, speaksVerb, "Tests/"+clipNameToPass);
        RuleEngine.GetInstance().ExecuteAction(action);  
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the status of the audio source after the method call
        var afterAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        var afterAudioSourceClipName = audioSource.clip.name;
        Debug.Log($"I saved the status of the audio source after the method call");

        // Check if the status are the same
        Assert.AreNotEqual(beforeAudioSourceClipHashCode, afterAudioSourceClipHashCode);
        Assert.AreEqual(clipNameToPass, afterAudioSourceClipName);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator SpeaksWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Speaks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Animal script
        var animalScriptReference = myObject.GetComponent<Animal>();
        myObject.GetComponent<ECAObject>().isActive = false;
        var audioSource = myObject.GetComponent<AudioSource>();

        // Saving the status of the audio source before the method call
        var beforeAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source before the method call");
        
        const string clipNameToPass =  "clipAudioForTest";

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        //animalScriptReference.Speaks(clipNameToPass);
        Action action = new Action(myObject, speaksVerb, "Tests/"+clipNameToPass);
        RuleEngine.GetInstance().ExecuteAction(action);  
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the status of the audio source after the method call
        var afterAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source after the method call");

        // Check if the status are the same
        Assert.AreEqual(beforeAudioSourceClipHashCode, afterAudioSourceClipHashCode);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator SpeaksPassingNonExistingAudioClipName()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Speaks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Animal script
        var animalScriptReference = myObject.GetComponent<Animal>();
        myObject.GetComponent<ECAObject>().isActive = false;
        var audioSource = myObject.GetComponent<AudioSource>();

        // Saving the status of the audio source before the method call
        var beforeAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source before the method call");

        const string clipNameToPass = "ItDoesNotExist";

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // animalScriptReference.Speaks(clipNameToPass);
        Action action = new Action(myObject, speaksVerb, clipNameToPass);
        RuleEngine.GetInstance().ExecuteAction(action); 
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the status of the audio source after the method call
        var afterAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source after the method call");

        // Check if the status are the same
        Assert.AreEqual(beforeAudioSourceClipHashCode, afterAudioSourceClipHashCode);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    /*[UnityTest]
    public IEnumerator SpeaksPassingExistingClipName()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Speaks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Animal script
        var animalScriptReference = myObject.GetComponent<Animal>();
        myObject.GetComponent<ECAObject>().isActive = false;
        var audioSource = myObject.GetComponent<AudioSource>();

        // Saving the status of the audio source before the method call
        var beforeAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source before the method call");

        const string clipNameToPass = "ItDoesNotExist";

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        animalScriptReference.Speaks(clipNameToPass);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the status of the audio source after the method call
        var afterAudioSourceClipHashCode = audioSource.clip.GetHashCode();
        Debug.Log($"I saved the status of the audio source after the method call");

        // Check if the status are the same
        Assert.AreEqual(clipNameToPass,audioSource.clip.name);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }*/
}