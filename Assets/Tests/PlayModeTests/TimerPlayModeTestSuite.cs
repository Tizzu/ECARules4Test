using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.TestTools;

public class TimerPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Timer1";
    }

    private const string changesDurationVerb = "changes duration to";
    private const string changesCurrentTimeVerb = "changes current time to";

    [UnityTest]
    public IEnumerator ChangesDurationPassingNullAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesDuration()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var timerScriptReference = myObject.GetComponent<Timer>();

        // creating the duration to pass
        const float durationToPass = 0f;

        // Saving the position before calling the method
        var durationBefore = timerScriptReference.duration = 5f;

        // Rotates() call
        Debug.Log($"Before calling {methodNameToTest}");
        // timerScriptReference.ChangeDuration(durationToPass);
        Action action = new Action(myObject, changesDurationVerb, durationToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the position after calling the method
        var durationAfter = timerScriptReference.duration;

        // Check if the positions are the same
        Assert.AreEqual(durationAfter, durationToPass);
        Assert.AreNotEqual(durationAfter, durationBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ChangesDurationPassingPositiveAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesDuration()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var timerScriptReference = myObject.GetComponent<Timer>();

        // creating the duration to pass
        const float durationToPass = 25f;

        // Saving the position before calling the method
        var durationBefore = timerScriptReference.duration = 5f;

        // Rotates() call
        Debug.Log($"Before calling {methodNameToTest}");
        // timerScriptReference.ChangeDuration(durationToPass);
        Action action = new Action(myObject, changesDurationVerb, durationToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Saving the position after calling the method
        var durationAfter = timerScriptReference.duration;

        // Check if the positions are the same
        Assert.AreEqual(durationAfter, durationToPass);
        Assert.AreNotEqual(durationAfter, durationBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator StartsCheckIfTimePasses()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Starts()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var timerScriptReference = myObject.GetComponent<Timer>();

        // creating the duration to pass
        const float durationToPass = 10f;
        // timerScriptReference.Stops();
        Action action = new Action(myObject, stopsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        timerScriptReference.duration = timerScriptReference.current = durationToPass;

        // Saving the position before calling the method
        var currentBefore = timerScriptReference.current;
        // timerScriptReference.Starts();
        action = new Action(myObject, startsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // timerScriptReference.Starts();
        action = new Action(myObject, startsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(MediumTimeDelay);

        // Saving the position after calling the method
        var currentAfter = timerScriptReference.current;

        // Check
        Assert.That(currentBefore - currentAfter, Is.EqualTo(MediumTimeDelay).Using(new FloatComparer(0.1f)));
        Assert.That(currentAfter, Is.LessThan(currentBefore));

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator StopsCheckIfTimeDoesNotPass()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Stops()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var timerScriptReference = myObject.GetComponent<Timer>();

        // creating the duration to pass
        const float durationToPass = 10f;
        // timerScriptReference.Stops();
        Action action = new Action(myObject, stopsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        timerScriptReference.duration = timerScriptReference.current = durationToPass;

        // Starting
        // timerScriptReference.Starts();
        action = new Action(myObject, startsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        yield return new WaitForSeconds(MediumTimeDelay);

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // timerScriptReference.Stops();
        action = new Action(myObject, stopsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        var currentImmediatelyAfterStops = timerScriptReference.current;
        Debug.Log($"After calling {methodNameToTest}");

        // Saving the position after calling the method
        yield return new WaitForSeconds(MediumTimeDelay);
        var currentAfterWhile = timerScriptReference.current;

        // Check
        Assert.That(currentImmediatelyAfterStops - currentAfterWhile,
            Is.EqualTo(0).Using(FloatComparer.s_ComparerWithDefaultTolerance));

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ResetsCheckIfTimerResetsCurrentTime()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Resets()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var timerScriptReference = myObject.GetComponent<Timer>();

        // creating the duration to pass
        const float durationToPass = 10f;
        // timerScriptReference.Stops();
        Action action = new Action(myObject, stopsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        timerScriptReference.duration = timerScriptReference.current = durationToPass;

        // Starting
        // timerScriptReference.Starts();
        action = new Action(myObject, startsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        yield return new WaitForSeconds(MediumTimeDelay);

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        var currentBefore = timerScriptReference.current;
        // timerScriptReference.Resets();
        action = new Action(myObject, resetsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Saving the position after calling the method
        yield return new WaitForSeconds(MediumTimeDelay);
        var currentAfter = timerScriptReference.current;

        // Check
        Assert.That(currentAfter,
            Is.EqualTo(durationToPass).Using(FloatComparer.s_ComparerWithDefaultTolerance));
        Assert.That(currentAfter, Is.GreaterThan(currentBefore));


        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}