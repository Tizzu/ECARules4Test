using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class ElectronicPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Electronic1";
    }

    [UnityTest]
    public IEnumerator LoadingTurnsRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"ElectronicTurnsRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }
    
    [UnityTest]
    public IEnumerator TurnsPassingTrueWhenStatusIsTrue()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var electronicScriptReference = myObject.GetComponent<Electronic>();

        // Forcing the status to true
        electronicScriptReference.@on = true;
        
        // Setting the bool to pass
        const bool inputToPass = true;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // electronicScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;
        
        // Check if the current status is equivalent to that passed
        Assert.IsTrue(inputToPass == electronicScriptReference.@on);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator TurnsPassingTrueWhenStatusIsFalse()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var electronicScriptReference = myObject.GetComponent<Electronic>();

        // Forcing the status to false
        electronicScriptReference.@on = false;
        
        // Setting the bool to pass
        const bool inputToPass = true;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;
        
        // Check if the current status is equivalent to that passed
        Assert.IsTrue(inputToPass == electronicScriptReference.@on);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator TurnsPassingFalseWhenStatusIsTrue()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var electronicScriptReference = myObject.GetComponent<Electronic>();

        // Forcing the status to true
        electronicScriptReference.@on = true;
        
        // Setting the bool to pass
        const bool inputToPass = false;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;
        
        // Check if the current status is equivalent to that passed
        Assert.IsTrue(inputToPass == electronicScriptReference.@on);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator TurnsPassingFalseWhenStatusIsFalse()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var electronicScriptReference = myObject.GetComponent<Electronic>();

        // Forcing the status to false
        electronicScriptReference.@on = false;
        
        // Setting the bool to pass
        const bool inputToPass = false;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;
        
        // Check if the current status is equivalent to that passed
        Assert.IsTrue(inputToPass == electronicScriptReference.@on);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}
