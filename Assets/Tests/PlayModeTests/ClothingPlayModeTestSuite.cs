using System;
using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class ClothingPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Cloth";
    }

    private const string wearsVerb = "wears";
    private const string unwearsVerb = "unwears";
    
    [UnityTest]
    public IEnumerator _WearsPassingNullCharacterWhenClothingIsWeared()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "_Wears()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to clothing script
        var clothingScriptReference = myObject.GetComponent<Clothing>();

        //Forcing flag to true
        clothingScriptReference.weared = true;
            
        // _Wears() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            clothingScriptReference._Wears(null);
            Action action = new Action(myObject, wearsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is still true
        Assert.IsTrue(clothingScriptReference.weared);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator _WearsPassingNullCharacterWhenClothingIsNotWeared()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "_Wears()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to clothing script
        var clothingScriptReference = myObject.GetComponent<Clothing>();

        //Forcing flag to true
        clothingScriptReference.weared = false;
            
        // _Wears() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // clothingScriptReference._Wears(null);
            Action action = new Action(myObject, wearsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is still true
        Assert.IsFalse(clothingScriptReference.weared);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator _UnwearsPassingNullCharacterWhenClothingIsWeared()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "_Unwears()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to clothing script reference
        var clothingScriptReference = myObject.GetComponent<Clothing>();

        clothingScriptReference.weared = true;

        // _Unwears() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // clothingScriptReference._Unwears(null);
            Action action = new Action(myObject, unwearsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);
        
        Assert.IsTrue(clothingScriptReference.weared);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator _UnwearsPassingNullCharacterIsNotWeared()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "_Unwears()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to clothing script reference
        var clothingScriptReference = myObject.GetComponent<Clothing>();

        clothingScriptReference.weared = false;

        // _Unwears() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // clothingScriptReference._Unwears(null);
            Action action = new Action(myObject, unwearsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);
        
        Assert.IsFalse(clothingScriptReference.weared);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

   
}
