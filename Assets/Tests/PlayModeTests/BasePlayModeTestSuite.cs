using System.Collections;
using System.Reflection;
using ECARules4All;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools.Utils;

public class BaseTestSuite
{
    // 
    protected string objInstanceName;
    protected string methodNameToTest = "";

    // PATHS/FOLDERS NAMES
    private const string resourcesTestsFolderPath = "Assets/Resources/Tests/";
    private const string txtRulesToReadFolderName = "RulesToRead/";
    protected const string pathForTextRuleParser = resourcesTestsFolderPath + txtRulesToReadFolderName;
    protected string scenePath = "Assets/Scenes/ScenesForTests/SceneForPlayModeTests.unity";

    // Paramer to use when loading the test scene
    private LoadSceneParameters parameters = new LoadSceneParameters(LoadSceneMode.Additive);

    // TIME CONSTANTS 
    protected const float ShortTimeDelay = 0.1f;
    protected const float MediumTimeDelay = 0.5f;
    protected const float LongTimeDelay = 1.5f;

    // ERROR ALLOWED IN COMPARING ASSERTS
    private const float ErrorAllowed = 10e-2f;

    // VERBS IN COMMON AMONG MULTIPLE ECAOBJECT
    protected const string insertsVerb = "inserts";
    protected const string turnsVerb = "turns";
    protected const string resetsVerb = "resets";
    protected const string startsVerb = "starts";
    protected const string stopsVerb = "stops";


    [SetUp]
    public virtual void Setup()
    {
        // Load the scene
        EditorSceneManager.LoadSceneAsyncInPlayMode(scenePath, parameters);
        Debug.Log($"Loading Scene: {SceneManager.GetActiveScene().name}");
    }

    [TearDown]
    public virtual void TearDown()
    {
        // Unload the scene
        if (!SceneManager.GetSceneByPath(scenePath).IsValid()) return;

        Debug.Log($"Unloading Scene {SceneManager.GetActiveScene().name}");
        SceneManager.UnloadSceneAsync(scenePath);
    }

    protected IEnumerator WaitForScene()
    {
        while (!SceneManager.GetSceneByPath(scenePath).isLoaded)
        {
            yield return new WaitForSeconds(ShortTimeDelay);
        }

        Debug.Log("Test scene has been loaded!");
    }

    protected static void PositionsAreEqual(Position p1, Position p2)
    {
        var currentMethodName = MethodBase.GetCurrentMethod().Name;
        if (p1 == null && p2 == null)
        {
            Debug.Log($"{currentMethodName}: Both positions are null, therefore the Assert is PASSED");
            Assert.Pass();
            return;
        }

        if (p1 != null && p2 == null || p1 == null)
        {
            Debug.Log($"{currentMethodName}: 1 position of 2 is null, therefore the Assert is FAILED");
            Assert.Fail();
            return;
        }

        var v1 = p1.GetPosition();
        var v2 = p2.GetPosition();
        Assert.AreEqual(v1, v2);
    }

    protected static void PositionsAreNotEqual(Position p1, Position p2)
    {
        var currentMethodName = MethodBase.GetCurrentMethod().Name;
        if (p1 == null && p2 == null)
        {
            Debug.Log($"{currentMethodName}: Both positions are null, therefore the Assert is PASSED");
            Assert.Pass();
            return;
        }

        if ((p1 != null && p2 == null) || (p1 == null))
        {
            Debug.Log($"{currentMethodName}: 1 position of 2 is null, therefore the Assert is FAILED");
            Assert.Fail();
            return;
        }

        var v1 = p1.GetPosition();
        var v2 = p2.GetPosition();
        Assert.AreNotEqual(v1, v2);
    }

    protected static void RotationsAreEqual(Rotation r1, Rotation r2)
    {
        var currentMethodName = MethodBase.GetCurrentMethod().Name;
        if (r1 == null && r2 == null)
        {
            Debug.Log($"{currentMethodName}: Both rotations are null, therefore the Assert is PASSED");
            Assert.Pass();
            return;
        }

        if ((r1 != null && r2 == null) || (r1 == null))
        {
            Debug.Log($"{currentMethodName}: 1 rotation of 2 is null, therefore the Assert is FAILED");
            Assert.Fail();
            return;
        }

        var q1 = Quaternion.Euler(r1.x, r1.y, r1.z);
        var q2 = Quaternion.Euler(r2.x, r2.y, r2.z);
        QuaternionsAreEqual(q1, q2);
    }

    protected static void RotationsAreNotEqual(Rotation r1, Rotation r2)
    {
        var currentMethodName = MethodBase.GetCurrentMethod().Name;
        if (r1 == null && r2 == null)
        {
            Debug.Log($"{currentMethodName}: Both rotations are null, therefore the Assert is PASSED");
            Assert.Pass();
            return;
        }

        if (r1 != null && r2 == null || r1 == null)
        {
            Debug.Log($"{currentMethodName}: 1 rotation of 2 is null, therefore the Assert is FAILED");
            Assert.Fail();
            return;
        }

        var q1 = Quaternion.Euler(r1.x, r1.y, r1.z);
        var q2 = Quaternion.Euler(r2.x, r2.y, r2.z);
        QuaternionsAreNotEqual(q1, q2);
    }

    /*protected static void Vector3sAreEqual(Vector3 v1, Vector3 v2)
    {
        Assert.That(v1, Is.EqualTo(v2).Using(new Vector3EqualityComparer(ErrorAllowed)));
    }
    
    protected static void Vector3sAreNotEqual(Vector3 v1, Vector3 v2)
    {
        Assert.That(v1, Is.Not.EqualTo(v2).Using(new Vector3EqualityComparer(ErrorAllowed)));
    }*/

    protected static void QuaternionsAreEqual(Quaternion q1, Quaternion q2)
    {
        var currentMethodName = MethodBase.GetCurrentMethod().Name;
        Assert.That(q1, Is.EqualTo(q2).Using(new QuaternionEqualityComparer(ErrorAllowed)));
    }

    protected static void QuaternionsAreNotEqual(Quaternion q1, Quaternion q2)
    {
        var currentMethodName = MethodBase.GetCurrentMethod().Name;
        Assert.That(q1, Is.Not.EqualTo(q2).Using(new QuaternionEqualityComparer(ErrorAllowed)));
    }

    protected static void ColorsAreEqual(Color c1, Color c2)
    {
        var currentMethodName = MethodBase.GetCurrentMethod().Name;
        Assert.That(c1, Is.EqualTo(c2).Using(new ColorEqualityComparer(ErrorAllowed)));
    }

    /*  [UnityTest]
      public IEnumerator RotationsEqualTest()
      {
          var v1 = new Rotation();
          v1.x = 0;
          v1.y = 0;
          v1.z = 0;
          var v2 = new Rotation();
          v2.x = 0;
          v2.y = 0.6f;
          v2.z = 0;
          
          
          RotationsAreNotEqual(v1,v2);
          v1.y = 1.0f;
          RotationsAreEqual(v1,v2);
          
          yield return null;
      }
  
      
      [UnityTest]
      public IEnumerator QuaternionsEqualTest()
      {
          QuaternionsAreEqual(Quaternion.identity, Quaternion.identity);
          QuaternionsAreEqual(Quaternion.Euler(1f, 1f, 1f), Quaternion.Euler(1f, 1f, 1f));
          QuaternionsAreNotEqual(Quaternion.identity, Quaternion.Euler(0,0,-1));
          QuaternionsAreNotEqual(Quaternion.Euler(1f, 1f, -1f), Quaternion.Euler(1f, 1f, -1.8f));        
          yield return null;
      }
      
      [UnityTest]
      public IEnumerator PositionsAreEqual()
      {
          var v1 = new Position(0, 0, 0);
          var v2 = new Position(0, 1, 0);
          
          PositionsAreNotEqual(v1,v2);
          v1.y = 1.0f;
          PositionsAreEqual(v1,v2);
          yield return null;
      }*/

    protected static void PrintRuleEngineStatus()
    {
        Debug.Log("");
        var rules = RuleEngine.GetInstance().Rules();
        foreach (var rule in rules)
        {
            Debug.Log($"Event: {rule.GetEvent()}");

            var condition = rule.GetCondition();
            if (condition != null)
            {
                while (condition.GetParent() != null)
                    condition = condition.GetParent();

                PrintConditions(condition, 0);
            }

            var actions = rule.GetActions();
            for (var index = 0; index < actions.Count; index++)
            {
                var action = actions[index];
                Debug.Log($"Action#{index + 1}: {action}");
            }

            Debug.Log("");
        }
    }

    private static void PrintConditions(Condition rootCondition, int nCall)
    {
        nCall++;
        
        if (rootCondition is SimpleCondition simpleCondition)
            Debug.Log(string.Format($"Simple condition: <color=yellow>{simpleCondition}</color>"));
        else
        {
            Debug.Log($"START Composite condition #{nCall}:");
            var children = ((CompositeCondition) rootCondition).Children();

            int index = 1;
            foreach (var simpleConditionChild in children)
            {
                Debug.Log($"Child condition #{index}:");
                PrintConditions(simpleConditionChild, nCall);
                index++;
            }
            Debug.Log($"END Composite condition #{nCall}:");
        }
    }
}