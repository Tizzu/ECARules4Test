using System;
using System.Collections;
using System.Collections.Generic;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class LightPlayModeTestSuite : BaseTestSuite
{
    private const float _minIntensity = 0f;
    
    private const string increasesIntensityVerb = "increases intensity by";
    private const string decreasesIntensityVerb = "decreases intensity by";
    private const string changesColorVerb = "changes color to";
    
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Light1";
    }

    [UnityTest]
    public IEnumerator LoadingTurnsRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"ECALightTurnsRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }
    
    [UnityTest]
    public IEnumerator LoadingIncreasesIntensityAndDecreasesIntensityRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"ECALightIncreasesIntensity&DecreasesIntensityRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();

        List<Rule> list = new List<Rule>();
        foreach (Rule r in RuleEngine.GetInstance().Rules())
        {
            list.Add(r);
        }
        Assert.IsTrue(list[0].GetEvent() == list[1].GetEvent());
        Assert.IsTrue(list[0].GetActions() == list[1].GetActions());
    }    
    
    [UnityTest]
    public IEnumerator LoadingSetColorRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"ECALightSetColor.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }
    
    [UnityTest]
    public IEnumerator TestingIfEcaColorIsEqualToUnityColor()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // Check if the current status is equivalent to that passed
        var unityColor = myObject.GetComponent<Light>().color;
        var ecaColor = lightScriptReference.color;

        Debug.Log("Comparing the colors...");
        ColorsAreEqual(ecaColor, unityColor);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TestingIfIntensityIsLessThanMaxIntensity()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();

        // Instantiate the object required for testing
        objInstanceName = "/LightWithIntensityGreaterThanMaximum";
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lightScriptReference = myObject.GetComponent<ECALight>();
        var intensity = lightScriptReference.intensity;
        var maxIntensity = lightScriptReference.maxIntensity;

        // Getting the intensity
        Assert.LessOrEqual(intensity, maxIntensity);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #region Turns

    [UnityTest]
    public IEnumerator TurnsPassingTrueWhenLightIsOn()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // Forcing the status to true
        lightScriptReference.@on = true;

        // Setting the bool to pass
        const bool inputToPass = true;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the current status is equivalent to that passed
        Assert.IsTrue(inputToPass == lightScriptReference.@on);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TurnsPassingTrueWhenLightIsOff()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // Forcing the status to true
        lightScriptReference.@on = false;

        // Setting the bool to pass
        const bool inputToPass = true;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the current status is equivalent to that passed
        Assert.IsTrue(inputToPass == lightScriptReference.@on);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TurnsPassingFalseWhenLightIsOn()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // Forcing the status to true
        lightScriptReference.@on = true;

        // Setting the bool to pass
        const bool inputToPass = false;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the current status is equivalent to that passed
        Assert.IsTrue(inputToPass == lightScriptReference.@on);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TurnsPassingFalseWhenLightIsOff()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // Forcing the status to true
        lightScriptReference.@on = false;

        // Setting the bool to pass
        const bool inputToPass = false;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the current status is equivalent to that passed
        Assert.IsTrue(inputToPass == lightScriptReference.@on);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #endregion
    
    #region IncreasesIntensity

    [UnityTest]
    public IEnumerator IncreasesIntensityPassingNullAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "IncreasesIntensity()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // saving the intensity
        var ecaIntensityBefore = lightScriptReference.intensity;
        var intensityBefore = myObject.GetComponent<Light>().intensity;

        // setting a null intensity
        const float intensityToPass = 0f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.IncreasesIntensity(intensityToPass);
        Action action = new Action(myObject, increasesIntensityVerb, intensityToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the intensity after the method call
        var ecaIntensityAfter = lightScriptReference.intensity;
        var intensityAfter = myObject.GetComponent<Light>().intensity;

        // Check if the intensity didn't change
        Assert.AreEqual(ecaIntensityBefore, ecaIntensityAfter);
        Assert.AreEqual(intensityBefore, intensityAfter);
        Assert.AreEqual(ecaIntensityAfter, intensityAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator IncreasesIntensityPassingNegativeAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "IncreasesIntensity()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // setting a positive intensity
        const float intensityToPass = -1f;

        lightScriptReference.intensity =
                myObject.GetComponent<Light>().intensity = 
                    (lightScriptReference.maxIntensity - _minIntensity)/2;
        
        // saving the intensity
        var ecaIntensityBefore = lightScriptReference.intensity;
        var intensityBefore = myObject.GetComponent<Light>().intensity;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.IncreasesIntensity(intensityToPass);
        Action action = new Action(myObject, increasesIntensityVerb, intensityToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the intensity after the method call
        var ecaIntensityAfter = lightScriptReference.intensity;
        var intensityAfter = myObject.GetComponent<Light>().intensity;

        // Check if the intensity didn't change
        Assert.Less(ecaIntensityAfter, ecaIntensityBefore);
        Assert.Less(intensityAfter, intensityBefore);
        Assert.AreEqual(ecaIntensityAfter, intensityAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator IncreasesIntensityPassingPositiveAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "IncreasesIntensity()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // setting a positive intensity
        const float intensityToPass = 1f;

        if (Math.Abs(lightScriptReference.intensity - lightScriptReference.maxIntensity) < 0.01f)
        {
            lightScriptReference.intensity =
                myObject.GetComponent<Light>().intensity = lightScriptReference.maxIntensity - intensityToPass;
        }

        // saving the intensity
        var ecaIntensityBefore = lightScriptReference.intensity;
        var intensityBefore = myObject.GetComponent<Light>().intensity;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.IncreasesIntensity(intensityToPass);
        Action action = new Action(myObject, increasesIntensityVerb, intensityToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the intensity after the method call
        var ecaIntensityAfter = lightScriptReference.intensity;
        var intensityAfter = myObject.GetComponent<Light>().intensity;

        // Check if the intensity didn't change
        Assert.Greater(ecaIntensityAfter, ecaIntensityBefore);
        Assert.Greater(intensityAfter, intensityBefore);
        Assert.AreEqual(ecaIntensityAfter, intensityAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator IncreasesIntensityPassingPositiveAmountExceedingLimit()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "IncreasesIntensity()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // setting a positive intensity
        var intensityToPass = lightScriptReference.maxIntensity;

        if (Math.Abs(lightScriptReference.intensity - lightScriptReference.maxIntensity) < 0.01f)
        {
            lightScriptReference.intensity =
                myObject.GetComponent<Light>().intensity = lightScriptReference.maxIntensity - intensityToPass;
        }

        // saving the intensity
        var ecaIntensityBefore = lightScriptReference.intensity;
        var intensityBefore = myObject.GetComponent<Light>().intensity;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.IncreasesIntensity(intensityToPass);
        Action action = new Action(myObject, increasesIntensityVerb, intensityToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the intensity after the method call
        var ecaIntensityAfter = lightScriptReference.intensity;
        var intensityAfter = myObject.GetComponent<Light>().intensity;

        // Check if the intensity didn't change
        Assert.Greater(ecaIntensityAfter, ecaIntensityBefore);
        Assert.Greater(intensityAfter, intensityBefore);
        Assert.AreEqual(ecaIntensityAfter, intensityAfter);
        Assert.AreEqual(ecaIntensityAfter, lightScriptReference.maxIntensity);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #endregion

    #region DecreasesIntensity

    [UnityTest]
    public IEnumerator DecreasesIntensityPassingNullAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "DecreasesIntensity()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // saving the intensity
        var ecaIntensityBefore = lightScriptReference.intensity;
        var intensityBefore = myObject.GetComponent<Light>().intensity;

        // setting a null intensity
        const float intensityToPass = 0f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.DecreasesIntensity(intensityToPass);
        Action action = new Action(myObject, decreasesIntensityVerb, intensityToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the intensity after the method call
        var ecaIntensityAfter = lightScriptReference.intensity;
        var intensityAfter = myObject.GetComponent<Light>().intensity;

        // Check if the intensity didn't change
        Assert.AreEqual(ecaIntensityBefore, ecaIntensityAfter);
        Assert.AreEqual(intensityBefore, intensityAfter);
        Assert.AreEqual(ecaIntensityAfter, intensityAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator DecreasesIntensityPassingNegativeAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "DecreasesIntensity()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // setting a positive intensity
        const float intensityToPass = -1f;

        lightScriptReference.intensity =
                myObject.GetComponent<Light>().intensity = 
                    (lightScriptReference.maxIntensity - _minIntensity)/2;

        // saving the intensity
        var ecaIntensityBefore = lightScriptReference.intensity;
        var intensityBefore = myObject.GetComponent<Light>().intensity;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.DecreasesIntensity(intensityToPass);
        Action action = new Action(myObject, decreasesIntensityVerb, intensityToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the intensity after the method call
        var ecaIntensityAfter = lightScriptReference.intensity;
        var intensityAfter = myObject.GetComponent<Light>().intensity;

        // Check if the intensity didn't change
        Assert.Greater(ecaIntensityAfter, ecaIntensityBefore);
        Assert.Greater(intensityAfter, intensityBefore);
        Assert.AreEqual(ecaIntensityAfter, intensityAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator DecreasesIntensityPassingPositiveAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "DecreasesIntensity()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // setting a positive intensity
        const float intensityToPass = 1f;

        if (Math.Abs(lightScriptReference.intensity - _minIntensity) < 0.01f)
        {
            lightScriptReference.intensity =
                myObject.GetComponent<Light>().intensity = _minIntensity + intensityToPass;
        }

        // saving the intensity
        var ecaIntensityBefore = lightScriptReference.intensity;
        var intensityBefore = myObject.GetComponent<Light>().intensity;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lightScriptReference.DecreasesIntensity(intensityToPass);
        Action action = new Action(myObject, decreasesIntensityVerb, intensityToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the intensity after the method call
        var ecaIntensityAfter = lightScriptReference.intensity;
        var intensityAfter = myObject.GetComponent<Light>().intensity;

        // Check if the intensity didn't change
        Assert.Less(ecaIntensityAfter, ecaIntensityBefore);
        Assert.Less(intensityAfter, intensityBefore);
        Assert.AreEqual(ecaIntensityAfter, intensityAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TestingIfEcaIntensityIsEqualToUnityIntensity()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();

        // Instantiate the object required for testing
        objInstanceName = "/LightWithIntensityGreaterThanMaximum";
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lightScriptReference = myObject.GetComponent<ECALight>();

        // Getting the intensity
        var unityIntensity = myObject.GetComponent<Light>().intensity;
        var intensity = lightScriptReference.intensity;

        Debug.Log("Comparing the intensities...");
        Assert.AreEqual(intensity, unityIntensity);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #endregion
}