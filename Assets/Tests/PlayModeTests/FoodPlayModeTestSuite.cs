using System;
using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class FoodPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Food1";
    }


    [UnityTest]
    public IEnumerator Loading_EatsRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"Food_EatsRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }
    
    private const string eatsVerb = "eats";
        
    [UnityTest]
    public IEnumerator _EatsPassingNullCharacter()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "_Eats()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var foodScriptReference = myObject.GetComponent<Food>();
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Forcing the existence of the food
        ecaObjectScriptReference.isActive = true;
        foodScriptReference.eaten = false;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // foodScriptReference._Eats(null);
            Action action = new Action(null, eatsVerb, myObject);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the food still exists
        Assert.IsTrue(ecaObjectScriptReference.isActive);
        Assert.IsFalse(foodScriptReference.eaten);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator _EatsAfterExpirationDate()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "_Eats()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Instantiate the Character
        var character = GameObject.Find("/Character1");
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var foodScriptReference = myObject.GetComponent<Food>();
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Forcing the food as not eaten
        ecaObjectScriptReference.isActive = true;
        foodScriptReference.eaten = false;
        // and as expired (yesterday)
        foodScriptReference.expiration = DateTime.Today.AddDays(-1);

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // foodScriptReference._Eats(character.GetComponent<Character>());
        Action action = new Action(character, eatsVerb, myObject);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the food still exists
        Assert.IsTrue(ecaObjectScriptReference.isActive);
        Assert.IsFalse(foodScriptReference.eaten);


        // Destroy the Character
        Debug.Log($"Destroying GameObject {character.name}");
        Object.Destroy(character);
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator _EatsPassingExistingCharacter()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "_Eats()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Instantiate the Character
        var character = GameObject.Find("/Character1");
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Food and ECAObject scripts
        var foodScriptReference = myObject.GetComponent<Food>();
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Forcing the food as not eaten
        ecaObjectScriptReference.isActive = true;
        foodScriptReference.eaten = false;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // foodScriptReference._Eats(character.GetComponent<Character>());
        Action action = new Action(character, eatsVerb, myObject);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the food still exists
        Assert.IsFalse(ecaObjectScriptReference.isActive);
        Assert.IsTrue(foodScriptReference.eaten);

        // Destroy the Character
        Debug.Log($"Destroying GameObject {character.name}");
        Object.Destroy(character);
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}