using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.TestTools.Utils;
using UnityEngine.Video;

public class VideoPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Video1";
    }

    private const string playsVerb = "plays";
    private const string pausesVerb = "pauses";
    private const string changesVolumeVerb = "changes volume to";

    
    [UnityTest]
    public IEnumerator PlaysCheckIfVideoIsReproduced()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Plays()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaVideoScriptReference = myObject.GetComponent<ECAVideo>();
        var videoPlayerScriptReference = myObject.GetComponent<VideoPlayer>();
        
        // Saving status before
        ecaVideoScriptReference.paused = false;
        ecaVideoScriptReference.playing = false;
        ecaVideoScriptReference.stopped = false;
        //var currentTimeBefore = ecaVideoScriptReference.currentTime = 0f;
        
        // method call
        Debug.Log($"Before calling {methodNameToTest}");
        //ecaVideoScriptReference.Plays();
        Action action = new Action(myObject, playsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        while(!videoPlayerScriptReference.isPrepared)
            yield return new WaitForSeconds(MediumTimeDelay);

        // Save the status after
        var pausedAfter = ecaVideoScriptReference.paused;
        var playingAfter = ecaVideoScriptReference.playing;
        var stoppedAfter = ecaVideoScriptReference.stopped;
        //var currentTimeAfter = ecaVideoScriptReference.currentTime;

        // Check 
        Assert.IsFalse(pausedAfter);
        Assert.IsTrue(playingAfter);
        Assert.IsFalse(stoppedAfter);
        Assert.IsTrue(videoPlayerScriptReference.isPlaying);
        Assert.IsFalse(videoPlayerScriptReference.isPaused);
        // Assert.IsFalse(videoPlayerScriptReference.time);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator PlaysCheckIfVideoStartsAccuratelyAfterPause()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Plays()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaVideoScriptReference = myObject.GetComponent<ECAVideo>();
        var videoPlayerScriptReference = myObject.GetComponent<VideoPlayer>();
        
        // Saving status before
        ecaVideoScriptReference.paused = false;
        ecaVideoScriptReference.playing = false;
        ecaVideoScriptReference.stopped = false;
        //var currentTimeBefore = ecaVideoScriptReference.currentTime = 0f;
        
        //ecaVideoScriptReference.Plays();
        Action action = new Action(myObject, playsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        while(!videoPlayerScriptReference.isPrepared)
            yield return new WaitForSeconds(MediumTimeDelay);
        
        // ecaVideoScriptReference.Pauses();
        action = new Action(myObject, pausesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        // Saving status before
        var frameAfterPause = videoPlayerScriptReference.frame;
        var pausedAfterPause = ecaVideoScriptReference.paused;
        var playingAfterPause = ecaVideoScriptReference.playing;
        var stoppedAfterPause = ecaVideoScriptReference.stopped; 
        yield return new WaitForSeconds(MediumTimeDelay);
        
        // Check 
        Assert.IsTrue(pausedAfterPause);
        Assert.IsFalse(playingAfterPause);
        Assert.IsFalse(stoppedAfterPause);
        Assert.IsFalse(videoPlayerScriptReference.isPlaying);
        Assert.IsTrue(videoPlayerScriptReference.isPaused);

        // method call
        Debug.Log($"Before calling {methodNameToTest}");
        //ecaVideoScriptReference.Plays();
        action = new Action(myObject, playsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");
        
        do
        {
            yield return new WaitForSeconds(MediumTimeDelay);
        } while (!videoPlayerScriptReference.isPrepared || !videoPlayerScriptReference.isPlaying ||
                 !videoPlayerScriptReference.canStep);
        
        // save the status
        var frameAfterStartingAgain = videoPlayerScriptReference.frame;
        var pausedAfterStartingAgain = ecaVideoScriptReference.paused;
        var playingAfterStartingAgain = ecaVideoScriptReference.playing;
        var stoppedAfterStartingAgain = ecaVideoScriptReference.stopped;
        //var currentTimeAfter = ecaVideoScriptReference.currentTime;
        
        // Check 
        Assert.IsFalse(pausedAfterStartingAgain);
        Assert.IsTrue(playingAfterStartingAgain);
        Assert.IsFalse(stoppedAfterStartingAgain);
        Assert.IsTrue(videoPlayerScriptReference.isPlaying);
        Assert.IsFalse(videoPlayerScriptReference.isPaused);
        Assert.That(frameAfterPause, Is.LessThan(frameAfterStartingAgain));
        //var fps = videoPlayerScriptReference.frameRate;
        //Assert.That(frameAfterStartingAgain-frameAfterPause, Is.EqualTo(pauses*MediumTimeDelay*fps).Using(new FloatComparer(0.1f)));

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator PlaysCheckIfVideoStartsAccuratelyAfterStop()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Plays()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaVideoScriptReference = myObject.GetComponent<ECAVideo>();
        var videoPlayerScriptReference = myObject.GetComponent<VideoPlayer>();
        
        // Saving status before
        ecaVideoScriptReference.paused = false;
        ecaVideoScriptReference.playing = false;
        ecaVideoScriptReference.stopped = false;
        //var currentTimeBefore = ecaVideoScriptReference.currentTime = 0f;
        
        // ecaVideoScriptReference.Plays();
        Action action = new Action(myObject, playsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        do
        {
            yield return new WaitForSeconds(LongTimeDelay);
        } while (!videoPlayerScriptReference.isPrepared || !videoPlayerScriptReference.isPlaying ||
                 !videoPlayerScriptReference.canStep);

        var frameImmediatelyBeforeStop = videoPlayerScriptReference.frame;
        // ecaVideoScriptReference.Stops();
        action = new Action(myObject, stopsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        // Saving status before
        var pausedAfterStop = ecaVideoScriptReference.paused;
        var playingAfterStop = ecaVideoScriptReference.playing;
        var stoppedAfterStop = ecaVideoScriptReference.stopped; 
        yield return new WaitForSeconds(MediumTimeDelay);
        
        // Check 
        Assert.IsFalse(pausedAfterStop);
        Assert.IsFalse(playingAfterStop);
        Assert.IsTrue(stoppedAfterStop);
        Assert.IsFalse(videoPlayerScriptReference.isPlaying);
        Assert.IsFalse(videoPlayerScriptReference.isPaused);
        
        // method call
        Debug.Log($"Before calling {methodNameToTest}");
        //ecaVideoScriptReference.Plays();
        action = new Action(myObject, playsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");
        
        do
        {
            yield return new WaitForSeconds(MediumTimeDelay);
        } while (!videoPlayerScriptReference.isPrepared || !videoPlayerScriptReference.isPlaying ||
                 !videoPlayerScriptReference.canStep);

        // save the status
        var frameAfterStartingAgain = videoPlayerScriptReference.frame;
        var pausedAfterStartingAgain = ecaVideoScriptReference.paused;
        var playingAfterStartingAgain = ecaVideoScriptReference.playing;
        var stoppedAfterStartingAgain = ecaVideoScriptReference.stopped;
        //var currentTimeAfter = ecaVideoScriptReference.currentTime;
        
        // Check 
        Assert.IsFalse(pausedAfterStartingAgain);
        Assert.IsTrue(playingAfterStartingAgain);
        Assert.IsFalse(stoppedAfterStartingAgain);
        Assert.IsTrue(videoPlayerScriptReference.isPlaying);
        Assert.IsFalse(videoPlayerScriptReference.isPaused);
        Assert.That(frameImmediatelyBeforeStop, Is.GreaterThan(frameAfterStartingAgain));
        //var fps = videoPlayerScriptReference.frameRate;
        //Assert.That(frameAfterStartingAgain-frameAfterStop, Is.EqualTo(MediumTimeDelay*fps).Using(new FloatComparer(0.1f)));

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator PausesCheckIfVideoIsPaused()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Pauses()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaVideoScriptReference = myObject.GetComponent<ECAVideo>();
        var videoPlayerScriptReference = myObject.GetComponent<VideoPlayer>();
        
        // Saving status before
        ecaVideoScriptReference.paused = false;
        ecaVideoScriptReference.playing = false;
        ecaVideoScriptReference.stopped = false;
        //var currentTimeBefore = ecaVideoScriptReference.currentTime = 0f;
        
        // ecaVideoScriptReference.Plays();
        Action action = new Action(myObject, playsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        do
        {
            yield return new WaitForSeconds(MediumTimeDelay);
        } while (!videoPlayerScriptReference.isPrepared || !videoPlayerScriptReference.isPlaying ||
                 !videoPlayerScriptReference.canStep);
        
        // method call
        Debug.Log($"Before calling {methodNameToTest}");
        // ecaVideoScriptReference.Pauses();
        action = new Action(myObject, pausesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        do
        {
            yield return new WaitForSeconds(ShortTimeDelay);
        } while (videoPlayerScriptReference.isPlaying);
        
        var frameBeforePause = videoPlayerScriptReference.frame;
        yield return new WaitForSeconds(MediumTimeDelay);
        var frameAfterPause = videoPlayerScriptReference.frame;
        
        // Save the status after
        var pausedAfter = ecaVideoScriptReference.paused;
        var playingAfter = ecaVideoScriptReference.playing;
        var stoppedAfter = ecaVideoScriptReference.stopped;
        //var currentTimeAfter = ecaVideoScriptReference.currentTime;

        // Check 
        Assert.That(frameBeforePause, Is.EqualTo(frameAfterPause).Using(FloatEqualityComparer.Instance));
        Assert.IsTrue(pausedAfter);
        Assert.IsFalse(playingAfter);
        Assert.IsFalse(stoppedAfter);
        Assert.IsFalse(videoPlayerScriptReference.isPlaying);
        Assert.IsTrue(videoPlayerScriptReference.isPaused);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator StopsCheckIfVideoIsStopped()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Stops()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaVideoScriptReference = myObject.GetComponent<ECAVideo>();
        var videoPlayerScriptReference = myObject.GetComponent<VideoPlayer>();
        
        // Saving status before
        ecaVideoScriptReference.paused = false;
        ecaVideoScriptReference.playing = false;
        ecaVideoScriptReference.stopped = false;
        //var currentTimeBefore = ecaVideoScriptReference.currentTime = 0f;
        
        // ecaVideoScriptReference.Plays();
        Action action = new Action(myObject, playsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        yield return new WaitForSeconds(MediumTimeDelay);
        
        // method call
        Debug.Log($"Before calling {methodNameToTest}");
        // ecaVideoScriptReference.Stops();
        action = new Action(myObject, stopsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        var frameBeforePause = videoPlayerScriptReference.frame;
        yield return new WaitForSeconds(MediumTimeDelay);
        var frameAfterPause = videoPlayerScriptReference.frame;
        
        // Save the status after
        var pausedAfter = ecaVideoScriptReference.paused;
        var playingAfter = ecaVideoScriptReference.playing;
        var stoppedAfter = ecaVideoScriptReference.stopped;
        //var currentTimeAfter = ecaVideoScriptReference.currentTime;

        // Check 
        Assert.That(frameBeforePause, Is.EqualTo(frameAfterPause).Using(FloatEqualityComparer.Instance));
        Assert.IsFalse(pausedAfter);
        Assert.IsFalse(playingAfter);
        Assert.IsTrue(stoppedAfter);
        Assert.IsFalse(videoPlayerScriptReference.isPlaying);
        Assert.IsFalse(videoPlayerScriptReference.isPaused);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator ChangesVolumePassingAmountInRange()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesVolume()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaVideoScriptReference = myObject.GetComponent<ECAVideo>();
        var videoPlayerScriptReference = myObject.GetComponent<VideoPlayer>();

        const float volumeToPass = 10;
        ecaVideoScriptReference.maxVolume = 8*volumeToPass;
        // saving the status before
        var ecaVideoVolumeBefore = ecaVideoScriptReference.volume = ecaVideoScriptReference.maxVolume/2;
        var videoPlayerVolumeBefore = ecaVideoVolumeBefore / ecaVideoScriptReference.maxVolume;
        videoPlayerScriptReference.SetDirectAudioVolume(0,videoPlayerVolumeBefore);
        
        // method call
        Debug.Log($"Before calling {methodNameToTest}");
        // ecaVideoScriptReference.ChangesVolume(volumeToPass);
        Action action = new Action(myObject, changesVolumeVerb, volumeToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(MediumTimeDelay);

        // Save the status after
        var ecaVideoVolumeAfter = ecaVideoScriptReference.volume;
        var videoPlayerVolumeAfter = ecaVideoVolumeAfter / ecaVideoScriptReference.maxVolume;
        
        // Check 
        Assert.That(ecaVideoVolumeBefore, Is.Not.EqualTo(ecaVideoVolumeAfter));
        Assert.That(ecaVideoVolumeAfter, Is.EqualTo(volumeToPass));
        Assert.That(videoPlayerVolumeBefore, Is.Not.EqualTo(videoPlayerVolumeAfter));
        Assert.That(videoPlayerVolumeAfter, Is.EqualTo(volumeToPass/ecaVideoScriptReference.maxVolume));
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator ChangesVolumePassingAmountLowerThanMinVolume()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesVolume()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaVideoScriptReference = myObject.GetComponent<ECAVideo>();
        var videoPlayerScriptReference = myObject.GetComponent<VideoPlayer>();

        const float volumeToPass = -5;
        ecaVideoScriptReference.maxVolume = 80;
     
        // saving the status before
        var ecaVideoVolumeBefore = ecaVideoScriptReference.volume = ecaVideoScriptReference.maxVolume/2;
        var videoPlayerVolumeBefore = ecaVideoVolumeBefore / ecaVideoScriptReference.maxVolume;
        videoPlayerScriptReference.SetDirectAudioVolume(0,videoPlayerVolumeBefore);
        
        // method call
        Debug.Log($"Before calling {methodNameToTest}");
        // ecaVideoScriptReference.ChangesVolume(volumeToPass);
        Action action = new Action(myObject, changesVolumeVerb, volumeToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(MediumTimeDelay);

        // Save the status after
        var ecaVideoVolumeAfter = ecaVideoScriptReference.volume;
        var videoPlayerVolumeAfter = ecaVideoVolumeAfter / ecaVideoScriptReference.maxVolume;
        
        // Check 
        Assert.That(ecaVideoVolumeBefore, Is.Not.EqualTo(ecaVideoVolumeAfter));
        Assert.That(ecaVideoVolumeAfter, Is.EqualTo(0f));
        Assert.That(videoPlayerVolumeBefore, Is.Not.EqualTo(videoPlayerVolumeAfter));
        Assert.That(videoPlayerVolumeAfter, Is.EqualTo(0f));
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator ChangesVolumePassingAmountHigherThanMaxVolume()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesVolume()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaVideoScriptReference = myObject.GetComponent<ECAVideo>();
        var videoPlayerScriptReference = myObject.GetComponent<VideoPlayer>();

        const float volumeToPass = 100;
        ecaVideoScriptReference.maxVolume = volumeToPass*0.8f;
     
        // saving the status before
        var ecaVideoVolumeBefore = ecaVideoScriptReference.volume = ecaVideoScriptReference.maxVolume/2;
        var videoPlayerVolumeBefore = ecaVideoVolumeBefore / ecaVideoScriptReference.maxVolume;
        videoPlayerScriptReference.SetDirectAudioVolume(0,videoPlayerVolumeBefore);
        
        // method call
        Debug.Log($"Before calling {methodNameToTest}");
        // ecaVideoScriptReference.ChangesVolume(volumeToPass);
        Action action = new Action(myObject, changesVolumeVerb, volumeToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(MediumTimeDelay);

        // Save the status after
        var ecaVideoVolumeAfter = ecaVideoScriptReference.volume;
        var videoPlayerVolumeAfter = ecaVideoVolumeAfter / ecaVideoScriptReference.maxVolume;
        
        // Check 
        Assert.That(ecaVideoVolumeBefore, Is.Not.EqualTo(ecaVideoVolumeAfter));
        Assert.That(ecaVideoVolumeAfter, Is.EqualTo(ecaVideoScriptReference.maxVolume));
        Assert.That(videoPlayerVolumeBefore, Is.Not.EqualTo(videoPlayerVolumeAfter));
        Assert.That(videoPlayerVolumeAfter, Is.EqualTo(1));
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}
