using System.Collections;
using ECARules4All;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class VehiclePlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Vehicle1";
    }

    [UnityTest]
    public IEnumerator LoadingStartsAndAcceleratesRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser + "VehicleStartsAndAcceleratesRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }

    [UnityTest]
    public IEnumerator LoadingStopsAndSlowsDownRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser + "VehicleStopsAndSlowsDownRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }

    private const string acceleratesVerb = "accelerates by";
    private const string slowsVerb = "slows by";

    [UnityTest]
    public IEnumerator StartsTestingIfVehicleMovesOverTimeWhenSpeedIsPositive()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Starts()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();

        // setting a positive speed
        const float speedToPass = 10f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}"); 
        // vehicleScriptReference.Starts();
        Action action = new Action(myObject, startsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);

/* Non cambia neanche aspettando del tempo/usando la action accelerates invece del mero assegnamento di speed
//        yield return new WaitForSeconds(ShortTimeDelay);
//        action = new Action(myObject, acceleratesVerb, speedToPass);
//        RuleEngine.GetInstance().ExecuteAction(action);
*/
        vehicleScriptReference.speed = speedToPass;
        Debug.Log($"After calling {methodNameToTest}");

        // saving the position before the wait 
        var positionBefore = new Position();
        positionBefore.Assign(ecaObjectScriptReference.p);

        // Wait 
        yield return new WaitForSeconds(MediumTimeDelay);

        // Saving the position after the wait
        var positionAfter = new Position();
        positionAfter.Assign(ecaObjectScriptReference.p);

        // Check if the position changed
        PositionsAreNotEqual(positionBefore, positionAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator StartsTestingIfVehicleMovesOverTimeWhenSpeedIsNull()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Starts()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();

        // setting a positive speed
        const float speedToPass = 0f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.Starts();
        Action action = new Action(myObject, startsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        vehicleScriptReference.speed = speedToPass;
        Debug.Log($"After calling {methodNameToTest}");

        // saving the position before the wait
        var positionBefore = new Position();
        positionBefore.Assign(ecaObjectScriptReference.p);

        // Wait 
        yield return new WaitForSeconds(MediumTimeDelay);

        // Saving the position after the wait
        var positionAfter = new Position();
        positionAfter.Assign(ecaObjectScriptReference.p);

        // Check if the position changed
        PositionsAreEqual(positionBefore, positionAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator StopsTestingIfVehicleDoesNotMoveOverTime()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Stops()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.Stops();
        Action action = new Action(myObject, stopsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // saving the position before the wait 
        var positionBefore = new Position();
        positionBefore.Assign(ecaObjectScriptReference.p);

        // Wait 
        yield return new WaitForSeconds(MediumTimeDelay);

        // Saving the position after the wait
        var positionAfter = new Position();
        positionAfter.Assign(ecaObjectScriptReference.p);

        // Check if the position changed
        PositionsAreEqual(positionBefore, positionAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #region Accelerates

    [UnityTest]
    public IEnumerator AcceleratesPassingNegativeAcceleration()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Accelerates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        // TODO check also the position?
        //var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();
        vehicleScriptReference.@on = true;

        // saving the speed
        var speedBefore = vehicleScriptReference.speed;
        // setting a negative speed
        const float speedToPass = -1f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.Accelerates(speedToPass);
        Action action = new Action(myObject, acceleratesVerb, speedToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the speed after the method call
        var speedAfter = vehicleScriptReference.speed;

        // Check if the speed is decreased after the method call
        Assert.Less(speedAfter, speedBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator AcceleratesPassingNullAcceleration()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Accelerates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        // TODO check also the position?
        //var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();
        vehicleScriptReference.@on = true;

        // saving the speed
        var speedBefore = vehicleScriptReference.speed;
        // setting a null speed
        const float speedToPass = 0f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.Accelerates(speedToPass);
        Action action = new Action(myObject, acceleratesVerb, speedToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the speed after the method call
        var speedAfter = vehicleScriptReference.speed;

        // Check if the acceleration didn't change
        Assert.AreEqual(speedBefore, speedAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator AcceleratesPassingPositiveAcceleration()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Accelerates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        // TODO check also the position?
        //var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();
        vehicleScriptReference.@on = true;

        // saving the speed
        var speedBefore = vehicleScriptReference.speed;
        // setting a negative speed
        const float speedToPass = 1f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        //vehicleScriptReference.Accelerates(speedToPass);
        Action action = new Action(myObject, acceleratesVerb, speedToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the speed after the method call
        var speedAfter = vehicleScriptReference.speed;

        // Check if the acceleration didn't change
        Assert.Greater(speedAfter, speedBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator AcceleratesWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Accelerates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        // TODO check also the position?
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();

        // turning the car off
        vehicleScriptReference.@on = true;
        ecaObjectScriptReference.isActive = false;

        // saving the speed
        var speedBefore = vehicleScriptReference.speed;
        // setting a negative speed
        const float speedToPass = 1f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.Accelerates(speedToPass);
        Action action = new Action(myObject, acceleratesVerb, speedToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the speed after the method call
        var speedAfter = vehicleScriptReference.speed;

        // Check if the acceleration didn't change
        Assert.AreEqual(speedBefore, speedAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    /* [UnityTest]
     public IEnumerator AccelerateWhenVehicleIsOff()
     {
         Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
         methodNameToTest = "Accelerates()";
         yield return WaitForScene();
 
         // Instantiate the object required for testing
         var myObject = GameObject.Find(objInstanceName);
         Debug.Log($"Loading GameObject {myObject.name}");
 
         // Getting the reference to ECAObject and Vehicle scripts
         //var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
         var vehicleScriptReference = myObject.GetComponent<Vehicle>();
         
         // turning the car off
         vehicleScriptReference.@on = false;
 
         // saving the speed
         var speedBefore = vehicleScriptReference.speed;
         // setting a negative speed
         const float speedToPass = 1f;
 
         // Method call
         Debug.Log($"Before calling {methodNameToTest}");
         //vehicleScriptReference.Accelerates(speedToPass);
         Action action = new Action(myObject, acceleratesVerb, speedToPass);
         RuleEngine.GetInstance().ExecuteAction(action);
         Debug.Log($"After calling {methodNameToTest}");
 
         // Wait 0
         yield return null;
 
         // Saving the speed after the method call
         var speedAfter = vehicleScriptReference.speed;
 
         // Check if the acceleration didn't change
         Assert.AreEqual(speedBefore, speedAfter);
 
         // Destroy the object 
         Debug.Log($"Destroying GameObject {myObject.name}");
         Object.Destroy(myObject);
     }*/

    #endregion

    #region SlowsDown

    [UnityTest]
    public IEnumerator SlowsDownPassingNegativeDeceleration()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "SlowsDown()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        // TODO check also the position?
        //var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();
        vehicleScriptReference.@on = true;

        // saving the speed
        var speedBefore = vehicleScriptReference.speed;
        // setting a negative speed
        const float speedToPass = -1f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.SlowsDown(speedToPass);
        Action action = new Action(myObject, slowsVerb, speedToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the speed after the method call
        var speedAfter = vehicleScriptReference.speed;

        // Check if the deceleration didn't change
        Assert.Greater(speedAfter, speedBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator SlowsDownPassingNullDeceleration()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "SlowsDown()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        // TODO check also the position?
        //var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();
        vehicleScriptReference.@on = true;

        // saving the speed
        var speedBefore = vehicleScriptReference.speed;
        // setting a negative speed
        const float speedToPass = 0f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.SlowsDown(speedToPass);
        Action action = new Action(myObject, slowsVerb, speedToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the speed after the method call
        var speedAfter = vehicleScriptReference.speed;

        // Check if the deceleration didn't change
        Assert.AreEqual(speedBefore, speedAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator SlowsDownPassingPositiveDeceleration()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "SlowsDown()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        // TODO check also the position?
        //var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();
        vehicleScriptReference.@on = true;

        // saving the speed
        var speedBefore = vehicleScriptReference.speed;
        // setting a negative speed
        const float speedToPass = 1f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.SlowsDown(speedToPass);
        Action action = new Action(myObject, slowsVerb, speedToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the speed after the method call
        var speedAfter = vehicleScriptReference.speed;

        // Check if the deceleration didn't change
        Assert.Less(speedAfter, speedBefore);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator SlowsDownWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "SlowsDown()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObject and Vehicle scripts
        // TODO check also the position?
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var vehicleScriptReference = myObject.GetComponent<Vehicle>();

        // turning the car off
        vehicleScriptReference.@on = true;
        ecaObjectScriptReference.isActive = false;

        // saving the speed
        var speedBefore = vehicleScriptReference.speed;
        // setting a negative speed
        const float speedToPass = 1f;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // vehicleScriptReference.SlowsDown(speedToPass);
        Action action = new Action(myObject, slowsVerb, speedToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Saving the speed after the method call
        var speedAfter = vehicleScriptReference.speed;

        // Check if the deceleration didn't change
        Assert.AreEqual(speedBefore, speedAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    /* [UnityTest]
     public IEnumerator SlowsDownWhenVehicleIsOff()
     {
         Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
         methodNameToTest = "SlowsDown()";
         yield return WaitForScene();
 
         // Instantiate the object required for testing
         var myObject = GameObject.Find(objInstanceName);
         Debug.Log($"Loading GameObject {myObject.name}");
 
         // Getting the reference to ECAObject and Vehicle scripts
         //var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
         var vehicleScriptReference = myObject.GetComponent<Vehicle>();
         
         // turning the car off
         vehicleScriptReference.@on = false;
 
         // saving the speed
         var speedBefore = vehicleScriptReference.speed;
         // setting a negative speed
         const float speedToPass = 1f;
 
         // Method call
         Debug.Log($"Before calling {methodNameToTest}");
         //vehicleScriptReference.SlowsDown(speedToPass);
         Action action = new Action(myObject, slowsVerb, speedToPass);
         RuleEngine.GetInstance().ExecuteAction(action);
         Debug.Log($"After calling {methodNameToTest}");
 
         // Wait 0
         yield return null;
 
         // Saving the speed after the method call
         var speedAfter = vehicleScriptReference.speed;
 
         // Check if the deceleration didn't change
         Assert.AreEqual(speedBefore, speedAfter);
 
         // Destroy the object 
         Debug.Log($"Destroying GameObject {myObject.name}");
         Object.Destroy(myObject);
     }*/

    #endregion
}