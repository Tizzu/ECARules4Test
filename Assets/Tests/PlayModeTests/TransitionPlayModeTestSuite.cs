using System;
using UnityEngine.SceneManagement;
using static System.String;

public class TransitionPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Transition1";
    }

    /*
     * @author: yagero
     * @source: https://gist.github.com/yagero/2cd50a12fcc928a6446539119741a343
     */
    private static bool DoesSceneExist(string name)
    {
        if (IsNullOrEmpty(name))
            return false;

        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            var scenePath = SceneUtility.GetScenePathByBuildIndex(i);
            var lastSlash = scenePath.LastIndexOf("/", StringComparison.Ordinal);
            var sceneName = scenePath.Substring(lastSlash + 1,
                scenePath.LastIndexOf(".", StringComparison.Ordinal) - lastSlash - 1);

            if (Compare(name, sceneName, StringComparison.OrdinalIgnoreCase) == 0)
                return true;
        }

        return false;
    }

    // [UnityTest]
    // public IEnumerator TeleportsPassingNullScene()
    // {
    //     Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
    //     methodNameToTest = "Teleports()";
    //     yield return WaitForScene();
    //
    //     // Instantiate the object required for testing
    //     var myObject = GameObject.Find(objInstanceName);
    //     Debug.Log($"Loading GameObject {myObject.name}");
    //
    //     // Getting the reference to Transition script
    //     var transitionScriptReference = myObject.GetComponent<Transition>();
    //
    //     // Teleports() call
    //     Debug.Log($"Before calling {methodNameToTest}");
    //     try
    //     {
    //         Debug.Log($"I'm passing the null scene");
    //         transitionScriptReference.Teleports(null);
    //     }
    //     catch (NullReferenceException e)
    //     {
    //         Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
    //                   $"\nError message: {e.Message}");
    //         Assert.Fail();
    //     }
    //
    //     Debug.Log($"After calling {methodNameToTest}");
    //
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);
    //
    //     // Check if the current scene didn't change
    //     Assert.IsTrue(SceneManager.GetSceneByPath(scenePath).IsValid());
    //
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);
    //
    //     /*
    //     SceneManager.LoadSceneAsync(scenePath, LoadSceneMode.Single);
    //     yield return WaitForScene();
    //     //SceneManager.SetActiveScene(SceneManager.GetSceneByPath(scenePath));
    //     if (SceneManager.GetSceneByName(nameBefore).IsValid())
    //         SceneManager.UnloadSceneAsync(nameBefore);
    //     
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);*/
    // }
    //
    // [UnityTest]
    // public IEnumerator TeleportsPassingNonExistingScene()
    // {
    //     Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
    //     methodNameToTest = "Teleports()";
    //     yield return WaitForScene();
    //
    //     // Instantiate the object required for testing
    //     var myObject = GameObject.Find(objInstanceName);
    //     Debug.Log($"Loading GameObject {myObject.name}");
    //
    //     // Getting the reference to Transition script
    //     var transitionScriptReference = myObject.GetComponent<Transition>();
    //
    //     // Defining the scene
    //     var sceneToPass = GameObject.Find("/Scene1").GetComponent<Scene>();
    //     const string baseSceneName = "SceneThatDoesNotExist";
    //     sceneToPass.name = Copy(baseSceneName);
    //
    //     // Changing the sceneName as long as the scene with that name exists
    //     /*while (DoesSceneExist(sceneToPass.name))
    //         sceneToPass.name += "a";*/
    //     var nameBefore = Copy(sceneToPass.name);
    //
    //     // Teleports() call
    //     Debug.Log($"Before calling {methodNameToTest}");
    //
    //     Debug.Log($"I'm passing the scene with the name: {nameBefore}");
    //     transitionScriptReference.Teleports(sceneToPass);
    //
    //     Debug.Log($"After calling {methodNameToTest}");
    //
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);
    //
    //     // Check if the current scene didn't change
    //     Assert.AreNotEqual(nameBefore, SceneManager.GetActiveScene().name);
    //     Assert.AreEqual(scenePath, SceneManager.GetActiveScene().path);
    //
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);
    //
    //     /*
    //     SceneManager.LoadSceneAsync(scenePath, LoadSceneMode.Single);
    //     yield return WaitForScene();
    //     //SceneManager.SetActiveScene(SceneManager.GetSceneByPath(scenePath));
    //     if (SceneManager.GetSceneByName(nameBefore).IsValid())
    //         SceneManager.UnloadSceneAsync(nameBefore);
    //     
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);*/
    // }
    //
    // [UnityTest]
    // public IEnumerator TeleportsPassingExistingScene()
    // {
    //     Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
    //     methodNameToTest = "Teleports()";
    //     yield return WaitForScene();
    //
    //     // Instantiate the object required for testing
    //     var myObject = GameObject.Find(objInstanceName);
    //     Debug.Log($"Loading GameObject {myObject.name}");
    //
    //     // Getting the reference to Transition script
    //     var transitionScriptReference = myObject.GetComponent<Transition>();
    //
    //     // Defining the scene
    //     var sceneToPass = GameObject.Find("/Scene1").GetComponent<Scene>();
    //     //const string baseSceneName = "SceneThatDoesNotExist";
    //     const string baseSceneName = "SampleScene";
    //     sceneToPass.name = Copy(baseSceneName);
    //
    //     // Changing the sceneName as long as the scene with that name exists
    //     /*while (DoesSceneExist(sceneToPass.name))
    //         sceneToPass.name += "a";*/
    //     var nameBefore = Copy(sceneToPass.name);
    //
    //     // Teleports() call
    //     Debug.Log($"Before calling {methodNameToTest}");
    //
    //     Debug.Log($"I'm passing the scene with the name: {nameBefore}");
    //     transitionScriptReference.Teleports(sceneToPass);
    //     Debug.Log($"After calling {methodNameToTest}");
    //
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);
    //
    //     // Check if the current scene is the one passed
    //     Assert.AreEqual(nameBefore, SceneManager.GetActiveScene().name);
    //
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);
    //
    //     // If you are here, it means that we must reload the scene test
    //     SceneManager.LoadSceneAsync(scenePath, LoadSceneMode.Single);
    //     yield return WaitForScene();
    //     //SceneManager.SetActiveScene(SceneManager.GetSceneByPath(scenePath));
    //     if (SceneManager.GetSceneByName(nameBefore).IsValid())
    //         SceneManager.UnloadSceneAsync(nameBefore);
    //
    //     // Wait a little
    //     yield return new WaitForSeconds(ShortTimeDelay);
    // }
}