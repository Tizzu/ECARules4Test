using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class LockPlayModeTestSuite : BaseTestSuite
{
    
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Lock1";
    }

    private const string opensVerb = "opens";
    private const string closesVerb = "closes";
    
    [UnityTest]
    public IEnumerator OpensWhenIsLocked()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();
        methodNameToTest = "Opens()";
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lockScriptReference = myObject.GetComponent<Lock>();

        // Forcing the locked status to true
        lockScriptReference.locked = true;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lockScriptReference.Opens();
        Action action = new Action(myObject, opensVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;
        
        // Check if the current status is equivalent to the expected one
        Assert.IsFalse(lockScriptReference.locked);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator OpensWhenIsNotLocked()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();
        methodNameToTest = "Opens()";
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lockScriptReference = myObject.GetComponent<Lock>();

        // Forcing the locked status to false
        lockScriptReference.locked = false;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lockScriptReference.Opens();
        Action action = new Action(myObject, opensVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;
        
        // Check if the current status is equivalent to the expected one
        Assert.IsFalse(lockScriptReference.locked);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator ClosesWhenIsLocked()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();
        methodNameToTest = "Closes()";
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lockScriptReference = myObject.GetComponent<Lock>();

        // Forcing the status to true
        lockScriptReference.locked = true;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lockScriptReference.Closes();
        Action action = new Action(myObject, closesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;
        
        // Check if the current status is equivalent to the expected one
        Assert.IsTrue(lockScriptReference.locked);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator ClosesWhenIsNotLocked()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();
        methodNameToTest = "Closes()";
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to Electronic script
        var lockScriptReference = myObject.GetComponent<Lock>();

        // Forcing the locked status to false
        lockScriptReference.locked = false;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // lockScriptReference.Closes();
        Action action = new Action(myObject, closesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;
        
        // Check if the current status is equivalent to the expected one
        Assert.IsTrue(lockScriptReference.locked);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}
