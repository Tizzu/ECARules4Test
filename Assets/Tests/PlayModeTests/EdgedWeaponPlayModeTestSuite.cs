using System;
using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class EdgedWeaponPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/EdgedWeapon";
    }

    private const string stabsVerb = "stabs";
    private const string slicesVerb = "slices";
    
    
    [UnityTest]
    public IEnumerator LoadingStabsAndSlicesRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"EdgedWeaponSlices&StabsRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }
    
    [UnityTest]
    public IEnumerator StabsPassingNullObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Stabs()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to edged weapons script
        var edgedWeaponScriptReference = myObject.GetComponent<EdgedWeapon>();
        
        // Stabs() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // edgedWeaponScriptReference.Stabs(null);
            Action action = new Action(myObject, stabsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator StabsPassingNotNullObjectWithCharacter()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Stabs()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject_attacker = GameObject.Find("/HumanWithEdgedWeapon1");
        var myObject_attacked = GameObject.Find("/Character1");
        Debug.Log($"Loading GameObject {myObject_attacker.name}");
        Debug.Log($"Loading GameObject {myObject_attacked.name}");

        // Getting the reference to edged weapons script
        var edgedWeaponScriptReference = myObject_attacker.GetComponent<EdgedWeapon>();
        var ecaObjectAttacked = myObject_attacked.GetComponent<ECAObject>();

        var life_before = ecaObjectAttacked.GetComponent<Character>().life = 10.0f;
        myObject_attacker.GetComponent<Weapon>().power = 5;

        // Stabs() call
        Debug.Log($"Before calling {methodNameToTest}");
        // edgedWeaponScriptReference.Stabs(ecaObjectAttacked);
        Action action = new Action(myObject_attacker, stabsVerb, myObject_attacked);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);
        
        var life_after = ecaObjectAttacked.GetComponent<Character>().life;
        
        Assert.Greater(life_before, life_after);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {ecaObjectAttacked.name}");
        Debug.Log($"Destroying GameObject {myObject_attacker.name}");
        Object.Destroy(myObject_attacked);
        Object.Destroy(myObject_attacker);
    }

    [UnityTest]
    public IEnumerator StabsPassingNotNullObjectWithoutCharacter()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Stabs()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject_attacker = GameObject.Find("/HumanWithEdgedWeapon1");
        var myObject_attacked_noCharacter = GameObject.Find("/ECAObject1");
        Debug.Log($"Loading GameObject {myObject_attacker.name}");
        Debug.Log($"Loading GameObject {myObject_attacked_noCharacter.name}");

        // Getting the reference to edged weapons script
        var edgedWeaponScriptReference = myObject_attacker.GetComponent<EdgedWeapon>();
        var ecaObjectAttacked = myObject_attacked_noCharacter.GetComponent<ECAObject>();
        
        myObject_attacker.GetComponent<Weapon>().power = 5;

        // Stabs() call
        Debug.Log($"Before calling {methodNameToTest}");
        // edgedWeaponScriptReference.Stabs(ecaObjectAttacked);
        Action action = new Action(myObject_attacker, stabsVerb, myObject_attacked_noCharacter);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {ecaObjectAttacked.name}");
        Debug.Log($"Destroying GameObject {myObject_attacker.name}");
        Object.Destroy(myObject_attacked_noCharacter);
        Object.Destroy(myObject_attacker);
    }

    [UnityTest]
    public IEnumerator SlicesPassingNullObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Slices()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to edged weapons script
        var edgedWeaponScriptRefence = myObject.GetComponent<EdgedWeapon>();
        
        // Slices() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // edgedWeaponScriptRefence.Slices(null);
            Action action = new Action(myObject, slicesVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator SlicesPassingNotNullObjectWithCharacter()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Slices()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject_attacker = GameObject.Find("/HumanWithEdgedWeapon1");
        var myObject_attacked = GameObject.Find("/Character1");
        Debug.Log($"Loading GameObject {myObject_attacker.name}");
        Debug.Log($"Loading GameObject {myObject_attacked.name}");

        // Getting the reference to edged weapons script
        var edgedWeaponScriptReference = myObject_attacker.GetComponent<EdgedWeapon>();
        var ecaObjectAttacked = myObject_attacked.GetComponent<ECAObject>();

        var life_before = ecaObjectAttacked.GetComponent<Character>().life = 10.0f;
        myObject_attacker.GetComponent<Weapon>().power = 5;

        // Slices() call
        Debug.Log($"Before calling {methodNameToTest}");
        // edgedWeaponScriptReference.Slices(ecaObjectAttacked);
        Action action = new Action(myObject_attacker, stabsVerb, myObject_attacked);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);
        
        var life_after = ecaObjectAttacked.GetComponent<Character>().life;
        
        Assert.Greater(life_before, life_after);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {ecaObjectAttacked.name}");
        Debug.Log($"Destroying GameObject {myObject_attacker.name}");
        Object.Destroy(myObject_attacked);
        Object.Destroy(myObject_attacker);
    }

    [UnityTest]
    public IEnumerator SlicesPassingNotNullObjectWithoutCharacter()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Slices()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject_attacker = GameObject.Find("/HumanWithEdgedWeapon1");
        var myObject_attacked_noCharacter = GameObject.Find("/ECAObject1");
        Debug.Log($"Loading GameObject {myObject_attacker.name}");
        Debug.Log($"Loading GameObject {myObject_attacked_noCharacter.name}");

        // Getting the reference to edged weapons script
        var edgedWeaponScriptReference = myObject_attacker.GetComponent<EdgedWeapon>();
        var ecaObjectAttacked = myObject_attacked_noCharacter.GetComponent<ECAObject>();
        
        myObject_attacker.GetComponent<Weapon>().power = 5;

        // Slices() call
        Debug.Log($"Before calling {methodNameToTest}");
        // edgedWeaponScriptReference.Slices(ecaObjectAttacked);
        Action action = new Action(myObject_attacker, slicesVerb, myObject_attacked_noCharacter);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {ecaObjectAttacked.name}");
        Debug.Log($"Destroying GameObject {myObject_attacker.name}");
        Object.Destroy(myObject_attacked_noCharacter);
        Object.Destroy(myObject_attacker);
    }
}
