using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class CameraPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Camera1";
    }

    [UnityTest]
    public IEnumerator LoadingZoomsInAndZoomsOutRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"ECACameraZoomsIn&ZoomsOutRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }
    
    [UnityTest]
    public IEnumerator LoadingChangesPOVRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"ECACameraChangesPOV.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }
    
    private const float minZoom = 30f;
    private const float maxZoom = 100f;
    
    private const string zoomsinVerb = "zooms-in";
    private const string zoomsoutVerb = "zooms-out";
    private const string changesPovVerb = "changes POV to";
    
    [UnityTest]
    public IEnumerator TestingIfEcaCameraZoomIsEqualToCameraZoom()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var ecaZoomStart = myObject.GetComponent<ECACamera>().zoomLevel;
        var zoomStart = myObject.GetComponent<Camera>().fieldOfView;

        // Check if the zoom is in the interval [minZoom, maxZoom]
        Assert.AreEqual(ecaZoomStart, zoomStart);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TestingIfStartZoomExceedsMinZoom()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();

        // Instantiate the object required for testing
        objInstanceName = "/CameraWithZoomLessThanMinimum";
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var zoomStart = myObject.GetComponent<ECACamera>().zoomLevel;

        // Check if the zoom is in the interval [minZoom, maxZoom]
        Assert.GreaterOrEqual(zoomStart, minZoom);
        Assert.LessOrEqual(zoomStart, maxZoom);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TestingIfStartZoomExceedsMaxZoom()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();

        // Instantiate the object required for testing
        objInstanceName = "/CameraWithZoomGreaterThanMaximum";
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var zoomStart = myObject.GetComponent<ECACamera>().zoomLevel;

        // Check if the zoom is in the interval [minZoom, maxZoom]
        Assert.GreaterOrEqual(zoomStart, minZoom);
        Assert.LessOrEqual(zoomStart, maxZoom);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #region ZoomIn

    [UnityTest]
    public IEnumerator ZoomInPassingNullAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ZoomIn()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Save the current zoom
        var ecaZoomBefore = cameraScriptReference.zoomLevel;
        var zoomBefore = cameraScriptReference.GetComponent<Camera>().fieldOfView;


        // Method call
        const float amountToPass = 0f;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ZoomsIn(amountToPass);
        Action action = new Action(myObject, zoomsinVerb, amountToPass);
        RuleEngine.GetInstance().ExecuteAction(action); 
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current zoom
        var ecaZoomAfter = cameraScriptReference.zoomLevel;
        var zoomAfter = cameraScriptReference.GetComponent<Camera>().fieldOfView;


        // Check if the zoom are the same
        Assert.AreEqual(ecaZoomBefore, ecaZoomAfter);
        Assert.AreEqual(zoomBefore, zoomAfter);
        Assert.AreEqual(zoomAfter, ecaZoomAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ZoomInPassingNegativeAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ZoomIn()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Save the current zoom
        var ecaZoomBefore = cameraScriptReference.zoomLevel = minZoom;
        var zoomBefore = myObject.GetComponent<Camera>().fieldOfView = minZoom;
        
        // Method call
        const float amountToPass = -10;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ZoomsIn(amountToPass);
        Action action = new Action(myObject, zoomsinVerb, amountToPass);
        RuleEngine.GetInstance().ExecuteAction(action); 
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current zoom
        var ecaZoomAfter = cameraScriptReference.zoomLevel;
        var zoomAfter = myObject.GetComponent<Camera>().fieldOfView;

        // Check if the zoom are the same
        Assert.AreEqual(ecaZoomBefore - amountToPass, ecaZoomAfter);
        Assert.AreEqual(zoomBefore - amountToPass, zoomAfter);
        Assert.AreEqual(zoomAfter, ecaZoomAfter);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ZoomInPassingPositiveAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ZoomIn()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Save the current zoom
        var ecaZoomBefore = cameraScriptReference.zoomLevel = maxZoom;
        var zoomBefore = myObject.GetComponent<Camera>().fieldOfView = maxZoom;

        // Method call
        const float amountToPass = 10;
        Debug.Log($"Before calling {methodNameToTest}");
        // cameraScriptReference.ZoomsIn(amountToPass);
        Action action = new Action(myObject, zoomsinVerb, amountToPass);
        RuleEngine.GetInstance().ExecuteAction(action); 
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current zoom
        var ecaZoomAfter = cameraScriptReference.zoomLevel;
        var zoomAfter = myObject.GetComponent<Camera>().fieldOfView;

        // Check if the zoom are the same
        Assert.AreEqual(ecaZoomBefore - amountToPass, ecaZoomAfter);
        Assert.AreEqual(zoomBefore - amountToPass, zoomAfter);
        Assert.AreEqual(zoomAfter, ecaZoomAfter);


        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ZoomInPassingPositiveAmountExceedingLimit()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ZoomIn()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Save the current zoom
        var ecaZoomBefore = cameraScriptReference.zoomLevel = minZoom;
        var zoomBefore = myObject.GetComponent<Camera>().fieldOfView = minZoom;

        // Method call
        const float amountToPass = 1;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ZoomsIn(amountToPass);
        Action action = new Action(myObject, zoomsinVerb, amountToPass);
        RuleEngine.GetInstance().ExecuteAction(action); 
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current zoom
        var ecaZoomAfter = cameraScriptReference.zoomLevel;
        var zoomAfter = myObject.GetComponent<Camera>().fieldOfView;

        // Check if the zoom are the same
        Assert.AreEqual(ecaZoomBefore, ecaZoomAfter);
        Assert.AreEqual(zoomBefore, zoomAfter);
        Assert.AreEqual(zoomAfter, ecaZoomAfter);


        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #endregion

    #region ZoomOut

    [UnityTest]
    public IEnumerator ZoomOutPassingNullAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ZoomOut()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Save the current zoom
        var ecaZoomBefore = cameraScriptReference.zoomLevel;
        var zoomBefore = myObject.GetComponent<Camera>().fieldOfView;

        // Method call
        const float amountToPass = 0f;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ZoomsOut(amountToPass);
        Action action = new Action(myObject, zoomsoutVerb, amountToPass);
        RuleEngine.GetInstance().ExecuteAction(action); 
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current zoom
        var ecaZoomAfter = cameraScriptReference.zoomLevel;
        var zoomAfter = myObject.GetComponent<Camera>().fieldOfView;
        
        // Check if the zoom are the same
        Assert.AreEqual(ecaZoomBefore , ecaZoomAfter);
        Assert.AreEqual(zoomBefore , zoomAfter);
        Assert.AreEqual(ecaZoomAfter, zoomAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ZoomOutPassingNegativeAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ZoomOut()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Save the current zoom
        var ecaZoomBefore = cameraScriptReference.zoomLevel = maxZoom;
        var zoomBefore = myObject.GetComponent<Camera>().fieldOfView = maxZoom;

        // Method call
        const float amountToPass = -10;
        Debug.Log($"Before calling {methodNameToTest}");
        // cameraScriptReference.ZoomsOut(amountToPass);
        Action action = new Action(myObject, zoomsoutVerb, amountToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current zoom
        var ecaZoomAfter = cameraScriptReference.zoomLevel;
        var zoomAfter = myObject.GetComponent<Camera>().fieldOfView;
        
        // Check if the zoom are the same
        Assert.AreEqual(ecaZoomBefore + amountToPass , ecaZoomAfter);
        Assert.AreEqual(zoomBefore + amountToPass, zoomAfter);
        Assert.AreEqual(ecaZoomAfter, zoomAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ZoomOutPassingPositiveAmount()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ZoomOut()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Save the current zoom
        var ecaZoomBefore = cameraScriptReference.zoomLevel = minZoom;
        var zoomBefore = myObject.GetComponent<Camera>().fieldOfView = minZoom;
        
        // Method call
        const float amountToPass = 10;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ZoomsOut(amountToPass);
        Action action = new Action(myObject, zoomsoutVerb, amountToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current zoom
        var ecaZoomAfter = cameraScriptReference.zoomLevel;
        var zoomAfter = myObject.GetComponent<Camera>().fieldOfView;

        // Check if the zoom are the same
        Assert.AreEqual(ecaZoomBefore + amountToPass , ecaZoomAfter);
        Assert.AreEqual(zoomBefore + amountToPass, zoomAfter);
        Assert.AreEqual(ecaZoomAfter, zoomAfter);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ZoomOutPassingPositiveAmountExceedingLimit()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ZoomIn()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Save the current zoom
        var ecaZoomBefore = cameraScriptReference.zoomLevel = maxZoom;
        var zoomBefore = myObject.GetComponent<Camera>().fieldOfView = maxZoom;
        
        // Method call
        const float amountToPass = 1;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ZoomsOut(amountToPass);
        Action action = new Action(myObject, zoomsoutVerb, amountToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current zoom
        var ecaZoomAfter = cameraScriptReference.zoomLevel;
        var zoomAfter = myObject.GetComponent<Camera>().fieldOfView;
            
        // Check if the zoom are the same
        Assert.AreEqual(ecaZoomBefore, ecaZoomAfter);
        Assert.AreEqual(zoomBefore, zoomAfter);
        Assert.AreEqual(zoomAfter, ecaZoomAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #endregion

    #region ChangesPov

    [UnityTest]
    public IEnumerator ChangesPovPassingThirdWhenPovIsThird()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesPov()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Forcing the pov to third
        cameraScriptReference.pov = ECACamera.POV.Third;

        // Method call
        const ECACamera.POV povToPass = ECACamera.POV.Third;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ChangesPOV(povToPass);
        Action action = new Action(myObject, changesPovVerb, povToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Check if POVs are equal
        Assert.AreEqual(cameraScriptReference.pov, povToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ChangesPovPassingThirdWhenPovIsFirst()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesPov()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Forcing the pov to third
        cameraScriptReference.pov = ECACamera.POV.First;

        // Method call
        const ECACamera.POV povToPass = ECACamera.POV.Third;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ChangesPOV(povToPass);
        Action action = new Action(myObject, changesPovVerb, povToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Check if POVs are equal
        Assert.AreEqual(cameraScriptReference.pov, povToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ChangesPovPassingFirstWhenPovIsThird()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesPov()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Forcing the pov to third
        cameraScriptReference.pov = ECACamera.POV.Third;

        // Method call
        const ECACamera.POV povToPass = ECACamera.POV.First;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ChangesPOV(povToPass);
        Action action = new Action(myObject, changesPovVerb, povToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Check if POVs are equal
        Assert.AreEqual(cameraScriptReference.pov, povToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator ChangesPovPassingFirstWhenPovIsFirst()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "ChangesPov()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript and CharacterScript
        var cameraScriptReference = myObject.GetComponent<ECACamera>();

        // Forcing the pov to third
        cameraScriptReference.pov = ECACamera.POV.First;

        // Method call
        const ECACamera.POV povToPass = ECACamera.POV.First;
        Debug.Log($"Before calling {methodNameToTest})");
        // cameraScriptReference.ChangesPOV(povToPass);
        Action action = new Action(myObject, changesPovVerb, povToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest})");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Check if POVs are equal
        Assert.AreEqual(cameraScriptReference.pov, povToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    #endregion
    
   /* [UnityTest]
    public IEnumerator TestingIfPlayingActivesCamera()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        yield return new WaitForSeconds(ShortTimeDelay);
        
        // Getting the reference to ECAObjectScript and CharacterScript
        
        // TODO capire come si ottiene la camera che sta renderizzando
        var cameraIsEnabled = myObject.GetComponent<Camera>().enabled;
        var isPlaying = myObject.GetComponent<ECACamera>().playing;
        
        // check if bools are equivalent
        //Assert.AreEqual(isPlaying , cameraIsEnabled);
        System.Diagnostics.Debug.Assert(Camera.current != null, "Camera.main != null");
        Assert.AreEqual(Camera.current.name, myObject.name);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }*/
}