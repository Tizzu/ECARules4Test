using System;
using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Object = UnityEngine.Object;

public class ContainerPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Container1";
    }

    [UnityTest]
    public IEnumerator LoadingInsertsRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"ContainerInsertsRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }

    private const string removesVerb = "removes";
    private const string emptiesVerb = "empties";
    
    [UnityTest]
    public IEnumerator InsertsPassingNullObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Inserts()";

        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var containerScriptReference = myObject.GetComponent<Container>();

        // if the container has 0 capacity or if it is full
        if (containerScriptReference.capacity == 0 ||
            containerScriptReference.capacity == containerScriptReference.objectsCount)
            // increase the size of 1
            containerScriptReference.capacity++;

        // save the counter before the method call
        var counterBefore = containerScriptReference.objectsCount;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // containerScriptReference.Inserts(null);
            Action action = new Action(myObject, insertsVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // save the counter after the methdo call
        var counterAfter = containerScriptReference.objectsCount;

        // Check if the counter is the same
        Assert.AreEqual(counterBefore, counterAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator InsertsPassingNotNullObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Inserts()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        var objectToInsert = GameObject.Find("/Food1");
        Debug.Log($"Loading GameObject {objectToInsert.name}");
        
        // Getting the reference to the switch script
        var containerScriptReference = myObject.GetComponent<Container>();

        // if the container has 0 capacity or if it is full
        if (containerScriptReference.capacity == 0 ||
            containerScriptReference.capacity == containerScriptReference.objectsCount)
            // increase the size of 1
            containerScriptReference.capacity++;

        // save the counter before the method call
        var counterBefore = containerScriptReference.objectsCount;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // containerScriptReference.Inserts(objectToInsert);
        Action action = new Action(myObject, insertsVerb, objectToInsert);
        /* NON CAMBIA NIENTE
        Action action = new Action(myObject, insertsVerb, objectToInsert.GetComponent<ECAObject>());*/
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // save the counter after the methdo call
        var counterAfter = containerScriptReference.objectsCount;

        // Check if the object has been inserted accurately
        Assert.AreEqual(counterBefore + 1, counterAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator RemovesPassingNullObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Removes()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the container script
        var containerScriptReference = myObject.GetComponent<Container>();

        // if the container has 0 capacity or if it is full
        if (containerScriptReference.capacity == 0 ||
            containerScriptReference.capacity == containerScriptReference.objectsCount)
        {
            // increase the size of 1
            containerScriptReference.capacity++;
        }

        // save the counter before the method call
        var counterBefore = containerScriptReference.objectsCount;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // containerScriptReference.Removes(null);
            Action action = new Action(myObject, removesVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // save the counter after the method call
        var counterAfter = containerScriptReference.objectsCount;

        // Check if the object hasn't been inserted
        Assert.AreEqual(counterBefore, counterAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator RemovesPassingEcaObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Removes()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        var objectToDelete = GameObject.Find("/Food1");
        Debug.Log($"Loading GameObject {objectToDelete.name}");


        // Getting the reference to the container script
        var containerScriptReference = myObject.GetComponent<Container>();

        // if the container has 0 capacity or if it is full
        if (containerScriptReference.capacity == 0 ||
            containerScriptReference.capacity == containerScriptReference.objectsCount)
        {
            // increase the size of 1
            containerScriptReference.capacity++;
        }

        // Insert the object to delete in the future
        //containerScriptReference.Inserts(objectToDelete);
        Action action = new Action(myObject, insertsVerb, objectToDelete);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        // save the counter before the method call
        var counterBefore = containerScriptReference.objectsCount;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // containerScriptReference.Removes(objectToDelete);
        action = new Action(myObject, removesVerb, objectToDelete);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // save the counter after the method call
        var counterAfter = containerScriptReference.objectsCount;

        // Check if the object hasn't been inserted
        Assert.AreEqual(counterBefore - 1, counterAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator EmptiesWhenObjectListIsEmpty()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Empties()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the container script
        var containerScriptReference = myObject.GetComponent<Container>();

        //  if the list is not empty
        if (containerScriptReference.objectsCount != 0)
        {
            // empty it
            // containerScriptReference.Empties();
            Action action = new Action(myObject, emptiesVerb);
            RuleEngine.GetInstance().ExecuteAction(action);
        }

        // if the container has 0 capacity or if it is full
        if (containerScriptReference.capacity == 0 ||
            containerScriptReference.capacity == containerScriptReference.objectsCount)
        {
            // increase the size of 1
            containerScriptReference.capacity++;
        }

        // save the counter before the method call
        var counterBefore = containerScriptReference.objectsCount;

        // Check if the object hasn't been inserted
        Assert.AreEqual(0, counterBefore);

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // containerScriptReference.Empties();
            Action action = new Action(myObject, emptiesVerb);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // save the counter after the method call
        var counterAfter = containerScriptReference.objectsCount;

        // Check if the object hasn't been inserted
        Assert.AreEqual(0, counterAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator EmptiesWhenObjectListIsNotEmpty()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Empties()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the container script
        var containerScriptReference = myObject.GetComponent<Container>();

        // if the container has 0 capacity or if it is full
        if (containerScriptReference.capacity == 0 ||
            containerScriptReference.capacity == containerScriptReference.objectsCount)
        {
            // increase the size of 1
            containerScriptReference.capacity++;
        }

        Action action;
        //  if the list is empty
        if (containerScriptReference.objectsCount == 0)
        {
            // add some object
            // containerScriptReference.Inserts(GameObject.Find("/Food1"));
            action = new Action(myObject, insertsVerb, GameObject.Find("/Food1"));
            RuleEngine.GetInstance().ExecuteAction(action);
        }

        // save the counter before the method call
        var counterBefore = containerScriptReference.objectsCount;

        // Check if the object hasn't been inserted
        Assert.AreNotEqual(0, counterBefore);

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // containerScriptReference.Empties();
        action = new Action(myObject, emptiesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // save the counter after the method call
        var counterAfter = containerScriptReference.objectsCount;

        // Check if the object hasn't been inserted
        Assert.AreEqual(0, counterAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}