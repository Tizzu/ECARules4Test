using System;
using System.Collections;
using ECARules4All.RuleEngine; 
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

public class ParserPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/chParser";
    }

    [UnityTest]
    public IEnumerator SimpleRuleLoad()
    {
        string path = "Assets/Resources/ruleTest2.txt";
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "";
        yield return WaitForScene();

        // Instantiate the subject and object required for testing
        var subject = GameObject.Find(objInstanceName);
        var food = GameObject.Find("/Food1");

        Debug.Log($"Loading GameObject {subject.name}");

        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);

        int count = 0;
        foreach(Rule r in RuleEngine.GetInstance().Rules())
        {
            Assert.AreSame(r.GetEvent().GetSubject(), subject);
            Assert.AreEqual(r.GetEvent().GetActionMethod(), "interacts");
            Assert.AreSame(r.GetEvent().GetObject(), food);
            Assert.AreEqual(r.GetActions().Count, 2);
            Assert.AreSame(r.GetActions()[0].GetSubject(), subject);
            Assert.AreEqual(r.GetActions()[0].GetActionMethod(), "eats");
            Assert.AreSame(r.GetActions()[0].GetObject(), food);
            Assert.AreSame(r.GetActions()[1].GetSubject(), subject);
            Assert.AreEqual(r.GetActions()[1].GetActionMethod(), "changes");
            Assert.AreEqual(r.GetActions()[1].GetObject(), "life");
            Assert.AreEqual(r.GetActions()[1].GetModifier(), "to");
            Assert.AreEqual(r.GetActions()[1].GetModifierValue(), 17);
            if ( count == 1)
            {
                Assert.AreSame(((SimpleCondition)r.GetCondition()).GetSubject(), food);
                Assert.AreEqual(((SimpleCondition)r.GetCondition()).GetSymbol(), "=");
                Assert.AreEqual(((SimpleCondition)r.GetCondition()).GetValueToCompare(), 17);
            }
            if( count == 2)
            {
                CompositeCondition c = (CompositeCondition)r.GetCondition();
                Assert.AreEqual(c.ChildrenCount(), 2);
                Assert.AreEqual(c.Op, CompositeCondition.ConditionType.AND);
                SimpleCondition s1 = (SimpleCondition) c.GetChild(0);
                SimpleCondition s2 = (SimpleCondition)c.GetChild(1);
                Assert.AreSame(s1.GetSubject(), food);
                Assert.AreEqual(s1.GetSymbol(), "=");
                Assert.AreEqual(s1.GetValueToCompare(), 17);
                Assert.AreSame(s2.GetSubject(), food);
                Assert.AreEqual(s2.GetSymbol(), "<");
                Assert.AreEqual(s2.GetValueToCompare(), 34);
            }
            count++;

        }

    }

    [UnityTest]
    public IEnumerator SimpleRuleSave()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "";
        yield return WaitForScene();

        // Instantiate the subject and object required for testing
        var subject = GameObject.Find(objInstanceName);
        var food = GameObject.Find("/Food1");

        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile("Assets/Resources/ruleTest2.txt");


        TextRuleSerializer serializer = new TextRuleSerializer();

        Assert.AreEqual(typeof(Human), serializer.GetECAObjectType(subject));
        Assert.AreEqual(typeof(Food), serializer.GetECAObjectType(food));

        serializer.SaveRules("Assets/Resources/savedRules.txt");

    }
}