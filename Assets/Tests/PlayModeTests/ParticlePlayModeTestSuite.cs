using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


public class ParticlePlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Particle1";
    }

    [UnityTest]
    public IEnumerator TurnsPassingTrueWhenParticleIsOn()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var particleScriptReference = myObject.GetComponent<Particle>();
        
        // Forcing the status of the switch
        particleScriptReference.@on = true;

        // setting the input to pass
        const bool inputToPass = true;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // particleScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the switch status is equivalent to the input passed
        Assert.IsTrue(particleScriptReference.@on == inputToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TurnsPassingTrueWhenParticleIsOff()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var particleScriptReference = myObject.GetComponent<Particle>();

        // Forcing the status of the switch
        particleScriptReference.@on = false;

        // setting the input to pass
        const bool inputToPass = true;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // particleScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the switch status is equivalent to the input passed
        Assert.IsTrue(particleScriptReference.@on == inputToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TurnsPassingFalseWhenParticleIsOn()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var particleScriptReference = myObject.GetComponent<Particle>();

        // Forcing the status of the switch
        particleScriptReference.@on = true;

        // setting the input to pass
        const bool inputToPass = false;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // particleScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the switch status is equivalent to the input passed
        Assert.IsTrue(particleScriptReference.@on == inputToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator TurnsPassingFalseWhenParticleIsOff()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var particleScriptReference = myObject.GetComponent<Particle>();

        // Forcing the status of the switch
        particleScriptReference.@on = false;

        // setting the input to pass
        const bool inputToPass = false;

        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // particleScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the switch status is equivalent to the input passed
        Assert.IsTrue(particleScriptReference.@on == inputToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}