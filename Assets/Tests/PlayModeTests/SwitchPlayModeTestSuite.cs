using System.Collections;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class SwitchPlayModeTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/Switch1";
    }

    [UnityTest]
    public IEnumerator LoadingTurnsRuleTxtFile()
    {
        yield return WaitForScene();

        string path = pathForTextRuleParser+"SwitchTurnsRules.txt";
        TextRuleParser parser = new TextRuleParser();
        parser.ReadRuleFile(path);
        PrintRuleEngineStatus();
    }

    [UnityTest]
    public IEnumerator TurnsPassingTrueWhenSwitchIsOn()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();
        
        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var switchScriptReference = myObject.GetComponent<Switch>();

        // Forcing the status of the switch
        switchScriptReference.@on = true;

        // setting the input to pass
        const bool inputToPass = true;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // switchScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the switch status is equivalent to the input passed
        Assert.IsTrue(switchScriptReference.@on == inputToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator TurnsPassingTrueWhenSwitchIsOff()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var switchScriptReference = myObject.GetComponent<Switch>();

        // Forcing the status of the switch
        switchScriptReference.@on = false;

        // setting the input to pass
        const bool inputToPass = true;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // switchScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the switch status is equivalent to the input passed
        Assert.IsTrue(switchScriptReference.@on == inputToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator TurnsPassingFalseWhenSwitchIsOn()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var switchScriptReference = myObject.GetComponent<Switch>();

        // Forcing the status of the switch
        switchScriptReference.@on = true;

        // setting the input to pass
        const bool inputToPass = false;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        // switchScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the switch status is equivalent to the input passed
        Assert.IsTrue(switchScriptReference.@on == inputToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator TurnsPassingFalseWhenSwitchIsOff()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Turns()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to the switch script
        var switchScriptReference = myObject.GetComponent<Switch>();

        // Forcing the status of the switch
        switchScriptReference.@on = false;

        // setting the input to pass
        const bool inputToPass = false;
        
        // Method call
        Debug.Log($"Before calling {methodNameToTest}");
        //switchScriptReference.Turns(inputToPass);
        Action action = new Action(myObject, turnsVerb, inputToPass);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait 0
        yield return null;

        // Check if the switch status is equivalent to the input passed
        Assert.IsTrue(switchScriptReference.@on == inputToPass);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
}
