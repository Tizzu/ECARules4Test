using System;
using System.Collections;
using ECARules4All;
using ECARules4All.RuleEngine;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Action = ECARules4All.RuleEngine.Action;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;


public class ECAObjectTestSuite : BaseTestSuite
{
    public override void Setup()
    {
        base.Setup();
        objInstanceName = "/ECAObject1";
    }

    private const string movesVerb = "moves to";
    private const string rotatesVerb = "rotates towards";
    private const string looksVerb = "looks at";
    private const string showsVerb = "shows";
    private const string hidesVerb = "hides";
    private const string activatesVerb = "activates";
    private const string deactivatesVerb = "deactivates";
    
    [UnityTest]
    public IEnumerator MovesPassingNullPosition()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Moves()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Saving the position before calling the method
        var ecaObjectPositionBefore = new Position();
        ecaObjectPositionBefore.Assign(ecaObjectScriptReference.p);
        
        var positionBefore = myObject.transform.position;

        // Moves() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            Action action = new Action(myObject, movesVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
            // ecaObjectScriptReference.Moves(null);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaObjectPositionAfter = new Position();
        ecaObjectPositionAfter.Assign(ecaObjectScriptReference.p);
        var positionAfter = myObject.transform.position;

        // Check if the positions are the same
        PositionsAreEqual(ecaObjectPositionBefore, ecaObjectPositionAfter);
        Assert.AreEqual(positionBefore, positionAfter);


        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator MovesWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Moves()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Saving the position before calling the method
        var ecaPositionBefore = new Position();
        ecaPositionBefore.Assign(ecaObjectScriptReference.p);
        var positionBefore = myObject.transform.position;

        //Position Object
        var pos = new Position();
        pos.x = 50.0f;
        pos.y = 2.0f;
        pos.z = 4.0f;
        var p = positionBefore;
        if (pos.x == ecaPositionBefore.x && pos.y == ecaPositionBefore.y && pos.z == ecaPositionBefore.z)
        {
            pos.x += 1.0f;
            pos.y += 1.0f;
            pos.z += 1.0f;
            p.x += 1.0f;
            p.y += 1.0f;
            p.z += 1.0f;
            positionBefore = p;
        }
        
        // Moves() call
        Debug.Log($"Before calling {methodNameToTest}");
        ecaObjectScriptReference.isActive = false;
        // ecaObjectScriptReference.Moves(pos);
        Action action = new Action(myObject, movesVerb, pos);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaPositionAfter = new Position();
        ecaPositionAfter.Assign(ecaObjectScriptReference.p);
        var positionAfter = myObject.transform.position;

        // Check if the positions are the same
        PositionsAreEqual(ecaPositionBefore, ecaPositionAfter);
        Assert.AreEqual(positionBefore, positionAfter);


        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    /*[UnityTest]
    public IEnumerator MovesPassingNonValidePosition()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Moves()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Saving the position before calling the method
        var ecaObjectPositionBefore = new Position();
        ecaObjectPositionBefore.Assign(ecaObjectScriptReference.p);
        var positionBefore = myObject.transform.position;
        
        var pos = new Position();
        pos.x = 1000000.0f;
        pos.y = 1000000.0f;
        pos.z = 1000000.0f;

        // Moves() call
        Debug.Log($"Before calling {methodNameToTest}");
       
        ecaObjectScriptReference.Moves(pos);

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaObjectPositionAfter = new Position();
        ecaObjectPositionAfter.Assign(ecaObjectScriptReference.p);
        var positionAfter = myObject.transform.position;

        // Check if the positions are the same
        PositionsAreEqual(ecaObjectPositionBefore, ecaObjectPositionAfter);
        Assert.AreEqual(positionBefore, positionAfter);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }*/
    
    [UnityTest]
    public IEnumerator MovesPassingValidePosition()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Moves()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Saving the position before calling the method
        var ecaObjectPositionBefore = new Position();
        ecaObjectPositionBefore.Assign(ecaObjectScriptReference.p);
        
        var positionBefore = myObject.transform.position;
        
        var pos = new Position();
        pos.x = 1.0f;
        pos.y = 1.0f;
        pos.z = 1.8f;

        if (pos.x == positionBefore.x && pos.y == positionBefore.y && pos.z == positionBefore.z)
            pos.x++;
            
        // Moves() call
        Debug.Log($"Before calling {methodNameToTest}");
       
        //ecaObjectScriptReference.Moves(pos);
        Action action = new Action(myObject, movesVerb, pos);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaObjectPositionAfter = new Position();
        ecaObjectPositionAfter.Assign(ecaObjectScriptReference.p);
        var positionAfter = myObject.transform.position;

        // Check if the positions are not the same
        PositionsAreNotEqual(ecaObjectPositionBefore, ecaObjectPositionAfter);
        Assert.AreNotEqual(positionBefore, positionAfter);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    /*[UnityTest]
    public IEnumerator MovesPassingOccupiedPositionByNonContainerObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Moves()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        var ecaObjectScriptReferenceContainer = myObject.GetComponent<Container>();

        // Saving the position before calling the method
        var ecaObjectPositionBefore = new Position();
        ecaObjectPositionBefore.Assign(ecaObjectScriptReference.p);
        var positionBefore = myObject.transform.position;
        
        var pos = new Position();
        pos.x = 1.0f;
        pos.y = 1.0f;
        pos.z = 1.8f;

        // Moves() call
        Debug.Log($"Before calling {methodNameToTest}");
       
        ecaObjectScriptReference.Moves(pos);

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaObjectPositionAfter = new Position();
        ecaObjectPositionAfter.Assign(ecaObjectScriptReference.p);
        var positionAfter = myObject.transform.position;

        // Check if the positions are not the same
        PositionsAreNotEqual(ecaObjectPositionBefore, ecaObjectPositionAfter);
        Assert.AreNotEqual(positionBefore, positionAfter);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }*/

    [UnityTest]
    public IEnumerator RotatesPassingNullRotation()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Rotates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");
        
        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();
        
        // Saving the position before calling the method
        var ecaRotationBefore = new Rotation();
        ecaRotationBefore.Assign(ecaObjectScriptReference.r);
        var rotationBefore = myObject.transform.rotation;

        // Rotates() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            Action action = new Action(myObject, rotatesVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
            // ecaObjectScriptReference.Rotates(null);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaRotationAfter = new Rotation();
        ecaRotationAfter.Assign(ecaObjectScriptReference.r);
        var rotationAfter = myObject.transform.rotation;

        // Check if the positions are the same
        RotationsAreEqual(ecaRotationBefore, ecaRotationAfter);
        QuaternionsAreEqual(rotationBefore, rotationAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator RotatesWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Rotates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Saving the position before calling the method
        var ecaRotationBefore = new Rotation();
        ecaRotationBefore.Assign(ecaObjectScriptReference.r);
        var rotationBefore = myObject.transform.rotation;

        //Rotation Object
        var rotation = new Rotation();
        rotation.x = 50.0f;
        rotation.y = 40.0f;
        rotation.z = 140.0f;
        var r = rotationBefore.eulerAngles;
        if (rotation.x == ecaRotationBefore.x && rotation.y == ecaRotationBefore.y && rotation.z == ecaRotationBefore.z)
        {
            rotation.x += 1.0f;
            rotation.y += 1.0f;
            rotation.z += 1.0f;
            r.x += 1.0f;
            r.y += 1.0f;
            r.z += 1.0f;
            rotationBefore.eulerAngles = r;
        }
        
        // Rotates() call and forcing deactivated object
        Debug.Log($"Before calling {methodNameToTest}");
        ecaObjectScriptReference.isActive = false;
        // ecaObjectScriptReference.Rotates(rotation);
        Action action = new Action(myObject, rotatesVerb, rotation);
        RuleEngine.GetInstance().ExecuteAction(action);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        //TODO (Maybe couroutine)
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaRotationAfter = new Rotation();
        ecaRotationAfter.Assign(ecaObjectScriptReference.r);
        var rotationAfter = myObject.transform.rotation;

        // Check if the positions are the same
        RotationsAreEqual(ecaRotationBefore, ecaRotationAfter);
        QuaternionsAreEqual(rotationBefore, rotationAfter);


        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator RotatesPassingNotNullRotation()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Rotates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();

        // Saving the rotation before calling the method
        var ecaObjectRotationBefore = new Rotation();
        ecaObjectRotationBefore.Assign(ecaObjectScriptReference.r);
        
        var rotationBefore = myObject.transform.rotation;
        
        var rot = new Rotation();
        rot.x = 12.0f;
        rot.y = 43.0f;
        rot.z = 19.8f;

        if (rot.x == rotationBefore.x && rot.y == rotationBefore.y && rot.z == rotationBefore.z)
            rot.x++;
            
        // Rotates() call
        Debug.Log($"Before calling {methodNameToTest}");
        Debug.Log("Quaternione prima " + rotationBefore);
        
        // ecaObjectScriptReference.Rotates(rot);
        Action action = new Action(myObject, rotatesVerb, rot);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log("Quaternione dopo" + rotationBefore);
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaObjectRotationAfter = new Rotation();
        ecaObjectRotationAfter.Assign(ecaObjectScriptReference.r);
        var rotationAfter = myObject.transform.rotation;

        // Check if the rotations are not the same
        Debug.Log("Quaternione prima " + rotationAfter);
        RotationsAreNotEqual(ecaObjectRotationBefore, ecaObjectRotationAfter);
        RotationsAreEqual(ecaObjectRotationAfter, rot);
        QuaternionsAreNotEqual(rotationBefore, rotationAfter);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator LooksAtPassingNullObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Looks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");


        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReference = myObject.GetComponent<ECAObject>();


        // Saving the position before calling the method
        var ecaRotationBefore = new Rotation();
        ecaRotationBefore.Assign(ecaObjectScriptReference.r);
        var rotationBefore = myObject.transform.rotation;

        // Looks() call
        Debug.Log($"Before calling {methodNameToTest}");
        try
        {
            // ecaObjectScriptReference.Looks(null);
            Action action = new Action(myObject, looksVerb, null);
            RuleEngine.GetInstance().ExecuteAction(action);
        }
        catch (NullReferenceException e)
        {
            Debug.Log($"Null exception happened in {methodNameToTest}: therefore current test is FAILED" +
                      $"\nError message: {e.Message}");
            Assert.Fail();
        }

        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaRotationAfter = new Rotation();
        ecaRotationAfter.Assign(ecaObjectScriptReference.r);
        var rotationAfter = myObject.transform.rotation;

        // Check if the positions are the same
        RotationsAreEqual(ecaRotationBefore, ecaRotationAfter);
        QuaternionsAreEqual(rotationBefore, rotationAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject); 
    }

    [UnityTest]
    public IEnumerator LooksAtPassingNotNullObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Looks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObjectLooker = GameObject.Find("/Looker");
        var myObjectLooked02 = GameObject.Find("/Looked02");
        Debug.Log($"Loading GameObject {myObjectLooker.name}");
        Debug.Log($"Loading GameObject {myObjectLooked02.name}");


        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReferenceLooker = myObjectLooker.GetComponent<ECAObject>();
        var ecaObjectScriptReferenceLooked02 = myObjectLooked02.GetComponent<ECAObject>();


        // Saving the position before calling the method
        var ecaRotationBefore = new Rotation();
        ecaRotationBefore.Assign(ecaObjectScriptReferenceLooker.r);
        var rotationBefore = myObjectLooker.transform.rotation;
        

        // Looks() call
        Debug.Log($"Before calling {methodNameToTest}");

        var direction = (myObjectLooker.transform.position - myObjectLooked02.transform.position).normalized;

        // ecaObjectScriptReferenceLooker.Looks(ecaObjectScriptReferenceLooked02);
        Action action = new Action(myObjectLooker, looksVerb, myObjectLooked02);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaRotationAfter = new Rotation();
        ecaRotationAfter.Assign(ecaObjectScriptReferenceLooker.r);
        var rotationAfter = myObjectLooker.transform.rotation;
        
        var rotation = new Rotation();
        rotation.Assign(direction);
        
        // Check if the rotations are not the same
        RotationsAreNotEqual(ecaRotationBefore, ecaRotationAfter);
        QuaternionsAreEqual(Quaternion.Euler(direction), rotationAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObjectLooker.name}");
        Object.Destroy(myObjectLooker); 
        Object.Destroy(myObjectLooked02); 
    }

    [UnityTest]
    public IEnumerator LooksWhenObjectIsDeactivated()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Looks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObjectLooker = GameObject.Find("/Looker");
        var myObjectLooked02 = GameObject.Find("/Looked02");
        Debug.Log($"Loading GameObject {myObjectLooker.name}");
        Debug.Log($"Loading GameObject {myObjectLooked02.name}");


        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReferenceLooker = myObjectLooker.GetComponent<ECAObject>();
        var ecaObjectScriptReferenceLooked02 = myObjectLooked02.GetComponent<ECAObject>();


        // Saving the position before calling the method
        var ecaRotationBefore = new Rotation();
        ecaRotationBefore.Assign(ecaObjectScriptReferenceLooker.r);
        var rotationBefore = myObjectLooker.transform.rotation;
        
        //Forcing the flag isActive to false
        ecaObjectScriptReferenceLooker.isActive = false;

        // Looks() call
        Debug.Log($"Before calling {methodNameToTest}");

        var direction = (myObjectLooker.transform.position - myObjectLooked02.transform.position).normalized;

        //ecaObjectScriptReferenceLooker.Looks(ecaObjectScriptReferenceLooked02);
        Action action = new Action(myObjectLooker, looksVerb, myObjectLooked02);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaRotationAfter = new Rotation();
        ecaRotationAfter.Assign(ecaObjectScriptReferenceLooker.r);
        var rotationAfter = myObjectLooker.transform.rotation;
        
        var rotation = new Rotation();
        rotation.Assign(direction);
        
        // Check if the rotations are the same
        RotationsAreEqual(ecaRotationBefore, ecaRotationAfter);
        QuaternionsAreNotEqual(Quaternion.Euler(direction), rotationAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObjectLooker.name}");
        Object.Destroy(myObjectLooker); 
        Object.Destroy(myObjectLooked02); 
    }

    [UnityTest]
    public IEnumerator LooksAtPassingOccludedObject()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Looks()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObjectLooker = GameObject.Find("/Looker");
        var myObjectLooked = GameObject.Find("/Looked");
        Debug.Log($"Loading GameObject {myObjectLooker.name}");
        Debug.Log($"Loading GameObject {myObjectLooked.name}");


        // Getting the reference to ECAObjectScript
        var ecaObjectScriptReferenceLooker = myObjectLooker.GetComponent<ECAObject>();
        var ecaObjectScriptReferenceLooked = myObjectLooked.GetComponent<ECAObject>();


        // Saving the position before calling the method
        var ecaRotationBefore = new Rotation();
        ecaRotationBefore.Assign(ecaObjectScriptReferenceLooker.r);
        var rotationBefore = myObjectLooker.transform.rotation;

        // Looks() call
        Debug.Log($"Before calling {methodNameToTest}");

        var direction = (myObjectLooker.transform.position - myObjectLooked.transform.position).normalized;

        RaycastHit hit; 
        
        //ecaObjectScriptReferenceLooker.Looks(ecaObjectScriptReferenceLooked);
        Action action = new Action(myObjectLooker, looksVerb, myObjectLooked);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        if (Physics.Linecast(myObjectLooker.transform.position, myObjectLooked.transform.position, out hit))
        {
            Debug.Log("I'm checking if the object looked is occluded by someone else");
            Assert.AreEqual(myObjectLooked.name, hit.collider.gameObject.name);
        }
        else
        {
            Debug.Log("IMPOSSIBLE CASE");
            Assert.Fail();
        }
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        // Save the current position
        var ecaRotationAfter = new Rotation();
        ecaRotationAfter.Assign(ecaObjectScriptReferenceLooker.r);
        var rotationAfter = myObjectLooker.transform.rotation;
        
        var rotation = new Rotation();
        rotation.Assign(direction);
        
        // Check if the positions are the same
        RotationsAreEqual(rotation, ecaRotationAfter);
        QuaternionsAreEqual(Quaternion.Euler(direction), rotationAfter);

        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObjectLooker.name}");
        Object.Destroy(myObjectLooker); 
        Object.Destroy(myObjectLooked); 
    }

    [UnityTest]
    public IEnumerator ShowsWhenIsVisible()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Shows()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to object
        var showsScriptReference = myObject.GetComponent<ECAObject>();

        //Forcing flag to true
        showsScriptReference.isVisible = true;
            
        // Shows() call
        Debug.Log($"Before calling {methodNameToTest}");
        
        //showsScriptReference.Shows();
        Action action = new Action(myObject, showsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);    
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is still true
        Assert.IsTrue(showsScriptReference.isVisible);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator ShowsWhenIsNotVisible()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Shows()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to object
        var showsScriptReference = myObject.GetComponent<ECAObject>();

        //Forcing flag to false
        showsScriptReference.isVisible = false;
            
        // Shows() call
        Debug.Log($"Before calling {methodNameToTest}");
        
        // showsScriptReference.Shows();
        Action action = new Action(myObject, showsVerb);
        RuleEngine.GetInstance().ExecuteAction(action);  
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is true
        Assert.IsTrue(showsScriptReference.isVisible);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

    [UnityTest]
    public IEnumerator HidesWhenIsVisible()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Hides()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to object
        var showsScriptReference = myObject.GetComponent<ECAObject>();

        //Forcing flag to true
        showsScriptReference.isVisible = true;
            
        // Hides() call
        Debug.Log($"Before calling {methodNameToTest}");
        
        // showsScriptReference.Hides();
        Action action = new Action(myObject, hidesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is false
        Assert.IsFalse(showsScriptReference.isVisible);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator HidesWhenIsNotVisible()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Hides()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to object
        var showsScriptReference = myObject.GetComponent<ECAObject>();

        //Forcing flag to false
        showsScriptReference.isVisible = false;
            
        // Hides() call
        Debug.Log($"Before calling {methodNameToTest}");
        
        // showsScriptReference.Hides();
        Action action = new Action(myObject, hidesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);  
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is still false
        Assert.IsFalse(showsScriptReference.isVisible);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator ActivatesWhenIsActive()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Activates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to object
        var showsScriptReference = myObject.GetComponent<ECAObject>();

        //Forcing flag to true
        showsScriptReference.isActive = true;
            
        // Activates() call
        Debug.Log($"Before calling {methodNameToTest}");
        
        // showsScriptReference.Activates();
        Action action = new Action(myObject, activatesVerb );
        RuleEngine.GetInstance().ExecuteAction(action);  
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is still true
        Assert.IsTrue(showsScriptReference.isActive);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator ActivatesWhenIsNotActive()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Activates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to object
        var showsScriptReference = myObject.GetComponent<ECAObject>();

        //Forcing flag to false
        showsScriptReference.isActive = false;
            
        // Activates() call
        Debug.Log($"Before calling {methodNameToTest}");
        
        // showsScriptReference.Activates();
        Action action = new Action(myObject, activatesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);  
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is true
        Assert.IsTrue(showsScriptReference.isActive);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator DeactivatesWhenIsActive()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Deactivates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to object
        var showsScriptReference = myObject.GetComponent<ECAObject>();

        //Forcing flag to true
        showsScriptReference.isActive = true;
            
        // Deactivates() call
        Debug.Log($"Before calling {methodNameToTest}");
        
        // showsScriptReference.Deactivates();
        Action action = new Action(myObject, deactivatesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);  
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is false
        Assert.IsFalse(showsScriptReference.isActive);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }
    
    [UnityTest]
    public IEnumerator DeactivatesWhenIsNotActive()
    {
        Debug.Log($"Current test: {TestContext.CurrentContext.Test.Name}");
        methodNameToTest = "Deactivates()";
        yield return WaitForScene();

        // Instantiate the object required for testing
        var myObject = GameObject.Find(objInstanceName);
        Debug.Log($"Loading GameObject {myObject.name}");

        // Getting the reference to object
        var showsScriptReference = myObject.GetComponent<ECAObject>();

        //Forcing flag to false
        showsScriptReference.isActive = false;
            
        // Deactivates() call
        Debug.Log($"Before calling {methodNameToTest}");
        
        // showsScriptReference.Deactivates();
        Action action = new Action(myObject, deactivatesVerb);
        RuleEngine.GetInstance().ExecuteAction(action);  
        
        Debug.Log($"After calling {methodNameToTest}");

        // Wait a little
        yield return new WaitForSeconds(ShortTimeDelay);

        //Checking if flag is still false
        Assert.IsFalse(showsScriptReference.isActive);
        
        // Destroy the object 
        Debug.Log($"Destroying GameObject {myObject.name}");
        Object.Destroy(myObject);
    }

}