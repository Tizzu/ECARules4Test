using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECARules4All.RuleEngine;
using Action = ECARules4All.RuleEngine.Action;

public class buttonCube : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        RuleEngine.GetInstance().Add(new Rule(new Action(GameObject.Find("RightHand2"),"interacts with", this.gameObject),
            new SimpleCondition(GameObject.Find("Cubeddu"), "visible", "is", true),
            new List<Action>
            {
                new Action(GameObject.Find("Cubeddu"), "changes", "visible", false),
            }));
        
        RuleEngine.GetInstance().Add(new Rule(new Action(GameObject.Find("RightHand2"),"interacts with", this.gameObject),
            new SimpleCondition(GameObject.Find("Cubeddu"), "visible", "is", false),
            new List<Action>
            {
                new Action(GameObject.Find("Cubeddu"), "changes", "visible", true),
            }));
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        EventBus.GetInstance().Publish(new Action(other.gameObject, "interacts with", this.gameObject));
    }
}
