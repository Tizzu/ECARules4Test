using System.Collections;
using System.Collections.Generic;
using ECARules4All;
using UnityEngine;
using ECARules4All.RuleEngine;
public class RuleList : MonoBehaviour
{
    // Start is called before the first frame update 
    void Start()
    { 
        RuleEngine.GetInstance().Add(new Rule(
            new Action(GameObject.Find("Player"),"interacts with", GameObject.Find("Visible")),
            new CompositeCondition(CompositeCondition.ConditionType.OR, new List<Condition>
            {
                new SimpleCondition(GameObject.Find("Sphere"), "visible", "is", true),
                new SimpleCondition(GameObject.Find("Sphere"), "active", "is", false),
                new CompositeCondition(CompositeCondition.ConditionType.OR, new List<Condition>
                {
                    new SimpleCondition(GameObject.Find("Sphere"), "visible", "is", true),
                    new SimpleCondition(GameObject.Find("Sphere"), "active", "is", false)
                })
            }),
            new List<Action>
            {
                new Action(GameObject.Find("Sphere"), "changes", "visible", true),
                //It's called Uman because it's a poetic license, not a grammar error :)
                //new Action(GameObject.Find("Uman"), "jumps to", new Position(-3.5f, 1.62f, -2.42f)),
                new Action(GameObject.Find("Uman"), "jumps on", new Path(new List<Position>()
                {
                    new Position(-3.5f, 1.62f, -2.42f),  new Position(-3.6f, 1.62f, 2.42f), new Position(3.42f, 1.62f, 2.42f), new Position(3.42f, 1.62f, -2.42f)  
                })),
                new Action(GameObject.Find("Sword"), "stabs", GameObject.Find("Uman")),
                new Action(GameObject.Find("Sword"), "fires", GameObject.Find("Uman")),

            }
        ));
        
        RuleEngine.GetInstance().Add(new Rule(
            new Action(GameObject.Find("Player"),"interacts with", GameObject.Find("Visible")),
            new List<Action>
            {
                new Action(GameObject.Find("Shield"),"blocks", null),
            }
        ));
        
        RuleEngine.GetInstance().Add(new Rule(
            new Action(GameObject.Find("Player"),"interacts with", GameObject.Find("Active")),
            new CompositeCondition(CompositeCondition.ConditionType.OR, new List<Condition>
            {
                new SimpleCondition(GameObject.Find("Sphere"), "active", "is", false)
            }),
            new List<Action>
            {
                new Action(GameObject.Find("Sphere"), "changes", "active", true),
            }
        ));
        
        RuleEngine.GetInstance().Add(new Rule(
            new Action(GameObject.Find("Player"),"interacts with", GameObject.Find("Active")),
            new CompositeCondition(CompositeCondition.ConditionType.OR, new List<Condition>
            {
                new SimpleCondition(GameObject.Find("Sphere"), "active", "is", true)
            }),
            new List<Action>
            {
                new Action(GameObject.Find("Sphere"), "changes", "active", false),
            }
        ));
        
        /*
        RuleEngine.GetInstance().Add(new Rule(
            new Action(GameObject.Find("Player"),"interacts with", GameObject.Find("Active")),
            new List<Action>
            {
                new Action(GameObject.Find("Sphere"), "changes", "active", true),
            }
        ));
        */
        
        RuleEngine.GetInstance().Add(new Rule(
            new Action(GameObject.Find("Player"),"interacts with", GameObject.Find("Inactive")),
            new List<Action>
            {
                new Action(GameObject.Find("Sphere"), "swaps", "active", false),

            }
        ));

        //This rule is for a Behaviour instead of a ECAObject, it works the same way
        RuleEngine.GetInstance().Add(new Rule(
            new Action(GameObject.Find("Player"),"interacts with", GameObject.Find("Sphere")),
            //new Condition(GameObject.Find("Sphere"), "on", "is", false),
            new List<Action>
            {
                //new Action(GameObject.Find("Sphere"), "turns", true),
                new Action(GameObject.Find("Sword"), "fires", GameObject.Find("Player")),

            }
        ));
        
        //This rule is for a Behaviour instead of a ECAObject, it works the same way
        RuleEngine.GetInstance().Add(new Rule(
            new Action(GameObject.Find("Player"),"interacts with", GameObject.Find("Sphere")),
            new SimpleCondition(GameObject.Find("Sphere"), "on", "is", false),
            new List<Action>
            {
                //new Action(GameObject.Find("Sphere"), "turns", true),
                new Action(GameObject.Find("Sword"), "fires", GameObject.Find("Player")),

            }
        ));
        

    }
}
