﻿using System;
using System.IO;
using UnityEngine;

namespace ECARules4All.RuleEngine
{
    public class TextRuleSerializer
    {
        public TextRuleSerializer()
        {

        }

        public void SaveRules(string path)
        {
            using(StreamWriter writer = File.CreateText(path))
            {
                PrintDeclarations(writer);
            }
        }

        public Type GetECAObjectType(GameObject gameObject)
        {
            bool isECAObject = false;
            bool changed;
            Type maxType = typeof(ECAObject);

            do
            {
                changed = false;
                foreach (Component c in gameObject.GetComponents(typeof(Component)))
                {
                    if (c.GetType() == typeof(ECAObject))
                    {
                        isECAObject = true;
                    }
                    else
                    {
                        foreach (RequireComponent require in c.GetType().GetCustomAttributes(
                            typeof(RequireComponent), true))
                        {
                            if (require.m_Type0 == maxType)
                            {
                                maxType = c.GetType();
                                changed = true;
                            }
                        }
                    }

                }
            } while (changed);
            

            if (isECAObject) return maxType;
            else return null;
        }

        private string GetTypeName(Type t)
        {
            var attrs = t.GetCustomAttributes(typeof(ECARules4AllAttribute), false);
            ECARules4AllAttribute attr = (ECARules4AllAttribute)attrs[0];
            return attr.Name;
        }

        private string GetTokenString(int token)
        {
            return ECARulesLexer.DefaultVocabulary
                .GetDisplayName(ECARulesLexer.DEFINE)
                .Replace("'", "");
        }

        private void PrintDeclarations(TextWriter writer)
        {
            var objects = GameObject.FindObjectsOfType<ECAObject>();
            foreach(ECAObject obj in objects)
            {
                GameObject gameObj = obj.gameObject;
                writer.WriteLine(String.Format("{0} {1} {2};",
                    GetTokenString(ECARulesLexer.DEFINE),
                    GetTypeName(GetECAObjectType(gameObj)),
                    gameObj.name
                    )); ;
            }
        }
    }
}
