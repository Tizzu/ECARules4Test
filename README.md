# ECARules4All
***
NOTE: THIS IS AN OLD, ARCHIVED VERSION OF THE PROJECT, PLEASE REFER TO https://github.com/cg3hci/ECARules4All FOR USING THE MOST UP TO DATE VERSION

ORIGINAL README BELOW 
***
**Prerequisites**
*  Unity 2020.2.7f1
*  JetBrains Rider
(if you already have Visual Studio, I would suggest to change deafault IDE in Unity to Rider)

**Set up**
1.  Clone via ssh the project (See how to create an SSH key here: [Generating a New SSH Key pair](https://platform.xr4all.eu/help/ssh/README#generating-a-new-ssh-key-pair))
2.  Open the project in Unity
3.  Open the solution in Rider

As you can see, inside Assets/ECAScripts there is the core of the project, with a proper read me.
Scenes/WirldInteractionDemo contains a demo scene for XR
Scenes/SampleScne contains a demo scene for Unity
